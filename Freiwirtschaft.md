# Freiwirtschaft

ist ein Wirtschaftsmodell, das auf der Kritik der Geldverfassung und der
Kritik der Grundrente durch [Silvio Gesell](/silvio_gesell "wikilink")
beruht. Freiwirtschaft setzt sich aus den drei Konzepten Freiland,
Freigeld und Freihandel zusammen.

Freihandel
----------

ist die Abschaffung nationaler Wirtschaftsgrenzen gemeint. Da Freihandel
von praktisch allen Ökonomen gefordert und befürwortet wird, ist
Freihandel der einzige Freiwirtschaftliche Aspekt, der sich soweit
global durchzusetzen scheint.

Der Freihandel wird aber oft als eine der Ursachen für [wirtschaftliche
Asymmetrien](/wirtschaftliche_asymmetrien "wikilink") genannt, da offene
Grenzen und Subventionsabbau alter Strukturen zerstören, die Vorteile
aber oft von einzelnen Händlern abgeschöpft werden

Freiland
--------

Nach freiwirtschaftlicher Auffassung soll die Bodenrente nicht in
private Verfügung gelangen, sondern der Allgemeinheit zukommen, weil
Boden ein Produkt der Natur und kein vom Menschen geschaffenes Gut ist,
und der Wert, und damit die Bodenrente, nur durch die Allgemeinheit
entsteht.

Ob man nun Grund und vor allem dessen Bodenschätze, oder nur die darauf
gewonnenen Güter als [Eigentum](/eigentum "wikilink") betrachtet ist
strittig.

Freigeld
--------

Hauptforderungen der Freigeldpolitik sind:

-   Abschaffung des Goldstandards und Flexible Wechselkurse
-   Einführung einer [Umlaufgesicherten
    Währung](/umlaufgesicherten_währung "wikilink")

Mit [LUNA](/luna "wikilink") gibt es natürlich keinen Goldstandard mehr
und die Wechselkurse zwischen einzelnen Währungssystemen wären wirklich
mal flexibel, ohne politische Pfuscherei welche sozial verkauft wird,
aber nur feudale Strukturen erhalten lässt.

<Kategorie:Theorie>
