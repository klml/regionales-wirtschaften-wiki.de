# CongressGeld

**CongressGeld** ist ein mögliches Modell zur experimentellen Versuch
eine (temporäres) [dezentrales
Finanzsystem](/dezentrale_finanzsysteme "wikilink") auf die Beine zu
stellen. Der Name CongressGeld kommt von der Idee am 25c3, außerdem
klingt der Begriff *Congress*, eine Zusammenkunft von Personen, im
Zusammenhang mit Geld recht nett.

Bei Dorf, Viertel, Vereinsfesten werden oft Essens- und Getränkebons
ausgegeben. Teilweise aus hygienischen Gründen (Geldhände sollen kein
Essen anfassen), teilweise aus organisatorischen Gründen (der Mann am
Zapfhahn soll zapfen und nicht Wechselgeld raus kramen). Dieses Bons
haben aber noch einen Vorteil, die vielen freiwilligen Helfer bekommen
als eine klitzekleine Aufwandsentschädigung solche Bierbons als Lohn,
aber ohne dafür Geld zu bezahlen. Sie bekommen also Bier und Essen ohne
dafür “richtiges” Geld auszugeben, sondern nur Ihre Leistung. Denn diese
Gesamtleistung aus z.B. Bewirtung und Musik ist ja der Grund dafür das
die echten Kunden mehr als den Einkaufspreis der Flasche Bier bezahlen.

Rein rechnerisch schaut das so aus: der Verein kauft 1000 Flaschen Bier
für € 500, verkauft gegen echtes Geld 500 Flaschen zu je € 1, hat so den
Einkaufspreis reamortisiert und kann 500 weitere Flaschen als “Gewinn”
über weitere Bons an seine Helfer ausgeben. Zugegeben bei dieser
Kalkulation wären die Helfer schnell arbeitsunfähig;) Der Verein kann
aber auch dem Kassenwart und dem Platzwart, die nichts zum Fest
beitragen aber unter dem Jahr Bücher führen und rasen mähen
entschädigen. Soweit würde er aber die Geldschöpfung auf nicht
Congresseinzahler (echtes Geld oder Leistung) ausdehnen. Was vollkommen
in Ordnung ist und in maßen funktioniert. Der Verein sollte gut daran
tun dem (F)Bon(d)verwalter, dem (F)Bon(d)weiterträgerhilfsstempler und
dem (F)Bon(d)abreiser nicht jeweils 29% Leistungs-Boni zugestehen; das
würde kein Vereinsvorstand zulassen.

CongressGeld will jetzt aber kein reiner Ersatz der bunten
[Bierbons](http://www.flickr.com/photos/kamikadse/2497358606/), sondern
will dieses System ausweiten um so noch mehr Leistungen mit
“vergnügensgedeckten” Währungsystemen tauschen zu können. Vielleicht
lässt sich ja der externe Bühnenbauer dazu überreden, auch nur einen
kleinen Teil, seines Lohns in diesem Geld anzunehmen, weil er eben weiß,
das er die jährliche Elektroüberprüfung seiner Geräte bei einem
Elektrofachbetrieb vornehmen kann, der wiederum die Reparatur seines
Autos bei einer Werkstatt machen lassen kann, die wiederum bei einer
Steuerkanzlei Kunde ist dessen Betriebsausflug genau auf dieses
Vereinsfest sein wird. Der echte Leistungsflusss wird dazu noch
verästelt und wesentlich komplexer sein.

Wichtig ist das der Bühnenbauer von Anfang an nicht wissen muss wie
dieser weg sein wird, sondern [vertrauen](/vertrauen "wikilink") haben
kann das er passieren wird.

Hat man ein solches Experiment erfolgreich gestartet, kann man das ganze
auch gleich als [Regionalgeld](/regionalgeld "wikilink") weiterführen.

<Kategorie:Regionalgeld>
