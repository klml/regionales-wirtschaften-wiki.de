# Redundanz

**Redundanz** (latein. redundare „im Überfluss vorhanden sein“)
bezeichnet grundsätzlich einen Zustand von Überschneidung oder
Überfluss. ()

Im ökonomischen Kontext spielt Redundanz eine wichtige Rolle bei der
Stabilität des wirtschaftlichen und damit des gesellschaftlichen
Systems. Wie im ingenieurwissenschaftlichen Bereich auch werden
Wirtschaftsstrukturen zunehmend mehrfach ausgelegt, um Instabilitäten
eines Bereichs durch weitere Systeme aufzufangen. Ein Beispiel für die
Entwicklung redundanter Systeme ist der parallele Einsatz [regionaler
Währungen](/regionalgeld "wikilink"), kontinentaler Währungen und eines
oder mehrerer globaler Währungssysteme. (siehe auch
[Währungswettbewerb](/währungswettbewerb "wikilink"))
