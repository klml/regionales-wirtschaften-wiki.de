# Elia

Ich habe mich aus der Buchhaltungsecke auf das Thema zu bewegt, speziell
das Thema Debitoren und Kreditoren - Konten. Das Gedankenspiel dazu ist
recht simpel.

Angenommen die Knex AG richtet ihren Lieferanten einen Zugriff auf das
zugehörige Kreditorenkonto ein. Damit ist im Grunde schon ein
Elektronisches-Geld geschaffen mit dem die Lieferanten der Knex AG via
OnlineBanking untereinander Rechnungen begleichen können. Hier ist die
Geldschöpfung an eine Lieferung an die Knex AG gebunden. Das System ist
leicht zu erweitern auf Leute die dem Geldemittenten Euros oder Regios
geben und dann eRegios gutgeschrieben bekommen. So ist es auch möglich
Goldwährungen zu implementieren, pro Gramm Gold gibts ein eGold oder so
was in der Art.

Bei dieser Idee würde eine Buchhaltungssoftware den Part der lokalen
XML-Datei übernehmen. Jeder Prosument muss nun bei jedem
Währungsanbieter dessen Währung er akzeptiert ein Konto besitzen.
Ebenfalls kann er eine eigene Währung anbieten.

Auf der technischen Ebene müssen die Währungsanbieter im Internet immer
erreichbar sein, denn ansonsten können keine Transaktionen angestoßen
werden.

Die Marktlücke die ich hier sehe ist das berüchtigte Micropayment. Eine
einfache Bezahlfunktion für Kleinstbeträge fehlt dem Internet auf jeden
Fall. Vielleicht analog zu dem mailto-Link ein payto-Link oder so etwas.

Als Problem sehe ich aber noch die Umsetzung einer marktgerechten
Umlaufgebühr. Eventuell ist auch der Umgang mit mehreren Währungen etwas
zu kompliziert obwohl ich denke dass das eigentlich nicht so schwer ist.
