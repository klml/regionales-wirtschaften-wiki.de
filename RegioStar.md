# RegioStar

Die **RegioStar**-Genossenschaft ist eine aus dem
[Sterntaler](/sterntaler "wikilink") hervorgegangene
[Genossenschaft](/genossenschaft "wikilink"). Sie basiert auf dem
Gedanken der [Lokalen Agenda](/lokale_agenda_21 "wikilink"). Priorität
hat die Versorgung der Genossenschaftler auf Basis regionaler
Ausrichtung. Die Geschäftsbereiche umfassen derzeit die Betreuung des
Regiogeldes [Sterntaler](/sterntaler "wikilink"), den Betrieb eines
[Dorfladens](/dorfladen "wikilink") und eines
[Tauschringes](/tauschring "wikilink"). Künftig soll der Ansatz um einen
[Regionalfond](/regionalfond "wikilink") erweitert werden.

-   Webseite: <http://www.regiostar.com>

<Kategorie:Regionalgeld>
