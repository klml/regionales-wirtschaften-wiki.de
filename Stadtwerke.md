# Stadtwerke

Die Bedeutung von [Stadtwerke](https://de.wikipedia.org/wiki/stadtwerke "wikilink") für die
regionale [Energiewirtschaft](/energie "wikilink").

Strom ist weniger ein Gut als ein Service, denn er ist schwer zu
speichern und zu transportieren. So ist Strom immer auch ein regional
besonderes Angebot.

Mit lokal erzeugten und regional vernetzter Struktur einen Energiemarkt
schaffen, der die Endverbraucher mit [variablen
Tarifen](https://de.wikipedia.org/wiki/variable_tarife "wikilink") zur
[Netzentlastung](/netzentlastung "wikilink") motiviert.

1.  1.  Literatur

-   *Der Aufstieg der Konzerne war also auch mit dem Niedergang der
    Stadtwerke verbunden, auf die die Konzerne hämisch als bloße
    'Weitervertreiber' herabblickten*(Peter Becker: Aufstieg und Krise
    der deutschen Stromkonzerne ISBN 978-3920328577)
-   oder sogar [Erzeugung
    Genossenschaften](http://blog.stromhaltig.de/2013/07/burgerbeteiligung-wie-es-auch-gehen-kann/)

1.  1.  Beispiele

-   [Doch beim Kampf ums Netz geht es eben um mehr als nur ums
    kurzfristige Geschäft. Es geht um die
    Macht.](http://www.zeit.de/2014/53/energiewende-schwarzwald-e-on)

<Kategorie:Energie>
