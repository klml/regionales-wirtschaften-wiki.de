# OpenPatent

**OpenPatent** ist die Idee “offener Patente”, also technische
Definitionen im öffentlichen Eigentum, nutzbar von jedermann. Dahinter
steht die Weltsicht, daß Eigentum an immateriellen Objekten den Großteil
der Menschheit unzumutbar ausschließt: Das alte Patentwesen erlaubt die
Nutzung von Wissen nur dem Patentanmelder, während 6 Milliarden Menschen
von diesem Wissen ausgeschlossen werden.

OpenPatent würde ermöglichen, dass technologisches Wissen schnell von
vielen Menschen angewandt werden kann.

Verwandt ist der Ansatz mit [OpenSource](/opensource "wikilink"), also
Software, deren Quelltext offen vorliegt und die frei genutzt werden
kann.

Beispiele
---------

-   [Open Graphics Project](http://wiki.opengraphics.org/tiki-index.php)
-   [RepRap](http://reprap.org/bin/view/Main/WebHome)
-   [OpenCores.org](http://opencores.org/)

Argumentation & News
--------------------

-   [US-Wirtschaftswissenschaftler fordern Abschaffung geistigen
    Eigentums](http://www.heise.de/newsticker/US-Wirtschaftswissenschaftler-fordern-Abschaffung-geistigen-Eigentums--/meldung/134400)

<Kategorie:OpenSource>
