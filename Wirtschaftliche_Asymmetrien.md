# Wirtschaftliche Asymmetrien

Der Mensch hat (Nahrung, Wohnung, Gesundheit, Sicherheit etc). Diese
kann er und will er nicht allein decken. Durch Arbeitsteilung und
Spezialisierung befriedigt jeder Teilnehmer andere Bedürfnisse. Die
Regulation wer wie viel, gegen was diese Arbeit verrichtet kann man
durch oder durch Marktwitschaft und [Geld](/geld "wikilink") steuern.

Das Ziel sollte sein das möglichst alle Menschen sich maximal viele
Bedürfnisse mit minimalem Aufwand befriedigen können. Das hat moralische
und motivatorische Gründe.

Allerdings treten oft Asymmetrien ein, da bestimmte Gruppen wesentlich
mehr decken können als andere. Das kann an

-   mehr Fleiß und Innovation liegen,
-   an Ausnutzung von Trägheitseffekten

Wer doppelt so viel Holz hackt, dem sollte auch doppelt so warm sein.
Echte Leistung darf auch belohnt werden. Wer aber Luxus dadurch anhäuft
das er Geld einmal um den Globus schiebt erbringt keine reale Leistung,
sondern nutzt träge Wirtschaftsverhältnisse aus.

<Kategorie:Theorie>
