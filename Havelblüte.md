# Havelblüte

Die **Havelblüte** ist ein [Regionalgeld](/regionalgeld "wikilink") in
Potsdam. Sie setzt Cyclos [ein](http://havelblueten.digitekst.com/)
(anscheinend auch für [Oder-](/oderblüte "wikilink") und
[Spreeblüte](/spreeblüte "wikilink")).

<Kategorie:Regiogeld-Initiativen>
