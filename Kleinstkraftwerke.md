# Kleinstkraftwerke

![mini|Wasser](/060929_kw_buchholz_001.jpg "fig:mini|wasser") Wie erzeugt
man dezentrale [Energie](/energie "wikilink")?

-   [Kleinwasserkraft](https://de.wikipedia.org/wiki/kleinwasserkraft "wikilink") kann nur gebaut
    werden wo auch Wasser fliesst. Auch wenn es sicherlich noch
    Nutzungslücken gibt, die Wasserkraft ist annähernd ausgebaut oder
    kann nur mit großen finanziellen oder ökologischen Aufwänden
    erweitert werden.
-   Solare Stromerzeugen ist meist im kleinen Bereich, hat aber
    betriebsbedingte Nachteile (Nachts und Winter) die von anderen
    Systemen abgefangen werden müssen.
-   [Mikro-Kraft-Wärme-Kopplung](https://de.wikipedia.org/wiki/mikro-kraft-wärme-kopplung "wikilink")
    die Strom und Abwärme nutzen haben gegenüber Fernwärme liefernden
    [Gas-und-Dampf-Kombikraftwerk](https://de.wikipedia.org/wiki/gas-und-dampf-kombikraftwerk "wikilink")
    eine schlechtere Effektivtät, da diese zusätzlich in einem
    thermodynamischen sinnvollen Bereich mit hohen Drücken und
    Temperaturen arbeiten. Dennoch sind GuD im ländlichen Bereich nicht
    sehr effektiv, da wesentlich merh Fernwäremleitung verbaut werden
    müsste. Und so lange es normale Gaskessel gibt und neu verbaut
    werden sollten diese lieber Mini-Blockheizkraftwerke sein.

Literatur
---------

Peter Becker: Aufstieg und Krise der deutschen Stromkonzerne ISBN
978-3920328577 255

<Kategorie:Energie>
