# ZukunftsWirtschaftsVision

Hier möchte ich meine Visionen für eine zukünftige Gesellschaft
festhalten, ausgehend von einem Wandel im Wirtschaftssystem. Gerne nehme
ich Anregungen und Kritik auf; seit aber noch nicht zu hart da das
Gedankengeflecht erst aufgebaut werden muß.

Jörn-Derek aus der “Ortenau”

Marktbasierende Nachhaltigkeitswirtschaft
-----------------------------------------

Ich möchte mich dem Thema einfach mal über eine Definition nähern; als
Fokus für den weiteren Denkprozess. Ungewöhnlich vielleicht, aber mal
einen Versuch wert denke ich.

### Begriffserläuterung

Im Zentrum des Systems steht die **Nachhaltigkeit als Zielsetzung des
wirtschaftlichen Handelns**. Die Wirtschaft soll die Menschen mit den
benötigten Gütern und Dienstleistungen versorgen, jedoch unter der
Maßgabe eine lebendige, ausgewogene Gesellschaft und eine ökologisch
intakte Umwelt zu erhalten oder (wie in den meisten heutigen Systemen
nötig) zu erschaffen. Die Nachhaltigkeit ist nicht nur ein Nebeneffekt,
sondern muß im Zentrum stehen.\
*Wir haben keine andere Wahl ! Gibt es ein besseres Argument ? Wir
müssen die Gewichtung so legen, um unsere Würde als Menschen zu bewahren
und uns dem Geschenk dieser schönen Welt würdig zu erweisen und diese
insbesondere auch zu erhalten. Jetzt könnte natürlich gesagt werden: die
Bedürfnisse des Einzelnen gehen hier verloren, alles für das Kollektiv !
Falsch: der Einzelne nimmt sich nur etwas zurück; die Wirtschaft ist ja
doch für Ihn (und alle anderen) da und die soziale Nachhaltigkeit
erfordert ja unabdingbar ein möglichst große Zahl an zufriedenen oder
gar glücklichen Menschen*. Ja gerade die Nachhaltigkeit und auch der Weg
dorthin führt zu einem Aufwerten der menschlichen Beziehungen, die für
fast jeden den größten Lebensinhalt bilden. Die Transformation einer
Ego-Gesellschaft in eine Wir-Gesellschaft

**Der freie Markt mit seinen Elementen dient hier nur mehr als Medium
und Motor**, nicht jedoch als Selbstzweck. Die Wirkung des Marktes und
seine Effizienz ist unbestritten, eine dynamische, möglichst feine
Steuerung jedoch unabdingbar.\
*Zu stark ist noch das politische Ringen um den Markt, als Allheilmittel
für die einen, als Höllenmaschine für die anderen. Ein Markt, mit neuen
Mitteln gezähmt (nicht gebändigt)ist Gold wert: als Drehtür zum
Austausch von Waren, Leistungen, Ideen und Möglichkeiten ist er wohl
auch unabdingbar für den Entwicklungsstand der heutigen Menschen.*

### Ziel

Das Ziel dieser Wirtschaftsform ist es, ein sozial und ökologisch
intaktes Lebensumfeld zu begünstigen. Die soziale Qualität wäre an der
Zufriedenheit eines möglichst großen Teils der Bevölkerung zu messen,
die Ökologische an der Qualität der natürliche Lebensräume des
Wirtschaftsraumes, der Schonung von natürlichen Ressourcen und dem
Ausbau regenerativer Systeme.

### Werkzeuge

Die wichtigste Vorgabe ist, Entscheidungen und Handlungen
wirtschaftspolitischer Natur zum größtmöglichen Teil in die Regionen zu
verlagern. Durch einen möglichst großen Anteil regionaler
Selbstversorgung gewinnt die Wirtschaft eine hohe Stabilität und schont
die Ressourcen durch kurze Wege der Produkte. Ebenso wird
sichergestellt, das ein großes Spektrum an Fachkompetenz und
Fertigungsressourcen vor Ort vorgehalten werden. Des weiteren soll die
Realwirtschaft gestärkt werden um Spekulationsblasen zu verhindern.
Dabei wirken komplementäre Geldsysteme unterstützend, sowohl als
Regionale Zweitwährungen als auch als Sektoralwährungen wie z. B.
Zeitkonten in der Altersvorsorge.

Titelideen

Organ-ismus – Der neue Weg

1.Einleitung : Abgrenzung zum Bestehenden
-----------------------------------------

1989, Perestroika, Zerfall des Ostblocks und der damit verbundenen
Konkursmeldung des Kommunismus, und somit der Sieg des Kapitalismus.

Der Kapitalismus hat sich als funktionstüchtiger erwiesen als der
Kommunismus (in der Gestalt des Sozialismus, Maoismus o.ä.) und somit
fühlten sich dessen Vorreiterstaaten (allen voran die USA) in ihrem
Gesellschaftsbild allgemein bestätigt; was sich aktuell sehr schön aus
der Agitation der USA in ihrem Krieg gegen den Terror herauslesen lässt.

Jedoch hat der Kapitalismus als dominierendes System einen gravierenden
Nachteil: seine grundlegende Rücksichtslosigkeit. Trotz verschiedener
Abfederungen (z.B. die soziale Marktwirtschaft und ökologischer
Gesetzgebung) möchte ich sein Verhalten dennoch mit dem eines
wildgewordenen Elefanten vergleichen: unbändige Kraft ohne Feingefühl.
Dies soll natürlich nicht seine einzelnen Teilnehmer oder Unternehmen
beschreiben sondern seine Wirkung als Ganzes.

Marx und seine Kritik haben bis heute recht behalten: die grosse Kluft
zwischen Besitz und Arbeit ist weiterhin vorhanden, evtl. sogar grösser
geworden und Abhängigkeiten und der Sklaverei ähnliche
Arbeitsverhältnisse bestehen weiter, wenn auch vielfach in
„modernisierter“ Form.

In der Zeit seiner Begründung mag er noch eine größere Nützlichkeit
bewiesen haben; doch die Umstände haben sich seither dermaßen geändert
so das eine generelle Neubewertung unabdingbar ist. Dazu einige Punkte:

-   Die Globalisierung: ist die weltweite Dynamik der Wirtschaft
    überhaupt vereinbar mit den Prinzipien des Kapitalismus ? – Bildung
    von Auswüchsen und

Fokussierung der Einleitung : Unverzichtbarkeit der Ganzsicht
-------------------------------------------------------------

Ganzsicht, ein komisches Wort allemal. Doch wie sonst die Notwendigkeit
zur Beachtung nicht nur von naheliegenden sondern auch von entfernteren
Faktoren beschreiben ? Umsicht ist zu schwach, Ganzheitlich in seiner
Bedeutung schon zu sehr belegt von spirituellen Gesichtspunkten um
neutral etwas aussagen zu können.

Also: Ganzsicht.

In diesem kleinen Wort liegt die Essenz des ganzen Ansatzes. Der in
alternativen Kreisen geschmähte Begriff „Globalisierung“ und die
Vernetzung und Kommunikationsmöglichkeiten der Weltbevölkerung sind
Elemente, die in ihren Auswirkungen damit verknüpft sind, verbunden,
eine Ganzsicht letztendlich erforderlich machen. Die Auswirkungen von
Ereignissen und Entwicklungen in Teilen unserer modernen Gesellschaften
auf die ganze Welt werden immer mehr und auch schneller. Jedoch werden
diese Auswirkungen immer noch entscheidend von kapitalistischen Faktoren
dominiert:

Macht und Besitz.

So haben wir einen regen Verkehr von Menschen, Waren und Informationen
aber doch beschränkt sich dieser fast ausschließlich auf das absolut
Notwendige (in den maßgeblichen Bereichen). Die Chancen, Perspektiven
und Optionen die mit diesen weltumspannenden

Die Grundlage : Kapitalismus als Motor
--------------------------------------

Es ist sicherlich unnötig, einen gesonderten Beweis für die Effizienz
des Kapitalismus zu führen. Staaten, die nach marktwirtschaftlichen
Grundlagen arbeiten sind generell damit sehr erfolgreich. Und dies
aufgrund der Tatsache, das er normalerweise dort anspricht, wo der
Mensch leicht schwach wird: die Gier nach Besitz (und Macht).

Doch letztendlich hat sich auch der Kapitalismus deklassiert: seine
Kernthese, das das Streben nach eigenem Vorteil letztendlich allen
nutzt. Die Schere zwischen arm und reich klafft weiter auf denn je zuvor
und nur eine hohe Wirtschaftsleistung über viele Jahr verhindert massive
Folgen für die unteren Gesellschaftsschichten. Ein kleines Beispiel:
nach dem Studium finden sich viele Akademiker in einem Praktikum statt
in einer festen Anstellung wieder, vielfach mit Anforderungen wie bei
einer vollwertigen Stelle jedoch mit geringer oder gar keiner Bezahlung.
Warum passiert das ? Ganz einfach, weil die Möglichkeiten (überschuss an
Akademikern, Fehlen von Regeln) besteht und dies einen wirtschaftlichen
Vorteil ergibt. Allein die Hoffnung, nach dem Praktikum fest angestellt
zu werden sorgt für eine ausreichende Arbeitsmoral.

Selbst bei gutem Willen hat eine Firmenleitung immer weniger
Möglichkeiten, für ein angemessenes Arbeitsmilieu zu sorgen: die
Finanzwelt wird von immer mehr ansteigenden Renditeerwartungen
beherrscht und schränkt den Spielraum des Managements in sozialen (und
auch ökologischen Fragen) immer mehr ein. Natürlich gibt es eine
Gegenbewegung, die nachhaltige Produkte und Produktion verstärkt
würdigt; ihr Marktanteil ist jedoch immer noch klein und ihre
Widerstandsfähigkeit gegen eine drohende Wirtschaftskrise vermutlich
sehr gering. Wenn das Geld knapp wird wird die Kaufentscheidung noch
mehr als jetzt vom Preis bestimmt.

Dies könnte nun ein Ansatz werden, den Kapitalismus zu verdammen und
seine Abschaffung zu fordern. Doch dies wäre zu kurzsichtig. Der
Kapitalismus reizt die Menschen (natürlich an seinen schlechten
Charaktereigenschaften Egoismus und Gier) zu Leistung und Entwicklung.
Dies ist natürlich grundsätzlich gut und wünschenswert, nur steht diese
Stimulation leer im Raum und ist beim puren Kapitalismus nicht an eine
Wertigkeit gebunden. Wenn die Produktion von Splitterbomben mehr Rendite
bringt als die Herstellung von günstigen Solarkochern für die Dritte
Welt dann ist die Entscheidung des Unternehmers oder Investors relativ
klar. Der Kapitalismus (oder Marktwirtschaft als gefälligere
Bezeichnung) stellt diese Frage nicht, er folgt immer dem grössten
Ertrag in kürzester Zeit. Er schafft somit ein darwinistisches Szenario:
der Stärkste (hier nun der Reichste oder Cleverste) gewinnt, die anderen
bekommen die Reste. Wollen wir wirklich, das unsere Gesellschaft von
solch archaischen Gesetzen gebrägt wird ? Wie schon gesagt: der
Kapitalismus als dominantes System ist zu verwerfen, nicht sein
Funktionsprinzip. Der Kapitalismus ist der Motor einer Wirtschaft.
Momentan läuft er unter Vollgas im roten Bereich, egal ob er sich selbst
zerstört oder der Wagen an die Wand fährt. Der Motor ist gut, aber es
fehlt das richtige Getriebe und die Steuerung.

Was somit fehlt, ist ein Wirtschafts- und Gesellschaftssystem, das das
Prinzip der Marktwirtschaft optimal einbindet und bei der optimalen
„Drehzahl“ hält.

### Das System

Was muss ein zukunftstaugliches System also beinhalten ? Zuallererst
muss es gelingen, eine möglichst grosse Zahl von Menschen an den
politischen Prozessen zu beteiligen. Eine stabile Gesellschaft kann sich
nur auf viele wache und interessierte Menschen stützen, die aktiv am
politischen Leben teilnehmen. Das in Deutschland etablierte System
bietet zwar verschieden Möglichkeiten

#### Mechanismen und Gesetze

Zusätzlich zu dem bekannten Prinzip der Gesetze sind übergeordnet
Mechanismen erforderlich. Sie legen grundlegende Strukturen fest, die
den Rahmen für die Entwicklung vorgeben und

Faktor 3 : Organ-ismus oder Soll und Muss
-----------------------------------------

Ein zentrales Prinzip ist der Gedanke des Organismus. Ein Organismus
funktioniert in gewisser Weise spielerisch, das Optimieren seiner
Funktionen wird von unzähligen Mechanismen geregelt, die wir trotz
leistungsfähiger Wissenschaft nur zu einem gewissen Teil bisher
entschlüsseln konnten.

Die Umsetzung : eine Firma als Startblock
-----------------------------------------

Die heute vorhandenen politischen Systeme sind auf Ebene von Staaten
manifestiert; auch wenn sie zu Beginn auch in kleineren Bereichen, so
auch Firmen angewendet wurden. Es wurde zügig versucht, sie auf einen
oder gleich mehrere Staaten (z.B. Weltrevolution des Kommunismus)
anzuwenden. Ihre rasante, oft auch mit Kriegen verbundene Verbreitung
hat jedoch ein ordentliches „Finetuning“ verhindert. Das vorherige
Establishment hat sich angepasst und/oder ein neues hat sich gebildet.
Und so hat sich in den meisten Fällen auch bei Verbesserung der
wirtschaftlichen und ökologischen Situation nichts Fundamentales
verändert.

Staaten haben einen fixen Grundriß, natürliche Gegebenheiten und ein
festes „Team“, nämlich die dort lebende Bevölkerung. Dies bietet für
eine gesellschaftliche Entwicklung zwar eine Konstanz und Sicherheit,
macht sie aber für äußere und innere Einflüsse viel angreifbarer als ein
Unternehmen.

Ein Unternehmen wiederum ist flexibel (oder kann so gestaltet werden,
wie es die Verlagerung von Produktionsstätten vorführt ), also kann es
negativ wirkenden Einflüssen eines Staates (also hauptsächlich von
seinen politischen und auch wirtschaftlichen Machtstrukturen ausgehend)
ausweichen da es die Dynamik des Geldes und auch von (anzunehmend)
engagierten Mitarbeitern nutzen kann. Dies gilt natürlich auch für
Einflüsse aus der Region und Anreihnerstaaten, die die
Unternehmensentwicklung über einen hinnehmbaren Level hinaus dämpfen.

Aus den vorangegangenen Kapiteln sollte klar sein: die erwähnte
Flexibilität ist nicht im selben Zusammenhang zu sehen wie bei einer
„normalen“ Firma. Sie ist ein notwendiger Notfallplan, ein
Rettungsanker. Es werden natürlich ab einem gewissen Punkt Geld,
Mitarbeiter und letztlich auch Firmenteile in andere Regionen oder
Länder verlagert aber nicht unter dem Gesichtspunkt der Kostensenkung
sondern unter dem Gesichtspunkt der Bereicherung und Vervielfältigung
des Konzeptes (immer aber mit der Direktive: die Qualität kommt vor der
Quantität). Bestehenden Standorten soll selbstverständlich die
Aufmerksamkeit und Investitionen zukommen, die ihr Wachstumspotential
optimal ausnutzt. Sie bedürfen der optimalen Pflege, um die Lebendigkeit
ihrer Strukturen zu gewährleisten. Sobald jedoch Überschüsse anfallen
(was bedeutet: Kapital und Mitarbeiter, die aktuell keinen lokalen
Gewinn oder Fortschritt schaffen können) ist eine Erweiterung des
Wirkungsfeldes naheliegend und letztendlich notwendig. Die Wahl wird
natürlich von vielen Faktoren beeinflusst: ein ausreichend gesichertes
Umfeld (im Sinne von politisch und sozial) ist ein Bedeutender davon;
also einerseits die Bereitschaft und Toleranz der Mächtigen
Entwicklungen zuzulassen oder gar zu fördern und andererseits der
sozialen Strukturen, dies zu ermöglichen.

Die Obersten Direktiven
-----------------------

Die Bezeichnung ist ganz bewusst der „Star Trek“-Serie entliehen: eine
gereifte Menschheit, die in fremden Welten unterwegs ist und durch diese
sicherstellen will, das Entwicklungen bei neu entdeckten Welten nicht
manipuliert werden und diese ihren, auch wenn teils schmerzlichen Gang
gehen können. Diese ersetzen in unserem Fall nicht die unveräusserlichen
Grundrechte der Menschen. Diese stehen als Konstanten fest und sind hier
keiner Diskussion unterworfen. Die Obersten Direktiven sollen den
sicherstellen, das die Entwicklung der Gemeinschaft/en die richtige
Richtung nimmt, ohne jedoch diese zu lähmen.

### I. Wandel vor Stagnation

Gerade in einer solch kritischen Situation, wie die Menschheit sich
befindet ist eine schnelle Entwicklung erforderlich. Deshalb ist eine
grosse Flexibilität und Anpassungsfähigkeit erforderlich, um die
Gesellschafts- und Wirtschaftsstruktur bestmöglich anzupassen. Dogmen
und Konstanten sind so gering wie möglich zu halten um eine möglichst
lebendige Entwicklung zu ermöglichen. Der Begriff „Stagnation“ wurde
jedoch bewusst gewählt um klarzustellen, das auch der Wandel kein
Selbstzweck ist: bewährtes und wirkungsvolles soll erhaltenbleiben, bis
es wirklich sinnvoll ersetzt werden kann.

### II. Qualität vor Quantität

Es ist ungemein wichtig, das zuerst die bestehenden Projekte eine gute
Stabilität erreichen bevor sich eine Gruppierung neuen Projekten annimmt
(seien es regionale, wirtschaftlich oder weltanschaulich begrenzte
Projekte). Dies ist so elementar, weil eine Verausgabung für ein neues
Projekt sehr schnell eine Erosion und Verfall beim Mutterprojekt nach
sich ziehen kann. Personell und zeitlich begrenzte Impulse bei
entsprechenden Gegebenheiten sind jedoch ebenfalls wünschenswert.

### III. Wirkung vor System

Die Konzeption in diesem Buch verfolgt den Zweck, für möglichst viele
Menschen eine lebenswerte Zukunft zu schaffen. Sie ist letztendlich nur
ein Werkzeug, das bei zukünftiger Verfügbarkeit eines Besseren angepasst
oder gegebenenfalls auch weggeworfen werden sollte. Allein die
Wirksamkeit zählt.

### IV. lokal vor global

Auch wenn die Probleme, vor der wir Menschen stehen global sind und auch
zum Teil globale Lösungen verlangen: die meisten Menschen leben und
denken eben doch in einem überschaubaren Bereich. Um einer Entwicklung
entsprechend Rückhalt zu geben ist eine möglichst grosse Unterstützung
der betroffenen Menschen erforderlich. Und dies bedeutet für viele
Menschen, das es bildhaft und auch nah erfolgen muss. Für globale
Entwicklungen bedeutet dies, das eine ausreichende Erdung der Bemühungen
vorhanden sein muss um einen Erfolg zu ermöglichen. Oder um das Bild
eines Faustkämpfers aufzugreifen: ein Schlag hat nur dann seine grösste
Wirkung, wenn er aus der Standfestigkeit des Fusses entsteht. Auch die
Anforderungen von Kon-fu-tse an einen funktionierenden Staat beschreiben
dies: nur gereifte Menschen und entwickelte kleine Einheiten können ein
Staatsgebilde oder ein grosses Unterfangen tragen.

### V. Integration statt Domination

Die Lebensweise und Kultur der Menschen soll so weit als möglich
erhalten bleiben, so sie den Entwicklungsprozess nicht zu stark
behindert. Verwurzelung und eigene Identität sind unbedingte
Anforderungen an eine lebendige Gesellschaft. Das System muss einer
Gruppe nicht aufgedrückt werden sondern übersetzt.

Leistungsgedecktes Regiogeld – ein kleiner Exkurs
-------------------------------------------------

Hier soll kurz beschrieben werden, wie ein leistungsgedecktes Regiogeld
aufgebaut sein kann und in der Praxis funktioniert.

### Begrifflichkeit

Ein leistungsgedecktes Regiogeld ist eine Tauscheinheit (materiell oder
elektronisch) für einen regionalen Markt, die ihren Wertgehalt aus einer
versprochenen oder schon erbrachten Leistung (Arbeit oder Ware) bezieht.

### Abgrenzung zum eurogedeckten Regiogeld

Eurogedecktes Regiogeld hat sich mehrfach in der Praxis bewährt. Es
erleichtert vor allem Unternehmern den Einstieg, die aus der
Tauschbarkeit in Euro leichter mit dem Regio vertraut werden. Auch
Verbraucher mit genügend Euro-Einnahmen finden hier sicherlich vorerst
einen leichteren Einstieg. Es gibt auf Dauer jedoch folgende
schwerwiegende Probleme:

-   beim eurogedeckten Regio wird die regionale Wirtschaft nur indirekt
    angeregt und zwar durch eine reale Zunahme an Handel und
    Dienstleistung.

### Deckung

Die Akzeptanz ist das Schlüsselelement; die Deckung ist in der Regel nur
ein Fundament für diese. Sie wird in der Regel durch die Arbeitskraft
der Teilnehmer und die Warenhaltung und Produktivität der Gemeinschaft
(z.B. Energie) dargestellt. Die Teilnehmer können auch Eigentum in die
Regionalgemeinschaft einbringen und dadurch mehr Geld schöpfen.

*Dies ist besonders bei Haus-, Grund- und Produktionsmittelbesitz ein
wichtiger Faktor, vor allem bei aufkommenden Krisenzeiten. Das so
geschöpfte Geld ist kein Kredit-Geld sondern Haben-Geld, das als
Grundstock der Gemeinschaft/Genossenschaft fungiert. Dies kann ein
Werkzeug sein die Spekulation auf Grundbesitz und Produktionsmittel zu
mindern indem diese in Gemeinschaftseigentum überführt werden und an die
Nutzer verpachtet werden.*\
''Ein eurogedecktes Regiogeld ist durch die hinterlegten Euro gedeckt,
dieser wiederum durch staatliche Maßgabe von jedem als Zahlungsmittel
akzeptiert zu werden. *Beim Leistungsgedeckten muß dies jedoch selbst
geleistet werden. Hier muß man nun zwischen Akzeptanz und Deckung
unterscheiden.* *Die Akzeptanz beschreibt die Bereitschaft, den Regio
anzunehmen und einen großen Anteil des eigenen Kapitalumsatzes darin zu
tätigen. Sie hängt davon ab*\
*a) in wie weit ich benötigte Waren und Dienstleistungen problemlos
damit erwerben kann*\
''b) wieviel Vertrauen ich in die Werthaltigkeit und deren Beständigkeit
setze. ''

### Wertdefinition

Der Wert des leistungsgedeckten Regios wir durch die Angabe der
**einfachen, ungelernen Arbeitszeit** angegeben und an diese gebunden.
Es sollte parallel auch ein Eurowert vermerkt sein, dieser ist jedoch
nur ein Anhaltspunkt und ist konkret nur zum Zeitpunkt der Drucklegung
gültig.\
*Der Wert wird also in Zeit angegeben. Warum aber nicht in ... . Also
muß ich diese Argumentation hier kurz führen:*\
''Geld wird eingenommen und ausgegeben. Wenn man nun die Ausgabenseite
betrachtet erhält man eine Vielzahl an Produkten und Leistungen, die man
durch einen Warenkorb darstellen kann. Dieser kann auch als
Grundversorgungswarenkorb dargestellt werden, aber auch dann enthält er
noch viele Komponenten. Ein Warenkorb ist zum Abgleich der Wertigkeit
eines Geldes sinnvoll eignet sich jedoch nicht zur Wertgebung. '' *Nun
könnte man dazu neigen, einzelne wichtige Artikel des Warenkorbs zu
verwenden; diese stellen jedoch dann wiederum nur einen Teil des
Warenkorbs dar und können in ihrer Wertentwicklung von ihm abweichen;
besonders bei einer Bewertung in kWh könnte dies zu einer Aufwertung
führen und somit Züge einer Deflation annehmen. Somit würde ich die
Ausgabenseite bei der Wertgebung nicht in Betracht ziehen.*

*Auf der Einnahmenseite ist neben dem Erzeugen von Produkten für die
meisten Menschen das Leisten von Arbeitszeit der beherrschende Faktor.
Dies ist sowohl bei Erwerbsarbeit der Fall als auch bei Dienstleistern
und Freiberuflern. Die Entlohnung kann mit einem Stundenlohn ausgedrückt
werden, also Geld pro Zeiteinheit. Somit liegt es nahe, den Wert eines
Regiogeldes in Zeit auszudrücken. Auch diese Bewertung kann sich
verschieben (eine Inflation in Zeiten einer Wirtschaftskrise mit
Massenarbeitslosigkeit z.B.) jedoch meines Erachtens nicht so schnell
und so massiv wie bei einzelnen Gütern da es in der Inflation sich nur
einer wie immer gearteten Armutsgrenze annähert die bei zu niedrigem
Niveau sowieso einen gesellschaftlichen Verfall erzeugen würde und somit
kein funktionierendes Wirtschaftssystem mehr als Raum hätte.*

*Auch auf der Einahmenseite könnte man nun wieder die kWh als Maßeinheit
erwägen; dies wäre jedoch nur umsetzbar wenn ein überwiegender Teil der
benötigten Energie der Region auch dort unter Kontrolle der Bürger
erzeugt würde. Die Notwendigkeit zum Zukauf würde die Bewertung des
Regionalgeldes mit Sicherheit verzerren, vor allem in einer sich
entwickelnden Krise. Hier gilt die Regel: Bewerte dein Geld mit nichts,
was du nicht in ausreichender Menge verfügbar hast*\
Als Zeiteinheit ist die Dauer von **6 Minuten** zu empfehlen. Sie
entspricht also 1/10 Stunde und weicht ca. um 30% von der Wertigkeit des
Euro ab (Süddeutschland). Als Bemessungsgröße mit Cent-Teilung und mit
einer gefühlten Nähe zum Euro-Wert kann damit gut gearbeitet werden.\
Begriffsvorschläge: Moment, Zentl, Deka, SeMin

### Umlaufsicherung

Die Umlaufsicherung von 5% wird zum Jahreswechsel beim Verruf der
Scheine fällig; bei eRegios auf Girokonten wird die Umlaufsicherung
tagesgenau berechnet. Die selbst geschöpfte Geldmenge ist davon nicht
betroffen, bei dem darüber hinausgehenden Betrag ist auch eine
Progression der Umlaufsicherung denkbar.

### Evolutionsstufen eines Geldes

Bezogen auf den Wirkradius eines Geldes kann man sich unterschiedliche
Qualitäten vorstellen. Dies wirkt sich auch auf die Gestaltung des
Geldes aus:

**1-25 Nutzer Ich-Geld**, einfaches Papier und Laserausdruck,
Handzeichen\
**25-150 Nutzer Wir-Geld**, Sonderpapier, hochwertiger Laserdruck mit
feinen Linien, Klebemarken des Herausgebers und der Mikro-Gruppe,
Signet\
**150-1000 Nutzer Lokal-Geld**, hochwertiges Sonderpapier und
aufwändiger Druck, ggf. Sicherheitsmerkmale, aufwändige Klebemarken als
Sicherheitsmerkmal ohne Signet\
**über 1000 Nutzer Regiogeld**, Papier, Druck und Sicherheitsstandard
auf Euro-Niveau, Wegfall der individuellen Kennzeichnung

#### Ich-Geld

Dies wäre die einfachste Form der Geldschöpfung: jeder schöpft sein
eigenes Geld, das heißt er/sie gibt als Bezahlung das Versprechen ab,
einen bestimmten Gegenwert/Leistung zu erbringen. Ein Wechsel, der
jederzeit fällig wird gegenüber dem der ihn vorzeigt; entweder in
Leistung, Ware, regionaler oder nationaler Währung. Wird das Ich-Geld
umfassend akzeptiert so nimmt es fast den Charakter eines regulären
Geldes an. Diese Form des Tauschmittels wird aber nur in kleinen,
vertrauten Kreisen einsetzbar sein.

#### Wir-Geld

Die erste Stufe zum Gemeinschaftsgeld. Das Geld wird von der Gruppe oder
vom einzelnen als Teil der Gruppe herausgegeben. Eine Variante wäre das
Mikro-Geld. Angelehnt an das Konzept der Grameen-Bank sind immer fünf
Menschen zu einer Schöpf-Gemeinschaft verbunden. Jeder kann die selbe
Summe schöpfen und steht dafür ein, falls jedoch ein Mitglied der Gruppe
seine geschöpfte Summe nicht ausgleichen kann bürgt die restliche Gruppe
dafür. Wie bei den Grameen-Banken wirkt hier die soziale Kontrolle der
Gruppe stabilisierend auf das gemeinsame Wirtschaften.

#### Lokal-Geld

Lokal-Geld kann in einer Gemeinde, einer Stadt oder einem Stadtviertel
herausgegeben werden. Die herausgebende Person tritt weiter in den
Hintergrund, wobei das Mikro-Geld-Prinzip beibehalten werden kann. Als
Garantiegeber können nun in verschiedenen Stufen weitere soziale
Verbindungen auftreten. Dies können Familienverbände, Vereine und
Kirchengemeinden für die Privatleute und Handwerksgilden, IHK und
Gewerbevereine sein. Dies wäre ein weiterer Schritt, auf den vorhandenen
sozialen und wirtschaftlichen Strukturen ein regionales
Wirtschaftssystem aufzubauen.

#### Regionalgeld

Als letzte Stufe in diesem Evolutionsmodell tritt nun das Regionalgeld
auf den Plan. In einer Region (in diesem Konzept als nachhaltig
funktionierende Einheit zur Versorgung ihrer Einwohner mit dem
Grundbedarf an Nahrung, Waren, Dienstleistungen und Energie zu
verstehen) funktioniert es als vollwertiges Geld das eine
Grundversorgung der Bevölkerung ermöglicht. Der Einzelne schöpft das
Geld nicht, er/sie bekommt es als Kredit-Geld von der Gemeinschaft. Auch
hier können 5er-Gruppen und Organisationen als Bürgen mit im Spiel sein,
sie treten jedoch (fast) vollkommen in den Hintergrund und spielen nur
noch im internen Ausgleich eine Rolle. Die Regiogeld-Gruppe legt die
Kriterien fest, nach der die Teilnehmer mit Geld ausgestattet und
bewertet werden. Dabei sollte ein nachhaltiger Score zum Tragen kommen,
der die Zuverlässigkeit und Kreditwürdigkeit des Einzelnen abbildet.
