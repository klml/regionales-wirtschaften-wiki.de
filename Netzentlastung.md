# Netzentlastung

Gerade regenerative [Energien](/energie "wikilink") sind schwerer
steuerbar als fossile Festbrennstoffe. Daher behaupten Kritiker „Wenn
der Wind nicht weht..., stehen alle Räder still!“. Abhilfe schaffen
unterschiedlichste [Energiespeicher](https://de.wikipedia.org/wiki/energiespeicher "wikilink"), aber
man kann und muss auch auf der anderen Seite des
[Lastprofils](https://de.wikipedia.org/wiki/lastprofil "wikilink") ansetzen. Und so eine aktive
[intelligenten
Stromverbrauch](https://de.wikipedia.org/wiki/intelligenter_stromverbrauch "wikilink") nicht nur
durch Großverbraucher sondern auch durch private Haushalte ermöglichen.

Die Schwierigkeit an der Stromversorgung ist nicht nur die Menge sondern
auch die richtige Zeit. Morgens, Mittags und Abends ist Strom am
begehrtesten und Nachts am wenigstens gebraucht.

Die aktueller Kratftwerkstruktur ist so mit Grund-, Spitzen- und einigen
Ausgleichskratfwerken bestückt das dieses Nachfrageprofil gedeckt werden
kann. Alternative Energieträger, wie vor allem Sonne und Wind haben aber
keine steuerbare Grundlast sondern folgen dem natürlichen Angebot.

Eine aktive **Netzentlastung** versucht diesen Spitzen auszuweichen wo
es sinnvoll und praktikabel ist, niemand soll im Dunkeln sitzen

Phasen
------

![aktuelles
Tages-Lastprofil](/lastprofil_vdew_winter_mit_kraftwerkseinsatz_schematisch.jpg "fig:aktuelles tages-lastprofil")
Es gibt drei Phasenrythmen

Tag - Nacht

:   Der wichtigste Lebenssrythmus zeichnet sich auch im Stromverbrauch
    ab. Die Phasen am Morgen, Mittag und Abends sind die wichtigsten
    Peaks die durch Netzentlastung einfach gemildert werden können.
    Waschen und sogar Kühlen lässt sich einfach aus der Abendphase in
    die Nacht oder den Nachmittag verschieben.

Werktag - Feiertag

:   Am Wochenende benötigt vor allem Industrie und Gewerbe weniger
    Strom. Hat man keine Möglichkeit unter der Woche vor- oder
    nachmittags oder nachts zu waschen, ist es Samstag Abend sinnvoller
    als Donnerstags um die gleiche Uhrzeit.(citation needed)

Sommer - Winter

:   Im Winter wird mehr Stromenergie zur Beleuchtung und Heizung
    verwendet und weniger zur Kühlung. Leider scheint die Sonne im
    Winter auch weniger.

natürliches Angebot: Wind Sonne

:   kann man Brennstoff und Wasserkraft recht gut lagern und zu den
    Spitzen aussteuern, so muss Sonne und Wind dann verstromt werden
    wenn sie anfallen. Verbrauch in windige und sonnige Zeiten zu
    verschieben funktioniert zwar kurzreistig, aber unstetig. So kann
    man, wenn man weiß das es morgen sonnig wird, das Waschen
    verschieben, aber nicht wenn 3 Wochen Regen zu erwarten sind.

Angebot
-------

Wann ist es, neben den sinnvollen Phasen, günstige Zeit um Strom
nachfragen und wann nicht?

-   Der [Grünstrom Index](http://mix.stromhaltig.de/gsi/) bietet eine
    “Berechnungsgrundlage um den Stromverbrauch an das Dargebot
    anzupassen, [lokale/regionale](/#region "wikilink") Gegebenheiten
    einzubeziehen, eine nachhaltige Marktintegration der Stromerzeugung
    aus Wind und Sonne zu gewährleisten, die verfügbaren
    Übertragungswege zu entlasten.” Es werden Zeiten für
    Versorgungsunterbrechung je nach Postleitzahl empfohlen ([zB
    München](http://mix.stromhaltig.de/gsi/lokal/markt/80331.html#wp)).
-   [netzentlaster.de](http://www.netzentlaster.de) aggregiert Daten aus
    [der eex und
    entsoe.eu](http://www.utopia.de/gruppen/erneuerbare-energien-203/diskussion/netzentlaster-de-203479#comment-283213)
-   [mit Hilfe des Wetters ein Teil der gesuchten Information und ich
    kann meine dafür geeigneten energiefressenden Batchprozesse
    anschalten.](http://www.abstract-life.de/HPM/Losungsansatz.html) vor
    allem [Regionen](/#region "wikilink") mit Wind- und Solarstrom genügt
    ein Blick aus dem Fenster
-   [Mit Produktionskosten von 15 bis 22 Cent je Kilowattstunde bleibt
    Biogas-Strom nach wie vor die teuerste erneuerbare
    Energie.](http://www.sueddeutsche.de/bayern/subventionen-laufen-aus-die-angst-der-biogas-bauern-1.2748751)
    aber die einzige sie speicherbar ist.

Verschiebepotential
-------------------

![miniatur|Verteilung des Stromverbrauchs in privaten Haushalten,
Ergebnisse einer Erhebung der Energieagentur NRW
2011](/verteilung_stromverbrauch_privatehh.png "fig:miniatur|verteilung des stromverbrauchs in privaten haushalten, ergebnisse einer erhebung der energieagentur nrw 2011")
Was kann man alles am [Bedarf an elektrischer
Energie](https://de.wikipedia.org/wiki/bedarf_an_elektrischer_energie "wikilink") verschieben?

Wärmepumpenheizung

:   [Wärmepumpenheizung](https://de.wikipedia.org/wiki/wärmepumpenheizung "wikilink") benötigen viel
    Strom und werden schon von den Netzbetreibern mit gesonderten
    Tarifen betreiben die eine
    [Sperrzeit](https://de.wikipedia.org/wiki/wärmepumpenheizung#sperrzeiten "wikilink") während der
    Peaks erlauben.

Wäsche waschen Geschirr spülen

:   ein Waschgang braucht [1
    kWh](http://presently.de/80/alltaegliche-dinge-kosten/#wasser-waesche),
    Geschirr spülen [1 und 1,5 Kilowattstunden je
    Spülgang](http://www.energieverbraucher.de/de/Zuhause/Hausgeraete/Geschirrspueler/site__1324/).

Kühlen

:   ein alter 150 Liter Kühlschrank verbraucht bei [166
    kWh/Jahr](http://www.thema-energie.de/strom/haushaltsgeraete/kuehl-gefriergeraete/ein-neuer-kuehlschrank-lohnt-sich-in-vielen-faellen.html)
    ca. 19 W. Ein sehr moderner Kühlschrank mit '60 kWh/Jahr'
    durchschnittlich **7 W** (realistisch vermutlich 10W). Um die
    Kühlung abzuschalten müsste man vorher die Temperatur senken um dann
    während der Ausschaltphase wieder an die Solltemperatur zu erwärmen.
    Das benötigt über die Laufzeit insgesamt [mehr
    Strom](http://www.eue24.net/pi/index.php?StoryID=317&articleID=207828)
    und eine auf Temperaturerhöhung und Auszeit optimierte Steuerung.
    Außerdem ist die Beleuchtung am Strom angeschlossen, was eine
    komplette Abschaltung per Zeitschaltuhr schwierig macht.

Akkus

:   Laden von Haushaltsakkus

    Notebook
    :   ein durchschnittlicher Akku hat 0,05 kWh.

    Mobiltelefon
    :   ein Smartphone ein Zehntel davon ca. 0,0048 kWh (3,7 V x 1,300
        Ah)

:   Fahrzeuge 

    E-Bikes
    : zwischen 0,25 und 1 kWh

    Elektroautos
    : haben zwischen [30 und 100 kWh Akkukapazität](https://de.wikipedia.org/wiki/Liste_von_Elektroautos_in_Serienproduktion) (VW e-Golf und Tesla) und können durch einen sinnvollen Ladezeitpunkt [netzstabilisierend](https://www.youtube.com/watch?v=5A6cNIl2cvk#t=9m27s) sein.


Im Vergleich sieht man dass einmal das Wäschewaschen (ca. 1kWh) zu
verschieben genauso effektiv ist wie

-   20 mal Notebook laden
-   200 mal Handy laden
-   100 Stunden Kühlschrank ausschalten (was man bei 2 Stunde morgens
    und 2 Stunden abends in ca. einem Monat erreichen kann)
-   2-3 mal das Ebike laden


Zum zwischenspeichern per Batterie wären dazu knapp zwei mittlere
[AutoStarterbatterie](https://de.wikipedia.org/wiki/starterbatterie#beispiele_für_die_kapazität_von_starterbatterien "wikilink")
nötig (mit ca. 50 Ah 12 V =\~ 0,6 Wh)

Mehr [tipps und tricks auf
netzentlaster.de](http://www.netzentlaster.de/tipps_und_tricks.htm)


Den VW e-Golf (das ist mit ca 33 kWh immer noch ein Drittel von einem Tesla) einmal laden, ist soviel:

-   600 mal Notebook laden
-   6000 mal Handy laden
-   3000 Stunden Kühlschrank ausschalten (das sind 4 Monate)
-   40-60 mal das Ebike laden
 

E-Autos sind daher ein willkommener "Verbraucher" für überschlüssigen Strom.
Man kann also die unregelbaren erneuerbaren Energien weiter ausbauen und abends um 7 nur das Licht anschalten, aber nachts um 2 den Tesla laden.



dezentrale verbrauchsartabhängige Batterie
------------------------------------------

[Batterie-Speicherkraftwerke](https://de.wikipedia.org/wiki/batterie-speicherkraftwerk "wikilink")
sind eine sehr zentrale Möglichkeit Strom zu puffern. Dezentralere
[Solarbatterien](https://de.wikipedia.org/wiki/solarbatterie "wikilink") speichern und verbrauchen
den Strom schon an einem Ort, sind aber noch teuer. Außerdem sind
Solarbatterien so konzipiert dass diese [netzstabilen
Strom](https://de.wikipedia.org/wiki/versorgungsqualität "wikilink") abgeben müssen um jedes Gerät
damit betreiben zu können.

Viel Strom aber nur mit einer geringen
[Stromqualität](/stromqualität "wikilink") oft für thermische Verbraucher
genutzt. Der Strombedarf von Durchlauferhitzern zum Duschen, Herde zum
Essen kochen können schwer in günstige Zeiten verschoben werden. Aber
ein einfacher Akku könnte den Strom puffern und den Batteriegleichstrom
ungerichtet, spannungsabfallend an den thermischen Verbraucher abgeben.
Geladen wird der Akku spät nachts oder am Vor oder Nachmittag.

Auch kann man diesen Pufferakku für eine durchschnittliche oder
Minimallast auslegen. Braucht man in Ausnahmefällen mehr, wird auf den
Netzstrom zurückgegriffen. Braucht man zum alltäglichen Duschen 1 kWh
und kann das aus dem Akku bedienen, so muss man für seltene Vollbäder
den Mehraufwand aus dem Netz nehmen.

Motivation: Idealismus oder Markt
---------------------------------

Netzentlastung kann und soll man aus ökologischer Verantwortung machen.
Aber Netzentlastung hat auch einen ökonomischen Vorteil, einen
grundsätzlichen Markteffekt, dass Angebot und Nachfrage sich mehr und
mehr angleichen. Daher soll der Verbraucher einen [variablen
Tarif](https://de.wikipedia.org/wiki/variable_tarife "wikilink") bezahlen der seinen Verbrauch in
eine Zeit mit höherem Angebot und geringere Nachfrage verschiebt.

Derzeit bekommen nur für größere Kunden mit [atypischer
Netznutzung](https://de.wikipedia.org/wiki/atypische_netznutzung "wikilink")
(Wärmepumpemheizungstarif) eine besseren Preis. Besitzer einer
Solaranlage partizipieren durch den günstigeren
[Eigenverbrauch](https://de.wikipedia.org/wiki/eigenverbrauch_(solarstrom) "wikilink") ebenfalls
davon wenn sie zu eigene sonnigen Zeiten Strom verbrauchen.

Um alle Stromkunden am Vorteil der Netzentlastung teilhaben zu lassen
benötigt man sinnvolle [Dezentrale_Smartmeter](/dezentrale_smartmeter "wikilink"), welche nicht
den Verbrauch durch die EVUs zentral regeln, sondern durch
Preisveränderung den Bedarf verändern.

[Das könnte etwa bedeuten, die Schwankungen der Strompreise - Letztere
sind besonders niedrig, wenn viel Ökostrom erzeugt wird - über variable
Stromtarife an Kunden weiterzugeben. Mit entsprechenden Zählern und
Geräten könnten sie Stromschlucker wie Wäschetrockner immer dann laufen
lassen, wenn Elektrizität besonders günstig
ist.](http://www.sueddeutsche.de/wirtschaft/energiewende-guter-ruf-schlechter-lauf-1.1925584)

[smartwatts.de - Ein dezentrales
Energiesystem](http://www.smartwatts.de/das-internet-der-energie/loesungsansatz.html)

ergänzende Lastprofile
----------------------

Das natürliche Angebote beeinflusst unser Nachfrage, . Aber es gibt
Bedarf der ist fast synchron zur Sonneneinstrahlung:

-   Die [Solare Klimatisierung](https://de.wikipedia.org/wiki/solare_klimatisierung "wikilink")
    kühlt dann wen die Sonne viel scheint. Baut man auf einem Kühlhaus
    eine Solaranlage kann man den erzeugten Strom fast ungeregelt im
    Kühlhaus verwerten, und nur die Kühlleistung für die Nächte aus dem
    Netz ziehen. Oder im Jahreszeitenverlauf, die höherer angefragte
    Sommerkühlleistung (auch für die Nacht gespeichert) durch
    Sommersonne abdecken und die Normallast auf ein Wintertagesprofil
    auslegen. Auch wenn die elektrisch nutzbare Lichtleistung der
    thermischen Leistung vor verschoben ist (Lichtpeak um 12 Uhr
    Wärmepeak um 15.00 Uhr), ein Kühlaggregat kann ja auch vor kühlen.
-   Blockheizkraftwerke können nur dann sinnvoll Strom produzieren, wenn
    sie auch Abnehmer für ihre [Nah-](https://de.wikipedia.org/wiki/nahwärme "wikilink") und
    [Fernwärme](https://de.wikipedia.org/wiki/fernwärme "wikilink") haben. Das ist meist im Winter
    oder, in der Zwischenjahreszeit, Nachts. Also die genau gegenüber
    liegende Lastphase einer Solaranlage.

Regionale Bedeutung
-------------------

Die derzeitige zentrale Strompolitik mit den vier großen Stromversorgern
betrachte alles nur in ihrem Maßstab und summiert gar nicht die
Einsparpotentiale bei den privaten und kleinen Verbrauchern. Außer ein
paar großmännische Dystopien einer Smartmeter-T-System-City gibt es
wenig.

Soll der Verbraucher die Energiewende schaffen müssen regionale
[Stadtwerke](/stadtwerke "wikilink") mit variablen Tarifen einen Markt
schaffen, der mit Netzentlastung und dezentraler Speicherung den Weg von
den materiellen fossilen Energieträgern zu den Regenerativen schafft.

Einführung
----------

Welcher EVU will so was schon einführen?

Aber z.B. ein großes Wohnhaus könnte damit auf dem Dach erzeugten
PV-Strom und im Keller erzeugten BHKW-Strom unter den Hausbewohnern
verkaufen. Denn in Mietskasernen kann bisher kein Strom eigenverbraucht
werden, da der Mieter keine PV-Anlage sich aufs Dach stellen kann. Der
Hauseigentümer entweder dort gar nicht wohnt, oder nur ganz wenig (im
Verhältnis zu großen Dachfläche) selbst verbrauchen kann.

Immer wenn eine solche Anlage Strom produziert und teurere als die
Einspeisevergütung im eigene Haus verkaufen kann, tut sie das. Sinkt der
Eigenbedarf unter die Schwelle der Einspeisevergütung, wird ins Netz
eingespeist.

Steigt der Bedarf und es wird zu wenig Strom produziert, steigt der
Preis für Eigenstrom bis kurz vor den Preis des EVU und dann liefert
auch der EVU Strom mit.

Weblinks
--------

-   [Ich rechne Euch den Strom jetzt nach der Uhrzeit
    ab](http://www.heise.de/tp/news/Ich-rechne-Euch-den-Strom-jetzt-nach-der-Uhrzeit-ab-1992754.html)

<Kategorie:Energie> <Kategorie:Netz>
