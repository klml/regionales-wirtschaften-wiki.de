# LUNA-XML

**LUNA-XML** ist die technische Beschreibung der für
[LUNA](/luna "wikilink")

Andere ähnliche Sprachen und Austuschhstandards sind
[Geldsprachen](/geldsprachen "wikilink")

XML Schema
----------

ist eine Empfehlung des W3C zum Definieren von Strukturen für
XML-Dokumente.

XML-Struktur
------------

[LUNA-XML-Struktur-Beispiel](/luna-xml-struktur-beispiel "wikilink")

<Kategorie:LUNA-XML>
