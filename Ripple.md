# Ripple

**[Ripple](https://de.wikipedia.org/wiki/ripple_(zahlungsverkehr) "wikilink")** ist ein
[Open-Source](/open-source "wikilink") Software-Projekt zur Entwickelung
und Implementierung eines Protokolls für ein offenes [dezentrales
Zahlungsverfahren](/dezentrale_finanzsysteme "wikilink"). Mit Ripple
können Finanztransaktionen in einem Peer-to-Peer Verfahren ausgetauscht
werden. Dabei können nicht nur klassische Währungen, sondern auch
Alternativen wie Regiogelder ohne eine Zentralbank getauscht werden.

Derzeit besteht Ripple aus einer Client- und einer Serversoftware bei
welcher über eMail-Adressen identifizierte Benutzer untereinander führen
können und sich so gegenseitig Schulden anschreiben und so Überweisungen
tätigen können. Zum Schutz vor Spamüberweisungen nutzt das System eine
eigene Währungseinheit, XRP genannt, von der jeder Nutzer einige wenige
Einheiten als „Reserve“ zurückhalten muss. Ausprobieren kann man das
System unter [rippletrade.com](http://www.rippletrade.com).

Generell ist es möglich eine eigene Instanz des Ripple-Netzwerks zu
installieren und mit ein wenig Aufwand auch mit dem „großen“
Hauptnetzwerk zu verbinden. Somit wäre über Ripple unter anderem auch
ein dezentraler Handel mit Regionalwährungen möglich.

Weblinks
--------

-   [ripple.sourceforge.net](http://ripple.sourceforge.net)
-   Original Konzept: [“Money as IOUs in Social Trust Networks & A
    Proposal for a Decentralized Currency Network
    Protocol”](http://ripple.sourceforge.net/decentralizedcurrency.pdf)
-   [Payment as a Routing
    Problem.](http://ripple.sourceforge.net/paymentrouting.pdf) Ansporn
    für das Ripple Projekt.
-   [Ripple protocol development wiki](http://ripplep2p.com/wiki/),
    containing the protocol drafts.
-   [mailingliste](https://lists.sourceforge.net/lists/listinfo/ripple-protocol)
    und [group](http://groups.google.com/group/rippleusers) for those
    interested in the nitty-gritty of the protocol.

