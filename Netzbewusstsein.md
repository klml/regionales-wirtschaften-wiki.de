# Netzbewusstsein

**Netzbewusstsein** bedeutet, sich der eigenen Eingebundenheit in die
netzartigen Strukturen dieser Welt bewusst zu sein.

Unsere Welt ist durchdrungen von netzartigen Strukturen. Nahrungsnetze
stellen die Grundlage des Überlebens jeder höheren Art dar.
Verkehrsnetze wie Straßen- und Eisenbahnnetz und Kommunikationsnetze wie
Telefon und [Internet](/internet "wikilink") prägen die menschliche
Lebensweise. Ein ganz besonderes Netz ist das Netz menschlicher
Beziehungen. Jeder Mensch ist Mittelpunkt seines persönlichen
Beziehungsnetzes, aber er steht über seine Bekannten mit weiteren
Menschen in Beziehung. Über dieses Beziehungsnetz verbreiten sich Viren
ebenso wie Ideen. [“Six degrees of
separation”](http://de.wikipedia.org/wiki/Kleine-Welt-Ph%C3%A4nomen)
wird das Phänomen genannt, daß von jedem Menschen zu jedem anderen auf
diesem Planeten maximal 6 Zwischenschritte nötig sind. Auf diesem Weg
ist also jeder von uns genauso mit George W. Bush und Osama bin Ladin
verbunden wie mit allen anderen unserer Spezies.

Auf wirtschaftlicher Ebene zeigt sich netzbewusstes Handeln
beispielsweise daran, dass man als Konsument die Geizmentalität beiseite
legt, weil einem klar ist: Wenn ich die Preise drücke, so nimmt mein
Gegenüber weniger ein. Damit muss jedoch auch er sparsam sein und der
eigene Geiz setzt sich über Preisdruck im
[Wirtschaftsnetz](/wirtschaftsnetzwerk "wikilink") fort. Dabei wird zu
oft vergessen, daß der Lohn der Preis für menschliche Arbeit ist und
auch dieser sinken muss, wenn alle
[Wirtschaftsakteure](/akteur "wikilink") beim Einkauf geizen. Ganz
ähnlich verhält es sich mit Fragen der
[Regionalität](/regionalität "wikilink"): Wenn ein Akteur in einer Region
Geschäfte machen will, so sollte ihm daran gelegen sein, daß es den
anderen Wirtschaftsakteuren in der Region gut geht. Denn: Wenn sie kein
Geld haben, können sie nicht bei ihm einkaufen. Mit etwas mehr
Netzbewusstsein sollten regional agierende Wirtschaftsakteure also
lieber bei anderen regionalen Akteuren einkaufen, selbst wenn es etwas
mehr kostet. Sie legen damit die Grundlage für zukünftige gute
Geschäfte.

<Kategorie:Netz>
