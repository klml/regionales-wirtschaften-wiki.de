# Vorschlag Neustart Trägermedium

Es gibt die Idee, als fälschungssicheres Medium für Geld CD-Rohlinge zu
verwenden.

CD-Rohlinge sind im Rohzustand billig und relativ weit verbreitet. Man
könnte als (beim Ausfall von komplexen Strukturen) Rohlinge mittels
Aufruf einsammeln und von der geldausgebenden Stelle beschreiben und
beschriften lassen. Der Code auf der CD kann relativ fälschungssicher
gemacht werden.
