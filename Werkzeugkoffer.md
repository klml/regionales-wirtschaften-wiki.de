# Werkzeugkoffer

Angesichts rasanter Entwicklungen in den weltweiten Finanzsystemen
stellt sich die Frage nach einem *Werkzeugkoffer*, der zum schnellen
Einsatz eines Regionalgeldes hilfreich ist.

Grundlegendes zur AG Krisenkonzepte
-----------------------------------

Die AG Krisenkonzepte hat sich im Herbst 2008 zusammengefunden, um
kontinuierlich die Arbeit von zwei vorhergegangenen Workshops zu dem
Thema fortzuführen.

Ihr Ziel ist es, Vorkehrungen und Maßnahmen für eine sich andeutende
Krise im globalen Geld- und Wirtschaftssystem aufgrund des Wissens und
der Erfahrungen der Regiogeldbewegung und ihrem Umfeld zu sammeln und zu
erarbeiten. Eine [Krise](/krise "wikilink") ist dann vorhanden, wenn die
reguläre Arbeit der Regiogeld-Aktiven und das gesunde Wachstum der
Initiativen die Nachfrage und den Bedarf nach komplementären
Geldkreisläufen und regionalen Wirtschaftsstrukturen nicht mehr erfüllen
können.

Eine Kernaufgabe der AG liegt darin, einen “Werkzeugkoffer Regiogeld”
anzulegen, der die Inhalte des Regiohandbuchs und zusätzliche Strategien
zu einem kurzen, klaren Handlungsleitfaden zusammenfaßt und den
Initiativen und auch Laien einen möglichst schnellen Weg zu einem
handlungsfähigen, intakten Regiogeld aufzeigt. Auch jetzt schon soll die
AG wichtige Anregungen aufnehmen und entwickeln die helfen können,
Regiogelder und verwandte Initiativen “krisenfest” zu machen. Ergänzend
ist auch das Thema “Krisen innerhalb von Initiativen” ein mögliches
Thema der AG.

Die AG arbeitet zusammen auf einer Wiki-Plattform, um dort gemeinsam den
“Werkzeugkoffer” zusammenzustellen.

Sie wird aktuell von Norbert Rost (Lausitzer) und Jörn-Derek Gehringer
(Die Ortenauer) koordiniert und besteht aus mehreren Menschen die im
Regionalgeld-Bereich aktiv sind und das Thema weiterentwickeln wollen.

Kontakt: Norbert Rost
&lt;norbert.rost\[at\]regionales-wirtschaften.de&gt; , Jörn-Derek
Gehringer &lt;jdg\[at\]die-ortenauer.de&gt;

Informationen & Ansprechpartner
-------------------------------

Um ein Regiogeld möglichst schnell umzusetzen, sind grundlegende
Informationen für den Organisator notwendig:

-   Wie wirkt Regiogeld?
-   Welche Argumente gibt es für seinen Einsatz?
-   Welche Menschen und Institutionen sind in einer Region als
    Mitstreiter hilfreich?

Infomaterial
------------

Um andere über das Werkzeug Regiogeld zu informieren bzw. zur Mitarbeit
zu motivieren, sind Info-Materialien für diese Menschen nützlich.

### Bücher

-   Handbuch des Regiogeld-Verbandes, verfügbar für Verbandsmitglieder
    im [internen Bereich](http://www.regiogeld.de/intern.html)
-   Bereich Regionalwährungen/Regionalgeld auf
    [Regionales-Wirtschaften.de](http://www.regionales-wirtschaften.de/4.25.0.0.1.0.phtml)

### Filme

-   Der Geist des Geldes
-   Neuland

### Sonstiges

...

Roadmap
-------

### Aktuelle Handlungvorschläge (in Bearbeitung)

-   Da fast alle Regiogelder eurogedeckt oder zumindestens
    europaritätisch bewertet sind wäre es wichtig, die Wertscheine mit
    einer zweiten Wertangabe zu versehen um das Abkoppeln im Ernstfall
    zu erleichtern und dann kein Chaos zu erleben. Als Empfehlung hat
    sich beim letzten Workshop die Definition über eine einfache
    Arbeitsstunde (ca. € 7,6 - 8,4 ? ) herauskristallisiert. Diese
    Einheit ist für die Menschen leicht zu erfassen, ein Warenkorb hilft
    vermutlich nur zum Abgleich im Hintergrund und nicht zur Definition
    und für eine kWh haben wir in den Initiativen noch keine
    ernstzunehmende Energieproduktion verfügbar.

Eine detailierte Empfehlung soll noch erarbeitet werden.

-   Als weiterer Punkt wäre noch zu hinterfragen, in wieweit jede
    Initiative im Bezug auf Änderungen des Umtauschkurses, Bewertung und
    der Umlaufsicherung reagieren kann. Es ist vermutlich noch Zeit, das
    in Ruhe zu entscheiden aber eine Position in eurer Satzung wäre ggf.
    schon sinnvoll; wir werden in der AG versuchen, Empfehlungen zu
    erarbeiten.

Danke an Philipp Heist aus Freiburg, der uns auf die Aktualität des
Themas nochmal angestoßen hat.

1.  Überblick verschaffen
2.  Mitstreiter gewinnen
3.  Region-spezifische Info-Materialien erstellen
4.  Organisation erschaffen
5.  Netzwerk aus Unternehmern knüpfen
6.  Regiogeld emittieren
7.  Regio-Netzwerk betreuen

Organisation & Organisationsform
--------------------------------

### Aufgaben

Um ein Regiogeld in Betrieb zu bringen und in Betrieb zu halten ist die
Erfüllung folgender Aufgaben notwendig:

-   Bildung und Akquise
-   Buchhaltung
-   Koordination
-   Öffentlichkeitsarbeit/Materialerstellung
-   Vertragsgestaltung

### Personen

Auch wenn es einzelne Regio-Initiativen gibt bzw. gab, die sich auf eine
einzelne Person stützen, so ist für eine langfristige Ausgestaltung eine
Gruppe von Personen besser geeignet. Dann hängt das Projekt nicht an
einer Person, die, wenn sie ausfällt, das Gesamtprojekt in Gefahr
bringt. Ein ideales Team besteht aus mindestens folgenden Personen:

-   Pädagoge
-   Buchhalter
-   Manager
-   Designer/Journalist
-   Jurist

Einzelne notwendige Aufgaben können natürlich von extern eingekauft
werden oder durch eine Zusammenarbeit mit (beispielsweise) Medienbüros
oder Steuerberatern erbracht werden.

### Ressourcen

Regiogeld basiert entweder auf Gutscheinen oder auf einem elektronischen
Verrechnungssystem oder auf beidem. Für Gutscheine ist die Gestaltung
der Scheine und eine Druckerei zu ihrer Herstellung notwenig. Für ein
elektronisches System muß eine entsprechende Software verfügbar sein:
[Cyclos](/cyclos "wikilink") ist eine Software, die einerseits auf einer
eigenen Webpräsenz installiert oder bei entsprechenden Dienstleistern
gemietet werden kann. Der elektronische Betrieb setzt die Verfügbarkeit
von Strom voraus.

Todo Werkzeugkoffer
-------------------

Nach der informellen Aufarbeitung des Koffers in diesem Wiki wäre eine
“Materialisierung” sinnvoll:

-   die genannten Filme auf DVD brennen
-   die genannten Bücher auf Papierform besorgen
-   die genannte Software auf DVD brennen
-   die verlinkten Texte drucken

und alle Bestandteile in “echten Koffern” realisieren

<Kategorie:Werkzeugkoffer>
