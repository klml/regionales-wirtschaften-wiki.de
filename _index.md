Das __Regionales WirtschaftenWiki__ ist ein Wiki-Projekt für den Gedanken, Ansätze und Projekte für eine Neuorientierung unserer Wirtschaftsweise. Unsere Maxime sind dezentral, demokratisch, menschennah und umweltfreundlich. Die Inhalte dürfen frei kopiert und verbreitet werden. Jeder kann sein Wissen einbringen, sei mutig – die ersten Schritte sind ganz einfach! Falls noch weitere Fragen bestehen wende dich einfach an einen der Administratoren.

Projekte & Ideen
----------------

In einem Wiki lässt sich gut brainstormen:

[Regionales Wirtschaften](/regionales_wirtschaften "wikilink")
:   konzentriert wirtschaftliche Aktivitäten auf geografisch
    überschaubare Regionen, in deren Mittelpunkt der Mensch und seine
    Umwelt steht.

[Regionalgeld](/regionalgeld "wikilink")
:   ist ein Zahlungsmittel, welches zusätzlich zu den bestehenden
    nationalen und kontinentalen Zahlungsmitteln regional eingesetzt
    wird.

[Mittlere Technologien](/mittlere_technologien "wikilink")
:   „Mittlere Technologien sind technologische Konzepte die es erlauben,
    mit kleinen Stückzahlen auch im Rahmen regionaler
    Wirtschaftskreisläufe ökonomisch sinnvoll zu handeln.“ - Dr.
    Reinhard Stransfeld

[Peak Oil](/peak_oil "wikilink")
:   Igendwann ist das Öl aus. Dieses Drama aus Monopol und Abhängigkeit
    braucht lieber schnell als langsam eine Umbau der
    Vorsorgungssysteme. Dezentral, Einfach und Nachaltig

[Dezentrale Finanzsysteme](/dezentrale_finanzsysteme "wikilink")
:   behandelt eine Betrachtung der derzeitigen Währungssysteme und eine
    Alternative werterhaltend wie Regionalgelder aber schnell und
    innovativ wie das [Internet](/internet "wikilink").


[Dezentrale Smartmeter](/dezentrale_smartmeter)
: Erneuerbare Energien verlangen nach variablen Tarifen und diese werden meist mit Smartmetern realisiert.
Smartmetern sind teuer, schlecht im Datenschutz und fehleranfällig.
Ein dezentrale Smartmeter soll nur Stromleistung mit einem aktuellen Preis multipliziern diesen aktuellen Preis nur summieren.