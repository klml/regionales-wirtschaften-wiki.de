# Parteien

Eine Sammlung neuerer oder unterbeleuchteter Parteien, von denen es
jüngst immer mehr zu geben scheint:

-   <http://www.piratenpartei.de>
-   <http://www.die-violetten.de>
-   <http://www.die-kreativen-partei.de>
-   <http://www.die-guten-partei.de>

In dem Zusammenhang ist die Kritik beachtenswert, die gegenüber Parteien
in einem Telepolis-Artikel zu Fragen der Verfassung der Bundesrepublik
geäußert wird:

-   <http://www.heise.de/tp/r4/artikel/30/30424/1.html>

<Kategorie:Politik>
