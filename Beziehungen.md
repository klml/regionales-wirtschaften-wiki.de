# Beziehungen

Beziehungen bestehen zwischen Menschen auf vielfältige Art und Weise.
Auf ökonomischer Ebene formen Geschäftsbeziehungen das Beziehungsnetz.
Immer dann, wenn ein Akteur mit einem anderen in einen wirtschaftlichen
Austausch tritt (meist verbunden mit einem Geld- und Leistungsfluss)
entsteht eine neue Beziehung oder wird eine vorhandene Beziehung
gestärkt.

Wirtschaftliche Beziehungen werden sehr stark vom
[Vertrauen](/vertrauen "wikilink") zwischen den
[Akteuren](/akteur "wikilink") geprägt.

<Kategorie:Theorie>
