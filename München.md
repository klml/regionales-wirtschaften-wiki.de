# München

Was gibt es zum Thema Regionales Wirtschaften in **München**. München
hat auch in \[wiki\](http://www.muenchenwiki.de) zur dezentralen
Informationssammlung.

1.  1.  Communities

-   \[rewig-muenchen.de\](http://rewig-muenchen.de) Genossenschaft für
    [Regionalgeld](/regionalgeld "wikilink")
-   [Urbaner Gartenbau](https://de.wikipedia.org/wiki/urbaner_gartenbau "wikilink")
    \[o-pflanzt-is.de\](http://o-pflanzt-is.de)
-   Gut ausgestattete Werkstätten zum Seber arbeiten für Metall, Holz
    etc \[hei-muenchen.de\](http://www.hei-muenchen.de) oder speziell
    für Fahrräder in der \[bikekitchen.de\](http://bikekitchen.de)
-   [Freifunk](https://de.wikipedia.org/wiki/freifunk "wikilink") ist eine Initiative zum Aufbau und
    Betrieb selbstverwalteter lokaler Computer-Netzwerke als freies
    Funknetz: \[freifunk-muenchen.de\](http://freifunk-muenchen.de)

1.  1.  regionale Wirtschaftbetriebe

-   \[aquamonaco.com\](http://aquamonaco.com) Softdrinks mit wenig
    Zucker. Mineralwasser, Tonic, Ginger

1.  1.  Verkehr

Der [Fahrradverkehr in München](https://de.wikipedia.org/wiki/fahrradverkehr_in_münchen "wikilink")
ist mittelmäßig. In Nebenstraßen kann man gut fahren, auch in vielen
Einbahnstraßen darf man mit dem Rad einfahren. Aber *Radlhauptstadt*,
wie die STadt kolpotiert, ist München noch lange nicht. Es gibt die
schönen Prachtstrecken entlang der Isar, aber leider sind auf
Hauptverkehrswege Radler nur geduldet. [Neuralgische
Stellen](http://problemstrassen.sueddeutsche.de) wie die Rosenheimer
Straße oder die Lindwurmstraße sind seit Jahren ein Dauerbrenner

Öffentlicher Personennahverkehr macht die
\[MVG\](http://www.mvg-mobil.de) mit U-Bahn, Tram und Bus. Auserdem die
S-Bahn.

Autofahren geht in München, ganz gut, ausser man hat es eilig oder
keinen Privatparkplatz. \[Carsharing in
Münchnen\](http://www.muenchenwiki.de/wiki/Carsharing) hat die üblichen
Anbieter wie drive now, aber auch richtige Anbieter
\[Stattauto\](http://www.stattauto-muenchen.de)

<Kategorie:Regionen>
