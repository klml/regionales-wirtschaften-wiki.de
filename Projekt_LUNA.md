# Projekt LUNA

[LUNA](/luna "wikilink") ist noch längst nicht fertig. Derzeit geht es
darum eine Basis für die Datenstruktur zu finden, auf die dann die
eigentliche Software aufgesetzt werden kann. Interessant sind die
Eigenschaften, die solch ein Ansatz bieten würde, der über die
XML-Dateien verfolgt werden kann. Diese Eigenschaften würde sicher nicht
jeder Wirtschaftsakteur gut finden, aber es gilt das Prinzip der
Freiwilligkeit: Wer sich mit den Regeln anfreunden kann, kann das System
nutzen, wer nicht will, darf sich frei fühlen, ein eigenes System zu
entwickeln.

ToDo
----

-   [LUNA-XML](/luna-xml "wikilink") definieren

<Kategorie:LUNA-XML>
