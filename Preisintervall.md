# Preisintervall

Der Preisintervall ist der Abstand zwischen der Preisvorstellung des
Anbieters und der Preisvorstellung des Nachfragers. Überschreitet das
Limit des einen Anbieter das des anderen, wird kein Tausch zustande
kommen.

Das Preisintervall ist beiden Tauschpartnern unbekannt. Durch Verhandeln
und Feilschen kommt es zur Preisbildung. Beiden Tauschparteie versuchen
dabei ein jeweils größeres Stück des unbekannten Tauschvorteils zu
bekommen.

Weil beide Seiten (Anbieter und Nachfrager) einen Vorteil im Tausch
haben (Sie haben “besser” getauscht als sie ursprünglich bereit waren,
maximal einzugehen) sind beide Seiten nur selten bereit
zurückzutauschen.

Der Wert einer Sache ist daher nicht objektiv, sondern vom Subjekt
(=Betrachter) abhängig. Die in der ökonomischen Theorie weit verbreitete
Ansiche, dass der Wert einer Sache daher im [Preis](/preis "wikilink")
liege, zu dem eine Sache getauscht wurde, unterliegt einem Irrtum. So
wie es nicht “durchschnittlich warm” ist, wenn ein Fuss im Kohlebecken
und einer im Eiswasser steht, so ist auch der [Wert](/wert "wikilink")
einer Sache nicht endgültig durch den Tauschpreis zu erklären.

Diese Schwierigkeit wurde von der [österreichischen
Schule](/österreichischen_schule "wikilink") als subjektivistische
Wertlehre beschrieben,

<Kategorie:Geld>
