# Akteur

Akteure sind innerhalb des Wirtschaftssystems agierende Menschen oder
Organisationen. Jeder, der am Wirtschaftssystem als Konsument oder
Produzent teilnimmt oder andere die Wirtschaft betreffende Aktivitäten
vollzieht, ist als Akteur zu betrachten. Zwischen Akteuren bestehen
[Beziehungen](/beziehungen "wikilink"). Die Gesamtheit aller Akteure und
den zwischen ihnen bestehenden Beziehungen formen ein
[Wirtschaftsnetzwerk](/wirtschaftsnetzwerk "wikilink").

Akteure können Individuen sein, [Unternehmen](/unternehmen "wikilink")
oder andere [Organisationen](/organisationen "wikilink").

### Funktion innerhalb des Wirtschaftssystems

Ein Wirtschaftsakteur ist die kleinste wirtschaftliche Einheit. Ihr
Willen und ihr Handeln führt (beeinflußt von den [Regeln des
Wirtschaftssystems](/regeln_des_wirtschaftssystems "wikilink")) zu den
Eigenheiten und der Entwicklung des
[Wirtschaftssystems](/wirtschaftssystems "wikilink").

<Kategorie:Theorie>
