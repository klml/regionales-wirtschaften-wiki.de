# Geldschöpfung

Geldschöpfung (derzeit)
-----------------------

Die Zentralbanken und Geschäftsbanken stellen in ihrer unendlichen
Weisheit und Güte dem Markt “die richtige Menge” Geld zur Verfügung.

Geldschöpfung (Vision)
----------------------

Aus Sicht des Netzwerkgedankens ist Geld von jedermann machbar: Geld
ist, was von anderen als Geld anerkannt wird. Stelle ich einen
Schuldschein aus, der seinem Inhaber verspricht, ihn bei mir für eine
Gegenleistung einzutauschen, so habe ich *Geld gemacht*. Denn dieser
Schuldschein kann von all jenen, die mich kennen und mir und meiner
Leistungsfähigkeit vertrauen, als Zahlungsmittel benutzt werden. Der
jeweilige Besitzer des Schuldscheines kann zu seinem Geschäftspartner
sagen: “Ich bezahle dich mit diesem Schein, den du jederzeit bei Norbert
einlösen kannst.” So lange der jeweilige Empfänger mir vertraut und auch
seine Geschäftspartner mir vertrauen, können sie das Papier nutzen, um
untereinander Leistungen auszutauschen und zu wirtschaften.

Prinzipiell ist dieses Vorgehen *jedermann* möglich! Jeder kann
Schuldscheine erstellen, die er und andere als Zahlungsmittel verwenden.
Wichtig für das Funktionieren solch eines Systems ist das Vertrauen in
den Schuldner. Dabei stellt jeder ausgestellte Schuldschein eine
zweiseitige Beziehung dar:

-   der Besitzer des Scheines hat Anspruch auf Leistungen
    (Leistungsguthaben)
-   der Herausgeber des Scheines hat die Pflicht zur Leistungserbringung
    (Leistungsschuld)

Jeder dritte, der davon ausgeht, dass entweder der Leistungsschuldner
oder ein anderer den Schein anerkennt, kann den Schein ebenfalls
akzeptieren.

### Geld ist Vertrauen

Aus Sicht des Netzwerkgedankens ist Geld von jedermann machbar: Geld
ist, was von anderen als Geld anerkannt wird. Stelle ich einen
Schuldschein aus, der seinem Inhaber verspricht, ihn bei mir für eine
Gegenleistung einzutauschen, so habe ich *Geld gemacht*. Denn dieser
Schuldschein kann von all jenen, die mich kennen und mir und meiner
Leistungsfähigkeit vertrauen, als Zahlungsmittel benutzt werden. Der
jeweilige Besitzer des Schuldscheines kann zu seinem Geschäftspartner
sagen: “Ich bezahle dich mit diesem Schein, den du jederzeit bei Norbert
einlösen kannst.” So lange der jeweilige Empfänger mir vertraut und auch
seine Geschäftspartner mir vertrauen, können sie das Papier nutzen, um
untereinander Leistungen auszutauschen und zu wirtschaften.

Schöpfung von Warengeld
-----------------------

Jeder Gutschein für eine bestimmte Menge Ware, der nicht an eine
bestimmte Person, sonder an den Überbringer des Gutscheins gebunden ist
stellt Geld in einer ursprünglichen Form dar.

Schöpfung von Schuldgeld
------------------------

Konkret lief solch ein Vorgehen bei uns wie folgt ab: Von einem
befreundeten Tischler liessen wir uns ein Bücherregal bauen. Den
Rechnungsbetrag akzeptierte er zu 20% in einer Art Schuldschein: Dieser
Schuldschein existierte nie real auf Papier, sondern nur als
Verrechnungsposten in einer Excel-Tabelle. Jedoch akzeptiert ein
befreundeter Webdesigner ihn als Zahlungsmittel, so dass unser Tischler
in absehbarer Zeit seine Internetpräsenz von diesem Webdesigner machen
lassen wird. Der Designer wiederum kann mit dem Schuldschein meine
Fähigkeiten für dieses oder andere Webprojekte einkaufen.

überträgt man dieses Prinzip auf grössere Strukturen sowie ins Internet,
so wird deutlich:

-   jeder kann Geld machen
-   jeder ist frei, eigenes oder fremdes Geld als Zahlungsmittel
    anzuerkennen
-   jeder ist frei, mehr als ein Zahlungsmittel zu nutzen
-   jeder, der ein Geld akzeptiert, wird Teil des jeweiligen
    Geld-Netzwerkes; also des Netzwerkes jener Akteure, die ein
    bestimmtes Zahlungsmittel nutzen

Deutlich sollte werden, dass unser Geldverständnis um eine Ebene
erweitert werden muss:

-   man besitzt Geld nicht mehr nur in einer bestimmten Menge
    (Quantität)
-   sondern auch in einer bestimmten “Ausprägung” (Qualität)

Denn wenn “jedermann” Geld machen kann, so wird es wichtig zu wissen,
*welches Geld* man in *welcher Menge* besitzt und *welche Akzeptanten*
für dieses Geld leisten.

Aus Sicht des Netzwerkgedankens ist Geld von jedermann machbar: Geld
ist, was von anderen als Geld anerkannt wird. Stelle ich einen
Schuldschein aus, der seinem Inhaber verspricht, ihn bei mir für eine
Gegenleistung einzutauschen, so habe ich Geld gemacht. Denn dieser
Schuldschein kann von all jenen, die mich kennen und mir und meiner
Leistungsfähigkeit vertrauen, als Zahlungsmittel benutzt werden. Der
jeweilige Besitzer des Schuldscheines kann zu seinem Geschäftspartner
sagen: “Ich bezahle dich mit diesem Schein, den du jederzeit bei Norbert
einlösen kannst.” So lange der jeweilige Empfänger mir vertraut und auch
seine Geschäftspartner mir vertrauen, können sie das Papier nutzen, um
untereinander Leistungen auszutauschen und zu wirtschaften.

<Kategorie:Geld>
