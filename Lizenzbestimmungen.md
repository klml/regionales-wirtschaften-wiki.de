# Lizenzbestimmungen

<div style=" font-size: 1.4em; line-height: 1.4em;  border: 2px solid #6B6B6B; margin 10px; padding: 10px; background-color: #F2F2F2; "><!-- http://i.creativecommons.org/l/by-sa/2.0/de/88x31.png -->Dieser Werk ist unter der <b>Creative Commons-Lizenz</b> <a href="http://creativecommons.org/licenses/by-sa/2.0/de/">Namensnennung-Weitergabe unter gleichen Bedingungen 2.0 Deutschland</a> lizenziert.</div>


Sie dürfen:

* das Werk vervielfältigen, verbreiten und öffentlich zugänglich machen
* Bearbeitungen des Werkes anfertigen

Zu den folgenden Bedingungen:

* Namensnennung. Sie müssen den Namen des Autors/Rechteinhabers in der von ihm festgelegten Weise nennen (wodurch aber nicht der Eindruck entstehen darf, Sie oder die Nutzung des Werkes durch Sie würden entlohnt).
* Weitergabe unter gleichen Bedingungen. Wenn Sie dieses Werk bearbeiten oder in anderer Weise umgestalten, verändern oder als Grundlage für ein anderes Werk verwenden, dürfen Sie das neu entstandene Werk nur unter Verwendung von Lizenzbedingungen weitergeben, die mit denen dieses Lizenzvertrages identisch oder vergleichbar sind.


Im Falle einer Verbreitung müssen Sie anderen die Lizenzbedingungen, unter welche dieses Werk fällt, mitteilen. Am Einfachsten ist es, einen Link auf diese Seite einzubinden.

* Jede der vorgenannten Bedingungen kann aufgehoben werden, sofern Sie die Einwilligung des Rechteinhabers dazu erhalten.
* Diese Lizenz lässt die Urheberpersönlichkeitsrechte unberührt.
