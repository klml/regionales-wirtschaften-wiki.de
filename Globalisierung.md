# Globalisierung

**Globalisierung** ist der Prozess, der planetenumfassende Strukturen
ausbildet. Er zeigte sich bislang am deutlichsten in wirtschaftlichen
Wirkungen und resultiert daraus, daß Unternehmen teilweise global
agieren und ausgeprägte internationale Unternehmensstrukturen aufgebaut
haben. Globalisierung hinterläßt jedoch auch enorme Spuren im
Verkehrsbereich (Reisen ist global möglich), Kommunikationsbereich
(Kommunikationsnetze umfassen den ganzen Planeten) und im menschlichen
Bewusstsein (der Klimawandel wird als globales Phänomen wahrgenommen).
Auf politischer Ebene entstehen derzeit vor allem im Bereich der
[Nichtregierungsorganisationen](http://de.wikipedia.org/wiki/Nichtregierungsorganisation)
(“NGOs”) supranationale Strukturen, die künftig eine neue globale
politische Struktur mitprägen werden.

[Regionalisierung](/regionalisierung "wikilink") kann als Parallel- und
Ausgleichsprozess zur Globalisierung verstanden werden. Es ist der
Versuch, unterhalb der Globalität Substrukturen zu schaffen oder neu zu
definieren. Dabei ist die Auflösung nationaler Strukturen zugunsten
regionaler Strukturen denkbar.
