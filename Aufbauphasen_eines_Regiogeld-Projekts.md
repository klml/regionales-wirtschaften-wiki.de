# Aufbauphasen eines Regiogeld-Projekts

1.  1.  Phasen eines Regiogeld-Projekts

Der Aufbau eines Regiogeldes ist nicht trivial. Die Entwicklung eines
Regiogeld-Projekts läßt sich in verschiedene, nicht strikt voneinander
abgrenzbare Phasen einteilen:

Ideenfindung

:   Suche des Namens, der Region, eines Grobkonzept und aktiver
    Mitstreiter

Strukturierungsphase

:   Gründung einer Organisation, Ausfeilen des Konzepts, Anwerbung der
    ersten Akzeptanten, Öffentlichkeitsarbeit

Emissionsphase

:   Herausgabe des ersten Regiogeldes

Vernetzungsphase

:   Suche nach neuen Akzeptanten, Beratung der vorhandenen Akzeptanten

Stabilisierungsphase

:   professionelle Netzwerkarbeit, Geldmengensteuerung, Kreditvergabe

<Kategorie:Regionalgeld>
