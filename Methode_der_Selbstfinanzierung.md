# Methode der Selbstfinanzierung

**Die Methode der Selbstfinanzierung aus der Sicht eines Ingenieurs**

Die Methode der Selbstfinanzierung beruht auf dem Grundsatz, dass ein
neues Produkt bereits während seiner Entwicklungszeit von Anfang an in
Form eines funktionierenden Prototypen Erträge erwirtschaftet. Die
Vorgehensweise besteht darin, dass das neu entwickelte Produkt möglichst
frühzeitig in Form eines kleinen funktionierenden Prototypen realisiert
wird. Der Prototyp wird an einen Nutzer verkauft, der mit diesem
Prototypen Erträge erwirtschaftet. Durch diese Erträge sind die Kosten
des Prototypen gedeckt. Aus der Betriebserfahrung mit dem ersten
Prototypen und dem intensiven Kundenservice kann ein nächstgrößerer
funktionierender Prototyp entwickelt und realisiert werden, der wiederum
Erträge liefert. Ganz wichtig ist hier, dass der Prototyp klein genug
ist, damit er nicht finanziell und technologisch außer Kontrolle gerät!

Das Vorbild der Methode der Selbstfinanzierung ist das Keimen von
Saatgut. Ein Samenkorn besitzt in seinem Inneren meist Fett oder Stärke
als Energieträger. Das Samenkorn enthält genug Energie, damit es keimen
kann. Sobald die entstehende Pflanze die ersten grünen Blätter ins Licht
der Sonne streckt, gewinnt sie die Energie, die sie zum Wachsen benötigt
selbst.

**Zweck der Methode der Selbstfinanzierung**

Durch die Methode der Selbstfinanzierung ist es möglich, mit minimalen
Investitionen Produkte zu entwickeln, die auf dem Markt auch benötigt
werden.

Die Methode der Selbstfinanzierung wird nur dann möglich, wenn ganz zu
Beginn ein Kunde gefunden wird, der einen funktionierenden Prototypen
kauft. Ein Kunde wird einen funktionierenden Prototypen nur dann kaufen,
wenn er ihn auch gebrauchen kann. Der Kunde kann der Prototypen nur dann
gebrauchen, wenn dieser auf seine individuellen Bedürfnisse
zugeschnitten ist. Praxisnähe ist hier einer der wichtigsten Grundsätze.
Auf diese Weise wird verhindert, dass Produkte entwickelt werden, die in
der Praxis nicht gebraucht werden können.

**Investitionen und Investoren**

Auch für die Methode der Selbstfinanzierung sind Investitionen
notwendig. Da aber Finanzkapital und Güter oft knapp sind, sollte die
Investition von Finanzkapital so weit wie möglich vermieden werden.
Darum wird bei der Methode der Selbstfinanzierung hauptsächlich
Arbeitszeit und Know-How investiert. Ist ein Kunde gefunden, der einen
funktionierenden Prototypen kaufen möchte, wird nach Unternehmen
gesucht, die als Zulieferer von Komponenten für den Prototypen in Frage
kommen. Alle beteiligten Unternehmen und Zulieferer investieren
Arbeitszeit und Know-How in das neue Produkt. Das Know-How investiert
jeder beteiligte in Form seiner eigenen Komponenten für den Prototypen.
Der Gewinn aus den Investitionen für jeden einzelnen Beteiligten sind
neue Absatzmöglichkeiten der eigenen Produkte. So hat jeder einzelne
Beteiligte Interesse am Erfolg des funktionierenden Prototypen. Alle
Beteiligten treten hier als Investoren auf, die in die Entwicklung des
Produktes direkt eingebunden sind.

<Kategorie:Konzepte>
