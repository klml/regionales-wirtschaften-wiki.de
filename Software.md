# Software

Um [LUNA-XML](/luna-xml "wikilink") auszutauschen, also Bankverkehr zu
betreiben, könnte man sich die Bestände und Transaktionen auch per und
notieren und austauschen; so haben Jahrhunderte lang Banken und
Buchhalter gelebt. Aber der Bankbereich ist einer der welcher am meisten
Software und Netzwerktechnik nutzt (afaik früher und umfassender als das
Internet).

Client
------

-   ist eine freie Finanzsoftware zur Verwaltung für Privatpersonen oder
    kleine Firmen.

-   Webclient (), der per Browser auf den Geldserver zugreift.

Server
------

### Datenbank

-   Transaktionssicher

weblinks
--------

-   [complementarycurrency.org -
    ccSoftware](http://www.complementarycurrency.org/software.html)

<Kategorie:Praxis>
