# Mobilitätseffizienz

Die Ansätze des 20. Jahrhunderts, wie **Mobilität** zu organisieren sei,
sind hochgradig ineffizient. Ein effizienterer Ansatz wäre
beispielsweise der Umbau der Autoindustrie zu einer Mobilitäts-Domain,
die statt des Automobilverkaufs die Bereitstellung von Mobilität
gewährleistet.

Effizienter wäre unter Umständen:

-   das Teilen von Autos wie [CarSharing](/carsharing "wikilink") oder
    [Fahrgemeinschaften](/fahrgemeinschaft "wikilink")
-   die Integration von Bahn-, ÖPNV- und Taxi-Diensten in einen
    ganzheitlichen Verkehrsverbund
-   [Netzlogistik](/netzlogistik "wikilink")

<Kategorie:Logistik>
