# Dezentralisierung

Gegenpol zur Zentralisierung ist die **Dezentralisierung**. Sie bedeutet
die Verteilung und Verkleinerung von Strukturen statt Konzentration und
Vergrößerung.

Prozesse und Strukturen, in denen Dezentralisierung sichtbar ist
----------------------------------------------------------------

-   [Dezentrale Finanzsysteme](/dezentrale_finanzsysteme "wikilink")
-   Energieversorgung: [Kalte
    Fusion](http://www.heise.de/tp/r4/artikel/29/29969/1.html) wird
    ebenso dezentral eingesetzt wie Solar- Wind- oder Wasserenergie
-   das [Internet](/internet "wikilink") dezentralisiert die Medien:
    Jeder tritt als Sender und Empfänger auf
-   [Regionalisierung](/regionalisierung "wikilink") als
    geografischer/politischer/ökonomischer Dezentralisierungsprozess

Vorteile
--------

-   Ein wesentlicher Vorteil der Dezentralisierung liegt in der
    grösseren Stabilität dezentraler Systeme. Selbst bei Fehlern (und
    Zusammenbrüchen) in Teilsystemen ist die Gesamtstruktur nicht
    gefährdet. Vielmehr sind Teilzusammenbrüche sogar sinnvoll, um die
    evolutionäre Dynamik zu erhalten. Aus den “Fehlern der anderen”
    lernt man, das eigene System zu optimieren. Vielleicht wird dieser
    Aspekt deutlicher, wenn man sich die Anfälligkeit von Monokulturen
    für Schädlinge und Krankheiten ansieht.
-   Durch die geringe Entfernung zu den Nutzern sind kostengünstigere
    Gesamtlösungen möglich, da Transportaufwand wegfällt.

Warum wurden die dezentrale Strukturen aufgegeben?
--------------------------------------------------

In den 1950er und 1960er Jahren waren in vielen Bereichen Strukturen üblich,
die wesentlich dezentraler gestaltet waren, als die heute üblichen
Systeme. In den vielen Fällen waren diese Strukturen gegenüber
industriellen, zentralen System noch nicht wirtschaftlich
konkurrenzfähig. Die aus kurzsichtigen Gründen aufgegebenen Strukturen
werden heute jedoch über ihren rein wirtschaftlichen Nutzen als wertvoll
gesehen. So erfüllt z.B. ein Nahversorger in kleinen Ortschaften nicht
nur logistische Funktionen, sondern stellt auch ein wesentliches
soziales Zentrum dar.
