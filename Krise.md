# Krise

Die derzeitige [Bankenkrise](/bankenkrise "wikilink") und kommende
[Finanzkrise](/finanzkrise "wikilink") wird in mehreren Bereichen massive
Auswirkungen auf die Regionalwährungen haben - zahlreiche Konzepte, die
bisher angewandt wurden, werden sich als wirkungslos, wertlos oder sogar
kontraproduktiv erweisen.

Die wesentlichen Themenkreise sind:

[Deckung in der Krise](/deckung_in_der_krise "wikilink")
-------------------------------------------------------

-   wie kann der Wert des Geldes definiert werden; über Zeit, Warenkorb,
    [Energie](/energie "wikilink") ...
-   Welche Definition wird am besten greifen ?
-   wie wird die [Geldschöpfung](/geldschöpfung "wikilink") gesteuert ?
-   Wie wird sich der Bedarf der Wertbestimmung / Deckung entwickeln ?
-   wie kann die Liquidität in der Region erhalten werden ?
-   Wie spielen regionale Unterschiede eine Rolle ?
-   oder wie ist das [Clearing](/clearing "wikilink") zwischen den
    verschiedenen Regios zu leisten ?

[Skalierbarkeit in der Krise](/skalierbarkeit_in_der_krise "wikilink")
---------------------------------------------------------------------

Wenn das Vertrauen in konventionelle Strukturen kippt, dann ist mit
einem irrationalen Ansturm zu rechnen, der mit den derzeitigen
Ressourcen (und Mechanismen) nicht bewältigt werden kann.

### [Skalierbarkeit der einzelnen Regiogelder](/skalierbarkeit_der_einzelnen_regiogelder "wikilink")

Macht/Einfluss/Entscheidungsfindung wie kann / muß sich die lokale
Regiogeldgruppe entwickeln, um ihrer zunehmenden Verantwortung gerecht
zu werden und ihre Aufgaben erfüllen zu können ? wie kann sichergestellt
werden, das bei einer Währungskrise und der dabei zunehmenden Nachfrage
nach Regiogeld dies auch geleistet werden kann ?

### [Skalierbarkeit und Krisenkonzepte - Regiogeld e.V.](/skalierbarkeit_und_krisenkonzepte_-_regiogeld_e.v. "wikilink")

Kommunikation notwendige Infrastruktur wie kann dabei möglichst effektiv
neuen lokalen Gruppen Hilfestellung gegeben werden ? wie kann der
Regiogeld e.V. die Initiativen unterstützen und selbst aktiv an den
Prozessen teilnehmen ? wie ist das Clearing zwischen den verschiedenen
Regios zu leisten ?

### [Denkanstösse](/denkanstösse "wikilink")

Diese Rubrik dient dazu, um sich gedanklich mit den möglichen
Umwälzungen vertraut zu machen.
