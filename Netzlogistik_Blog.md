# Netzlogistik Blog

Aktuelles zum Thema [Netzlogistik](/netzlogistik "wikilink")

2020
----

* [lieferlotse.de](https://lieferlotse.de) Mitfahrzentrale für Dinge

2018
----

-   [pickshare.de](http://pickshare.de) startet
-   [So könnten in Umschlagzentren am Stadtrand Paketsendungen der
    verschiedenen Lieferdienste gebündelt werden und im Anschluss
    einzelne Straßenzüge nur noch durch einen Transporter angefahren
    werden.](https://www.hessenschau.de/politik/paketdienste-cdu-will-weniger-lieferwagen-in-frankfurter-innenstadt,cdu-frankfurt-lkw-raus-100.html)

2017
----

-   [PassLfix](http://pacifics.org/) ein p2p Logistiker mit Blockchain

2016
----

-   [deliverix.net wird zum 29. Februar 2016
    eingestellt.](http://deliverix.net)

2015
----

-   [Denn in solch dünn besiedelten Regionen würden sich Paketzustellung
    oder Busverkehr alleine nicht rechnen. Anders sähe es aus, wenn
    Güter und Menschen zusammen transportiert würden – wie in der
    Postkutsche.](http://www.zeit.de/2015/50/demografie-landleben-infrastruktur-internet-anschluss-doerfer/komplettansicht)
-   [deliverix.net - Alle Paketdienstleister in einem Paketshop
    vereint](http://deliverix.net) startet Ende 2015 in München

2014
----

-   ~~[Open Postal Alliance](http://www.open-postal-alliance.de)~~
    (gegründet im Mai) zur *Förderung der offenen Zugänglichkeit von
    Netzwerken für Postdienstleistungen sowie dem dazugehörigen Wissen.*
-   [sueddeutsche.de - Geldstrafe für eiligen
    Paketzusteller](http://www.sueddeutsche.de/muenchen/muenchner-amtsgericht-geldstrafe-fuer-eiligen-paketzusteller-1.1959404)
    “Bei Mehrfamilienhäusern fehle schlichtweg der Platz für die
    Paketkästen, die dort in großer Zahl aufgestellt werden müssten...”
-   [sueddeutsche.de - Post plant
    XXL-Briefkästen](http://www.sueddeutsche.de/geld/paketboxen-post-plant-xxl-briefkaesten-1.1911211)
    “Ab Mai kann sich jeder Kunde einen solchen Kasten vor das Haus
    stellen und dort Pakete und Päckchen vom Paketboten ablegen lassen”,
    aber “Allerdings sollen die Kästen nur von Paketboten der Post und
    nicht von der Konkurrenz wie etwa UPS bedient werden.” soll sich
    jeder Empfänger 8 solcher Kästen in den Vorgarten stellen?
-   [sz-magazin.sueddeutsche.de Die Gewissensfrage: Muss ich Pakete für
    meine Nachbarn annehmen, wenn die tagsüber nicht da
    sind?](http://sz-magazin.sueddeutsche.de/texte/anzeigen/41481/Die-Gewissensfrage)
    Hilft es da wirklich nur auf den Kommunitarismus zu vertrauen?

2013
----

-   [internetworld.de Die letzte
    Meile](http://www.internetworld.de/Heftarchiv/2013/Ausgabe-26-2013/Die-letzte-Meile)
    (20.12.2013) herkömmliche Anbieter für die letzte Meile
-   [sueddeutsche.de - Treppauf,
    treppab](http://www.sueddeutsche.de/bayern/paketdienste-in-der-weihnachtszeit-treppauf-treppab-1.1847197),
    neben den gesellschaftlichen Arbeitsbedingungen auch der Vorschlag:
    “Darum denkt die Branche über Abendlieferungen nach.”
-   [Lieferstatus: Empfänger ist mit den Nerven
    fertig](http://www.sueddeutsche.de/geld/zustellpraxis-von-paketdiensten-lieferstatus-empfaenger-ist-mit-den-nerven-fertig-1.1837901)
    (7. Dezember 2013) Zustellpraxis von Paketdiensten und das Problem
    der Letzten Meile

2012
----

-   [wuv.de - Studie: Zusammenarbeit von Händlern und Herstellern lohnt
    sich](http://www.wuv.de/nachrichten/unternehmen/studie_zusammenarbeit_von_haendlern_und_herstellern_lohnt_sich)
    (14.05.2012) “Die Umfrage ergab außerdem, dass Kooperationen rund um
    Bedarfsplanung, Logistik und Prozessoptimierung an Bedeutung
    zunehmen. Der digitale Projektbereich, der derzeit noch keine Rolle
    in der Zusammenarbeit spielt, wird von den meisten Befragten jedoch
    als Topthema für die Zukunft genannt:...”

2009
----

-   [excitingcommerce.de - Netzwerte 09: Netzartige Logistik für den
    Versand realer
    Güter](https://excitingcommerce.de/2009/07/26/netzlogistik/)
    [Netzlogistik](/netzlogistik "wikilink") kommentiert auf
    excitingcommerce

2008
----

-   [crn.de - Grüne Supply Chains: virtuelle Bazare und Champignonzucht
    im LKW](http://www.crn.de/panorama/artikel-14743.html) (13.10.2008)
    “So soll es bei der Zusammenarbeit künftig vermehrt gemeinsame
    Liefernetzwerke geben, um halb leere LKWs und mehrere Lieferfahrten
    zum selben Empfänger zu vermeiden.”
-   [Paketversand: Der Hinkefuß des
    Online-Shoppings](http://www.spiegel.de/netzwelt/web/0,1518,580380,00.html)
    (26.09.2008) Datenpakete sind rasend schnell, ihre stofflichen
    Pendants dagegen nicht.

