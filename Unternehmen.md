# Unternehmen

Aus wirtschaftssystematischer Sicht ist jedes Unternehmen ein
[Akteur](/akteur "wikilink") innerhalb des
[Wirtschaftsnetzwerkes](/wirtschaftsnetzwerk "wikilink"). Unternehmen
stehen mit [Lieferanten](/lieferanten "wikilink"),
[Kunden](/kunden "wikilink"), [mitarbeitern](/mitarbeitern "wikilink") und
anderen die Unternehmensaufgabe unterstützenden Akteuren in Beziehung
(z.B. [Banken](/banken "wikilink")). Intern sind Unternehmen als
[Netzwerk](/netzwerk "wikilink") organisiert, wobei jeder Knoten des
unternehmensinternen Netzwerkes seine
[Mitarbeiter](/mitarbeiter "wikilink") sind. Die Struktur innerhalb eines
Unternehmens wird durch die betriebswirtschaftliche “Organisation”
([1](http://de.wikipedia.org/wiki/Organisation#Organisation_in_der_Betriebswirtschaftslehre))
beschrieben: Das unternehmensinterne Netzwerk ist meist hierarchisch
organisiert mit einem managenden Akteur an der Spitze.

<Kategorie:Theorie>
