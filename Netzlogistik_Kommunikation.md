# Netzlogistik Kommunikation

Die **Kommunikation** in der [Netzlogistik](/netzlogistik "wikilink")
zwischen einem der Transportnachfrager/bedürftigen/suchenden und einem
Anbieter von Transportmöglichkeit/leistung benötigt eine Sprache, die
einen einfachen, schnellen und maschinellen Austausch von Angebot und
Nachfrage erlaubt.

Der Sender muss seinen Auftrag in einem standardisierten
[Kommunikationsprotokoll](https://de.wikipedia.org/wiki/kommunikationsprotokoll "wikilink") zur
Begleitung der Sendung veröffentlichen können. In diesem müssen
Eigenschaften wie Herkunft, Ziel, Masse, Eigenart, maximale Lieferdauer
etc. definiert sein. Auch das festlegen der Route und die Übergaben sind
einer Teil der begleitenden Kommunikation und ermöglichen erst das
Springen über Anbieter hinweg wenn diese in einem Kommunikationsstandard
definiert sind.

Schichten
---------

In Anlehnung an das [OSI-Modell](https://de.wikipedia.org/wiki/osi-modell "wikilink") kann man das
multikontraktonalen Modell der Lieferkette in einer Schichtenarchitektur
darstellen.

Konvention

:   ![](/gap_signet.png "fig:gap_signet.png"){width="50"} eine
    multikontraktonalen Logistik ist ganz ohne IT möglich. Geht man
    bisher davon aus das ein Transporteur eine Sendung von Anfang bis
    Ende transportiert, müsste man sich nur per Konvention einigen das
    “Netzlogistik”-Sendungen auch Teilstrecken transportiert werden
    dürfen, so lange sie bei einem offenen Hub abgelegt werden. Es würde
    genügen Sendungen mit einem Logo zu kennzeichnen und jeder
    Transporteur trägt ein Paket immer ein Stück weiter zum Ziel. Der
    offene Buchtauschring [Bookcrossing](https://de.wikipedia.org/wiki/bookcrossing "wikilink")
    funktioniert zu aller erst auch ohne Tracking, nur per Konvention.

Sendungsverfolgung

:   um zu wissen wo ein Paket ist und wer im Verlust- oder
    Verzögerungsfalle angefragt werden muss, kann eine Sendung mit einer
    [Sendungsverfolgung](https://de.wikipedia.org/wiki/sendungsverfolgung "wikilink") nachvollzogen
    werden und am Zielort kann der Empfänger so wissen wann er das Paket
    annehmen kann oder muss. Dabei muss nur jeder Trans­por­teur die
    Übernahme einer Sendungen mitschreiben, am besten elektronisch für
    den Empfänger einsehbar.

Bezahlung

:   Trans­por­teure können für ihre Teilstrecke auch bezahlt werden.
    Auch wenn die erste und die letzte Meile vom Absender oder Empfänger
    bezahlt wird, die Zwischenschritte müssen auch bezahlt werden
    können. In einem Sendungsverfolgungsportal kann für eine Sendung,
    wenn diese “fest hängt”, geboten werden, diese auf die nächste
    sinnvolle Teilstrecke mit zu nehmen.

Clustern

:   um Sendungen nicht auf allen Teilstrecken einzeln routen zu müssen,
    muss man diese bündeln können. Eine Sendung von einer Straße in Sylt
    nach Garmisch, kann in Hamburg in einem Bündel Richtung München
    gepackt werden, das dann über Hannover und Frankfurt routet, falls
    sich eine Direktverbidung nicht lohnt und in München wieder
    entbündelt werden.

Funktion
--------

Welche Funktion muss ein Protokoll erfüllen:

Auftragsausschreibung

:   der Versender muss formulieren was, von wo nach wohin, Eigenschaften
    der Sendung und erwünschten Eigenschaften der Transporteure und
    Zeitwünsche
    -   ID (evtl. als URL)
    -   Status: ausgeschrieben, vergeben
    -   Standort
    -   Zielort
    -   Vertrauensstufe
    -   Formfaktoren
    -   maximaler und auch minimaler Lieferzeitraum

Angebot für Teilstrecken

:   Transporteure können Transporte vom Zwischen- oder Ausgangspunkt zu
    einem frei vorzuschlagenden Zwischenzielpunkt wählen.
    -   ID
    -   Bezug zu welcher Ausschreibung (ID)
    -   Wann Abholung
    -   möglicher Teilzielort
    -   Kosten

Zuverlässigkeitsprüfung

:   Hat der Transporteur das notwendige Vertrauen und kann dieses
    nachweisen (Zertifikat)

Teilauftragsannahme

:   der Agent des Versender prüft die 'Angebote für Teilstrecken' und
    sagt diese dem Trans­por­teur zu und veröffentlicht dies im
    ursprünglichen Auftrag.

Bestätigung der Übergabe

:   Bestätigung an jedem Punkt der [Übergabe](/#übergabe "wikilink") bis
    zum Zielpunkt. Dannach erfolgt eine neue Auftragsausschreibung.
    Ausser die Sednung hat ihr Ziel erreicht.

Routing
-------

![Das Internet ist
selbst-strukturiert...](/internet_map_1024.jpg "fig:das internet ist selbst-strukturiert...")
![... eine Transportnetzstruktur geplant
strukturiert.](/mehrnaben.jpg "fig:... eine transportnetzstruktur geplant strukturiert.")

Welche Route eine Sendung nimmt wird in der Netzlogistik nicht mehr
durch eine festgelegte
[Transportnetzstruktur](https://de.wikipedia.org/wiki/transportnetzstruktur "wikilink") oder vom
einem Disponenten festgelegt, sondern muss “sich selbst” organisieren.
Auch wenn der Sender die erste Strecke noch in die richtige Richtung
auswählt, ab dem ersten Wechsel muss entschieden werden wie es
weitergeht.

Diese kann man von Anfang an planen, und jeden Transporteur vorab
definieren. Der Sender oder ein Beauftragter steuert die Sendung, in dem
**vor** dem Versenden an jedem Hub den sinnvollsten Auftrag (Preis,
Geschwindigkeit, Zielrichtung) annimmt. In Rechnernetzen definieren
Routen in kleinen Netzen so mit [Source
Routing](https://de.wikipedia.org/wiki/source_routing "wikilink"). Auch Zugreisen plant man so: Der
Fahrgast kennt alle Verbindungen bis zum Schluss. Oft aber muss man
um-planen und dann an einem Knotenpunkt aufgrund der neuen Situation
anders verfahren.

Eine Alternative, ähnlich der
[Teilstreckenvermittlung](https://de.wikipedia.org/wiki/teilstreckenvermittlung "wikilink"), ist es
erst bei der Ankunft an einem Zwischenpunkt entscheiden wie es
weitergeht. U-Bahnfahren funktioniert so: Man geht zur Station und
wartet auf “die Nächste”, an der Umsteigestation die nächste Passende
(z.B. auf Strecken die in Kernbereichen parallel befahren werden). Oder
man entscheidet sich sogar, falls die Möglichkeit besteht, erst am
nächste Bahnhof für die eine oder andere Route. Im Prinzip ist das auch
schon sehr ähnlich zu den jetzigen Verfahren in der KEP Logistik.
Niemand kann genau sagen wann ein Paket genau ankommt. Zwar kann man
eine gut schätzbaren Wert angeben. Wenn aber ein Paket versendet wird,
weiß der dritte, vierte oder letzte Transporteur noch nicht ob und wann
er das Paket transportiert.

Es ist nur die Frage **wer** entscheidet wie es weitergeht. Entweder
entscheidet diese ein virtueller Dispatcher, oder jeder Knoten
entscheidet selbstständig wohin es geht.

Nutzt man für eine Sendung einen zentralen
[Dispatcher](https://de.wikipedia.org/wiki/dispatcher "wikilink"), vom Sender oder Empfänger
beauftragt, so erhält dieser bei jeder erfolgreichen Übergabe in eine
Lagerstätte eine Nachricht und kann dann anhand von Dringlichkeit und
Verfügbarkeit von Transportressourcen einen nachfolgenden Auftrag
anfragen. Hier liegt die Verantwortung welcher Anbieter zu welchen Preis
beauftragt wird in der Hand des Dispatchers. Allerdings auch der Aufwand
die jeweils optimale Verbindung zu erkennen. Eine zentrale
Kontrollinstanz ist aber abhängig von der lokalen Information über
Transportmöglichkeiten am Übergabepunkt.

Besser wäre es, der Übergabepunkt kennt die Entscheidungsregel (wie
schnell, wie gut) der Sendung und trifft diese Entscheidung selbst. Auch
diese lokalen Routingentscheidung wird niemals perfekt sein, aber je
nach
[Routingverfahren](https://de.wikipedia.org/wiki/routing#die_verfahren_im_einzelnen "wikilink"),
besser als eine statische Route.

Und es gibt Konzepte für [Selbststeuerndes Routing in
Transportnetzen](http://www.offis.de/fileadmin/Chefredakteur_files/PDFs/Veranstaltungen/OFFIS-Tag_2008/scholz-reiter.pdf)

Eine Sendung aus Hamburg soll in die Hauptstraße 3b in München und kommt
in einem Lager in Frankfurt am Main an: Ob die Strecke über Nürnberg
oder Stuttgart schneller ist oder die vielleicht eine teurere
Direktverbindung nach München notwendig ist, kann nur anhand von lokalen
Routingregel, basierend auf Erfahrungswerten, erfolgen. Entscheiden kann
das auch ein virtueller Dispatcher am Zwischenstopp.

Um zu verhindern dass in eine ideale Route nicht eine Schleife zur
Kosten-Erzeugung eingebaut wird, benötigt man wie für den Transporteur
ein Vertrauensnetz. Hat ein Übergabepunkt zu wenig Vertrauen für eine
Sendung darf diese schon gar nicht angefahren werden.

Weiterhin gibt es Transportnetzstrukturen, aber diese sind nun
auto-organisiert.

Übergabe
--------

Die Übergabe zwischen zwei Teiltransporteuren ist nun juristisch und
kaufmännisch jeweils eine eigene Lieferung. Der erste Teiltransporteur
übergibt die Sendung an den Nachfolgenden und lässt sich den
Zwischenpunkt auf einem [Lieferschein](https://de.wikipedia.org/wiki/lieferschein "wikilink")
bestätigen und schickt diesen an den Sender. Gleichzeitig reicht er beim
Sender das Übernahmeprotokoll des nächsten Transporteur ein, welcher
sich wiederum mit einem Lieferschein aus der Kette erfolgreich
verabschiedet.

Auch wenn man dieses Verfahren mit klassischen Papier und Unterschrift
machen könnte. Sinnvoll skalieren kann so ein System nur mit
elektronischen Nachrichten und [digitaler
Signatur](https://de.wikipedia.org/wiki/digitale_signatur "wikilink").

Praktischerweise übernimmt ein neuer Transporteur eine Sendung in dem er
diese scannt und dabei seine signierte Übernahmebestätigung dem
Übergebenden per [NFC](https://de.wikipedia.org/wiki/near_field_communication "wikilink") oder als
wiederum scannbaren Barcode übergibt. Dieser prüft die Signatur der
Übernahmebestätigung und gibt die Sendung physisch frei. Dieses
Verfahren funktioniert rationell und schnell zwischen professionellen
Transporteuren, kann aber auch über ein Barcodeeinlesen an Mobiltelefone
weitergegeben werden, was den einfachen “Normaluser” mit in die
Logistikkette integriert. Am Lieferende druckt man den papierschriftlich
Lieferschein aus und unterschreibt, wenn der Empfänger kein Endgerät mit
App hat.

Syntax
------

Diese Art der Logistik benötigt ein hohes Maß an Kommunikation. Die
“physikalische” Kommunikation geht leicht über Computernetze wie das
[Internet](/internet "wikilink"). Lediglich eine definierte gemeinsam
Sprache ist notwendig

Ein erster Ansatz ist [railML](https://de.wikipedia.org/wiki/railml "wikilink"), leider eben nur für
die Schiene. Zwar ist das
[schemes](http://www.railml.org/schemes/timetable/timetable_example_1.00.xml%7Ctimetable)
auch gut geeignet um Fahrten die von anderen noch mit genutzt werden
können zu beschrieben.

Entweder ein einheitlicher [XML](https://de.wikipedia.org/wiki/xml "wikilink")-Dialekt. Oder eine
Spezifikation für [Resource Description
Framework](https://de.wikipedia.org/wiki/resource_description_framework "wikilink") wären sinnvoll.

Ein mögliches Beispiel:

     <rdf:RDF
          xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
          xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
          xmlns:log="http://www.log.org/log-schema#" //Beispiel
          xmlns:pos="http://www.w3.org/2003/01/geo/wgs84_pos#"
           >

     <log:consignment>
      <log:ID>umija.org/20079999</log:ID>
      <log:owner>klml@umija.org</log:owner>
      <log:scheduler>umija.org/logscheduler/</log:scheduler>
      <log:priority>5</log:priority>
      <log:description>
       <log:size>100x33x20</log:size>
      </log:description>

      <log:based_near> //gibt es bereits in foaf
       <pos:Point>
        <pos:lat>48.136944</pos:lat>
        <pos:long>11.5625</pos:long>
       </pos:Point>
       <log:collecttime>
         <rdf:date>
         2007-08-15:15:00:00
         </rdf:date>
       </log:collecttime>
      </log:based_near>


      <log:target_near> 
       <pos:Point>
        <pos:lat>38.136944</pos:lat>
        <pos:long>11.5625</pos:long>
       </pos:Point>
      </log:target_near>
     </log:consignment>
     </rdf:RDF>

Oder man nutzt eine [vereinfachte
Auszeichnungssprache](https://de.wikipedia.org/wiki/vereinfachte_auszeichnungssprache "wikilink")
die von Mensch **und** Maschine geschrieben und verstanden werden kann.

     
    klml@umija.org:
    2007-08-15:15:00:00
    @Hauptstraße 12 Eching ---> @38.136944 11.5625
    #143kg
    +++

Könnte heißen: Klaus Mueller (*klml@umija.org*) will um *3* Uhr
Nachmittags soll am *15. August 2007* von der *Hauptstraße 12* in
*Eching* ein Paket an den Ort mit der Koordinate *38.136944* nördlicher
Breitengrad und *11.5625* östlicher Längengrad gebraucht haben. Das
Pakte wiegt *143kg* (und hat sonst keine Besonderheiten) und soll
innerhalb von *12 Stunde* (+++) aber nicht sehr eilig (++++) geliefert
werden.

