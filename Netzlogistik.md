# Netzlogistik

Mit **Netzlogistik** sollen Güter nicht mehr von einem Transportanbieter von
A nach B versendet werden, sondern von einer logistisch optimierten
Stafette von Dienstleistern. Um mehrere gleichberechtigte Teilnehmer in
den Versendeprozess zu integrieren muss die
[Kommunikation](/netzlogistik_kommunikation "wikilink") zur
Auftragsvergabe, Übergabe und Abrechnung standardisiert werden. So
lassen sich Warenströme auf verkehrstechnisch sinnvolle Touren über
Logistikdienstleistergrenzen hinweg optimieren und auch Verkehrswege
[gesellschaftlich besser nutzen](/netzlogistik_gesellschaft "wikilink").

Denn mir geht es oft so: Jeden Morgen wenn ich ins Büro komme rangieren
drei Paketdienste, vier Getränkelieferanten und ein Wäscheservice um den
begrenzten Platz im Hof.

[Aktuelles zum Thema Netzlogistik](/netzlogistik_blog "wikilink")


Theorie
-------
![Zwei Paketdienste mit langem Radstand blockieren eine Straße…](https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/2malKurierdienst.JPG/512px-2malKurierdienst.JPG)
![…um dann ein Päckchen abzuliefern.](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Kleines_paket.JPG/512px-Kleines_paket.JPG)


<a title="Klaus Mueller / cc by-sa (https://creativecommons.org/licenses/by-sa/3.0)" href="https://commons.wikimedia.org/wiki/file:kleines_paket.jpg"><img width="512" alt="kleines paket" src=""></a>

Oft sieht man in Städten Verkehrsstaus die von mehreren,
verständlicherweise in dritter Reihe parkenden, Liefer-Fahren verursacht
werden. Jene drei Fahrer steigen aus ihren Fahrzeugen aus, grüßen sich
vielleicht, und laufen dann in das selbe Geschäft um dort nacheinander
je ein Paket abzugeben. Wäre es nicht rationaler und
ressourcenschonender die drei Sendungen würden nur von einem
[Zusteller](https://de.wikipedia.org/wiki/zusteller "wikilink") ausgeliefert werden?

Denn meist wird die gesamte [Lieferkette](https://de.wikipedia.org/wiki/lieferkette "wikilink") von
einem einzelnen Anbieter erfüllt. Ein Versender übergibt eine Sendung an
einen Transporteur und jener übergibt diese dann an den gewünschten
Empfänger.

Zwar wechseln innerhalb des Transportprozesses sich Transportmethoden,
Sendungsbündelung und sogar die wirtschaftlichen Dienstleistern entlang
der Lieferkette ab. Dennoch treten für den Versender und Empfänger der
gleiche Dienstleister für Abwicklung und Abrechnung als Verantwortlicher
auf. Dieses System ist **monokontraktonal**.

Die Ausführung der Transportarbeit wird dabei teilweise sogar von
Subunternehmen übernommen. Die echte Leistung auf den Straßen erbringen
oft freiberufliche Fahrer mit Ihren eigenen Fahrzeugen, die gerade noch
mit einem Magnetschild “im Auftrag für Deutsche Parcel” auf den
Transportanbieter hinweisen.

Da, im Gegensatz zu einem monopolisierten Lieferdienst wie dem
staatlichen Postwesen, mehrere Anbieter auftreten, gibt es
**Überschneidungen** auf den Transportstrecken. Einerseits auf den
Kernstrecken, wenn Transportmittel unterschiedlicher Anbieter parallel
fahren (halb leere LKWs) und andererseits auf der letzten Meile bei der
Zustellung an den Empfänger.

Dabei würde ein geringerer Gesamtaufwand anfallen wenn eine örtliche und
zeitliche [Tour](https://de.wikipedia.org/wiki/tourenplanung "wikilink") nur von einem Lieferdienst,
dieser dann aber für alle Sendungen, befahren wird. In einer
Marktwirtschaft, die jedem Markteilnehmer den Zugang seiner
Dienstleistungen erlauben will und ohne staatlich reguliertes Monopol,
ist es nicht möglich Versandstücke zu bündeln. Außer die Lieferkette mit
unterschiedlichen Methoden und Anbietern öffnet sich für andere
Teilnehmer. So würde das monokontraktonale Angebot in ein
multikontraktonales Modell übergeführt werden.

### Multikontraktonales Modell

In einem **multikontraktonalen** Modell soll die Lieferkette für
unterschiedliche Teilnehmer geöffnet werden. Es wird so eine Möglichkeit
geschaffen, dass der Versender seine Sendung bei einem Transporteur X
aufgibt, der an den Spediteur Y weiter gibt, dann dieser an Kurierfahrer
X und dann an den Empfänger ausliefert. Diese System würde nicht primär
auf kontraktional Transportleistungen sondern vor allem anhand von
[Isochronen](https://de.wikipedia.org/wiki/isochrone "wikilink") (Orte, die von einem Ausgangspunkt
aus in derselben Zeit zu erreichen sind) aufbauen.

Daraus ergeben sich organisatorische Schnittstellen, die zwei Arten an
Herausforderung aufwerfen:

-   Die funktionierende **Kommunikation** zwischen allen Teilnehmern.
-   Das nötige **Vertrauen** die Ware zwischen mehreren Parteien
    auszutauschen.

![Auf dem Lieferwagen steht zwar DHL drauf, es liefert aber eine selbständiger Servicepartner (steht an der Tür)](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Lieferwagen_Servicepartner_fuer_DHL.jpg/512px-Lieferwagen_Servicepartner_fuer_DHL.jpg)

Beide sind aber bereits gelöst worden:

-   Die **Kommunikation** zwischen unterschiedlichen
    Logistikkettengliedern (KEP, LKW, Flugzeug; Crossdocking etc)
    innerhalb einer Organisation (Logistikfirma) sind vorhanden.
    Schließlich fährt kein KEP-Fahrer ein Paket persönlich vor Ort,
    sondern übergibt dieses, gestützt durch ein
    Logistikinformationssystem, innerhalb seiner Firma an andere Glieder
    der Lieferkette weiter. Nur fehlt bisher ein [offener
    Austauschstandard](/netzlogistik_kommunikation "wikilink") die
    Logistikglieder unterschiedlicher Organisationen (Unternehmen)
    verknüpft.
-   Das **Vertrauen**, dass mehrere Dienstleister ein Paket überbringen
    können, existiert und wird kontrolliert. So übergibt und erhält zwar
    das Kundenpaar seine Sendung an eine Organisation, diese aber
    beauftragt bereits heute fremde Spediteure bis hin zur Übergabe
    durch freiberufliche KEP-Serviceleute ([Hermes
    Europe](https://de.wikipedia.org/wiki/hermes_europe "wikilink")). Ob die Verantwortung innerhalb
    einer einer vertraglichen Vereinbarung zwischen Sub-Dienstleistern
    eines Unternehmens oder zwischen einzelnen Dienstleistern des
    Versenders oder Empfängers übergeben wird ist nicht von Bedeutung.
    Wichtig ist nur dass diese reglementiert übertragen wird. Auch
    könnten Transporteure in unterschiedliche Vertrauensstufen
    kategorisiert werden und je nach Transportgut wird von jedem
    möglichen Teilnehmer ein kodifiziertes Mindestvertrauen gefordert.

Vorteile
--------

Das System weist gegenüber dem klassischem monokontraktonalem
Logistiksystemen folgende Vorteile auf:

### Streckenbündelung

Das größter Potential einer offenen Lieferkette liegt in eine Bündelung
von einzelnen Sendungszustellungen. Versuchen Logistiknetze sich durch
ein [Hub and Spoke](https://de.wikipedia.org/wiki/hub_and_spoke "wikilink")-System zu optimieren, so
funktioniert das auch zwischen Logistikanbietern. Einzelne Lieferanten
[Planung ihre Touren](https://de.wikipedia.org/wiki/tourenplanung "wikilink") so dass gleiche
Empfänger gleichzeitig und ähnliche Empfänger nacheinander beliefert
werden. Das selbe funktioniert auch zwischen mehreren Lieferanten.

Ein Lieferant plant eine Tour in ein Gebiet und versucht möglichst viele
Aufträge für sinnvolle Mitlieferungen anzunehmen.

### Mischen von Warengruppen

Bisher versenden hauptsächlich nur Versandhändler oder Grosshändler
Waren über dritte Dienstleister. Zahlreiche Produzenten oder Händler
liefern aufgrund von zeitlichen Beschränkungen (Pizzadienst), von Masse
(Getränke Lieferdienst) oder aufgrund der Warengruppe eigenständig aus.
Oder weil das Versenden über Drittanbieter organisatorisch oder
preislich zu aufwändig ist. Dabei wäre es prinzipiell möglich das ein
DHL Lieferwagen auch zwei Getränkekisten liefert. Bei einer Lieferung im
Gastrobereich mit drei Paletten natürlich nicht mehr. Aber warum sollte
der Getränke LKW dann nicht ein normales Päckcehn mit ausliefern.

Mischt man aber alles, was über Straßen gefahren werden muss, so kann
man auch die Pizzalieferung, Arzneimittel, oder Personen-Transport so
vermischen dass Fahrten nicht aufgrund des Inhalts, oder ungünstiger,
aufgrund des des Kontraktors, sondern auf Grund von idealen örtlichem
Standorten vergeben werden. Natürlich unter der Voraussetzung das eine
Transporteinheit auch alles technisch transportieren kann. So wird ein
Kühllaster keinen Personen und ein Taxi kann kein Tiefkühlgemüse
transportieren.

### Letzte Meile

Die [Letzte Meile](https://de.wikipedia.org/wiki/letzte_meile "wikilink"), der letzten Abschnitt
eines Netzes zum Endteilnehmer, ist in allen Netzen meist der
aufwändigste und komplizierteste Teil. So ist auch in der
[Stückgut](https://de.wikipedia.org/wiki/stückgut "wikilink")-Logistik, also immer dann wenn nicht
ein voller LKW vom Produzent zum Endverbraucher durchfahren kann, die
Verteilung von Gütern in Summe und Kosten der aufwändigste Teil. Auch
ist die Stückgutlogistik “in der Mitte” der Kette relativ gut
automatisiert und strukturiert. Finden hier Sendungen anhand von
scannbaren IDs und automatischen Verteilsystem den richtigen Ort zur
richtigen Zeit, muss im Gegensatz der Auslieferfahrer am Ende der Kette
noch mit Adressaufkleber und handgeschriebener Benachrichtigungskarte
hantieren. Es gibt zwar die Möglichkeit
[Sendungsverfolgung](https://de.wikipedia.org/wiki/sendungsverfolgung "wikilink"), allerdings dienen
diese nur eine passiven Überwachung und keiner aktiven Steuerung des
Sendungsverlaufs. [Probleme gibt es in der Regel auf den letzten 1,5
Kilometer bis zur Haustür. Es ist der Abschnitt, auf dem etwa die Hälfte
der Kosten
entstehen.](http://www.sueddeutsche.de/wirtschaft/liefergesellschaft-alles-immer-bitte-sofort-1.3154867)

Eine ordnungsgemäße Zustellung scheitert oft im Privatgeschäft an der
Abwesenheit oder Unkenntnis des genauen Eintreffens der Waren beim
Empfänger. Wenn sich die Lieferkette durch einen offenen
[Kommunikationsstandard](/netzlogistik_kommunikation "wikilink") für
alle, auch den Empfänger öffnet, und alle Transporttouren nutzbar sind,
kann der Empfänger den Lieferprozess besser steuern. Könnte der
Endempfänger die letzte Meile der Transportkette aktiv bestimmen,
verbessert das den gesamten Versendeprozess.

Angebote wie [Same Day Delivery](/#same_day_delivery "wikilink")
versuchen Logistik schneller zu machen, allerdings sind [höheren
Zustellkosten für teure Services wie Same-Day zu
tragen.](http://www.internetworld.de/Heftarchiv/2013/Ausgabe-26-2013/Die-letzte-Meile).
Wenn ein Lieferdienst seine Touren nicht mehr nur nach einer optimalen
Strecke, sondern auch nach den zeitlichen Anforderungen seiner Kunden
planen muss, wird die Auslieferung aufwändiger und diese Mehrkosten
müssen gedeckt werden. Und Same Day Delivery ist auch möglich wenn man
das gelieferte Pakte möglichst nah zum Empfänger bringt.

Die Diversifikation an nachgefragter Transportdienstleistung kann eben
auch durch eine Innovation auf Angebotsseite bedient werden, anstatt nur
Preise zu erhöhen. Denn wenn Transportaufträge in der letzten Meile von
spontan unterschiedlichen Anbietern erfüllt werden, können diese über
Bündelungen von Aufträgen insgesamt effizienter sein.

Kommt ein Empfänger erst um 20.00 Uhr nach Hause, muss nicht der KEP
Hauptdienstleister extra eine Tour antreten, sondern diese kann
beispielsweise von einem Pizzadienst mit beliefert werden. Dazu muss
aber der Pizzalieferant einerseits einfach von dem Auftrag erfahren und
diesen auch sicher und verantwortlich an- und übernehmen können.

#### Sammeln

[Paketshops](https://de.wikipedia.org/wiki/paketshop "wikilink") nehmen Pakete für Privatpersonen an
und geben diese auch ab. Bisher aber nur für je einen Lieferdienst (DHL,
Hermes etc) Auch können Kunden meist nicht bestimmen in welchen
Paketshop sie die Lieferung haben wollen. Und die Übergabe vom und zum
Kunden könnte genauso IT unterstützt sein und nicht nur per
Papierzettel.

Besser wäre eine, ähnlich einer Email-Mailbox, vom Endempfänger
bestimmte Sammelstelle, in der **jeder** Sendungen ablegen kann und auch
bestätigt bekommt. Weiß der Endempfänger nun was in der “box” ist, kann
er sich je nach Menge oder Dringlichkeit in Abhängigkeit seiner
Anwesenheit den gesamten Inhalt zustellen lassen. Amazon macht das so:
[Prime Pantry items are all shipped
together.](http://www.amazon.com/gp/help/customer/display.html?nodeId=201505420)

Die [Packstation](https://de.wikipedia.org/wiki/packstation "wikilink") gibt es viel zu selten und
der [DHL Paketkasten](https://www.paket.de/paketkasten) ist leider viel
zu sperrig, außerdem ist beides nur für DHL.

#### Erste Meile

Was für die Letzte Meile gilt, gilt in der modernen Logistik teilweise
auch für die *Erste Meile* und auch für die
[Intralogistik](https://de.wikipedia.org/wiki/intralogistik "wikilink").

Zwar ist in der [Kommissionierung](https://de.wikipedia.org/wiki/kommissionierung "wikilink") beim
Lieferanten die Organisation und Kommunikation besser integriert, ist
doch die Nutzung von Logistikmitteln fast ähnlich breit verteilt.
Lieferten früher noch zentrale Lager sternförmig Waren aus, verteilen
sich Lieferaufträge auf immer mehr Spezialhändler und inhaltliche
Logistikstandorte.

So sind die Prozesse während der Kommissionierung ähnlich breit verteilt
wie im Lieferprozess. Innerhalb eines Lagers werde je nach
Kommissioniermethoden die einzelnen Teile mehrere Sendungen gemeinsam
gesammelt zusammengestellt und auf Sendungen verteilt
(Serienorientierte, parallele Kommissionierung) oder eine Sendung wird
von mehreren Kommissionieren bearbeitet und weitergeben (serielle
Kommissionierung). All diese Prozesse benötigen eine
[Kommunikation](/netzlogistik_kommunikation "wikilink") zwischen
organisatorisch getrennten Einheiten.

Besonders Universalversender haben eine derartige unterschiedliche
Produktpalette dass diese unterschiedliche gelagert, kommissioniert und
transportiert werden muss. Daher werden diese Waren in
unterschiedlichen, teilweise räumlich getrennten Lagern gehalten. Der
Versand erfolgt dann meist direkt an den Endkunden. So erhält man zwar
Waren von einem Händler, aber in mehreren Lieferungen. Auch
Fachversender mit einer kleinen Produktpaletten liefern aus
unterschiedlichen Lagern aus, was auch wirtschaftliche Gründe haben
kann, da spezialisierte Handelspartner den gesamten Handel, samt
externer Logistik, übernehmen.

Die Versender werden durch eine multikontraktonale offene Logistik
besser integriert, da diese nicht nur über Organisationsgrenzen hinweg,
sondern auch über räumliche Grenzen, kommuniziert.

### Fehlerkorrektur

Wenn Sendungen auf ihrer gesamten Route den Transporteur wechseln
können, können sie auch ausgefallen Teiltransporte umschiffen. Kann ein
Teilnehmer einen schon angenommen Auftrag, aus Überlastung,
Personalmangel oder Maschinenschaden, nicht ausführen, kann er den
Auftrag freigeben und von einem anderen ans (nächste) Ziel bringen
lassen. Dazu muss er nur seine Sendung anstatt am Ziel am aktuellen
Aufenthaltsort übernehmen lassen. Das ist zwar insgesamt aufwändiger,
aber besser als wenn eine Sendung festhängt.

Solche kritische Ausnahmeereignisse werden im [Supply Chain Event
Management](https://de.wikipedia.org/wiki/supply_chain_event_management "wikilink"), auch über
Unternehmensgrenzen hinweg, behandelt.

### Terminierung

Folgt die bisherige Logistik noch einer tages oder 2-tages Terminierung,
wird inzwischen eine stündliche Genauigkeit gefordert. Die
Nichtzustellung ist ein großes Problem und [Zustellungen für
Privatkunden sind mittlerweile bis zu vier Mal teurer als Zustellungen
für
Firmen.](http://www.dhl-discoverlogistics.com/cms/de/course/technologies/reinforcement/first.jsp)

Flexibilisiert man die Logistik, können zeitliche Verzögerungen sinnvoll
mit eingebaut werden. So kann man Dinge kurzfristig lagern, oder
langsamere, aber günstigere Transportmittel ein flechten.

Neben den rein organisatorisch und betriebswirtschaftlich logistischen
Vorteilen ergeben sich weitere [gesellschaftliche
Synergien](/netzlogistik_gesellschaft "wikilink") etwa in der
Verkehrsentlastung von überfüllten urbanen Regionen oder besserer
Versorgung von ländlichen und weitläufigen Landstrichen.

Praxis
------

### Touren und Hubs

Entscheidend für eine offene Versandkette ist die Möglichkeit ohne hohen
Aufwand und schnell Waren von einem Transportanbieter zum anderen zu
tauschen oder auch zwischen zu lagern, ähnlich dem [Cross
docking](https://de.wikipedia.org/wiki/cross_docking "wikilink").

Neben einer geregelten
[Kommunikation](/netzlogistik_kommunikation "wikilink") müssen solche
Tauschpunkte aber vor allem bekannt und gut verteilt sein. Solche
Tauschlager müssen definierte **Öffnungszeiten** haben, um Sendungen
sicher anliefern zu können und kann während des Tages Pakete sammeln,
der Endempfänger kann dann entweder wie bisher seine Sendung selbst
Pakete abholen, oder wenn er dies nicht kann oder möchte, sich dann auf
Auftrag die Sendung zu einem geeigneten Zeitpunkt von einem
**alternativen** Lieferanten, welcher eine ähnliche oder passende Route
schon fährt, bringen lassen.

[Paketshops](https://de.wikipedia.org/wiki/paketshop "wikilink") leisten genau diese Aufgabe, nur
dass diese für je einen Anbieter arbeiten und es keine Möglichkeit gibt
von dort sich Lieferungen zustellen zu lassen. Vor allem fehlt eine
standardisierte [Kommunikation](/netzlogistik_kommunikation "wikilink")
die Übergaben schneller und einfacher macht als mit Zettel und Nummer.

Aber auch um mit Zeitverschiebung arbeiten zu können: Soll ein tagsüber
eintreffendes Paket erst zum Feierabend zugestellt werden, muss das
Tauschlager dieses auch dann abgeben können. Vor allem wenn eine Sendung
nicht sofort nach eintreffen weiter verschickt werden soll, sondern
gewartet werden kann bis ein Transporteur eine günstige
Streckenbündelung anbieten kann, muss klar sein bis wann das noch
funktioniert.

Stößt ein Tauschlager an **Kapazitätsgrenzen** (Lagerplatz, Personal
etc) sollte es einfach durch ein anderes Tauschlager unterstützt werden
können. Da Sendungen in ein Gebiet nicht zwingend an einem definierten
Tauschlager übergeben werden müssen, sondern frei geroutet werden
können.

Die [Tourenplanung](https://de.wikipedia.org/wiki/tourenplanung "wikilink") der Strecken sollte
möglichst aufgrund der
[Streckenbündelung](/#streckenbündelung "wikilink") erfolgen.

### Kosten und Auftragsvergabe

Öffnet sich die Transportkette, so muss sich auch die Abrechnungstruktur
öffnen und klar sein was jede Teildienstleistung kostet und wie diese
abgerechnet wird. Monokontraktionale Gesamtanbieter arbeiten mit einer
Mischkalkulation, so kostet eine Paket zu einem Mehrfamilienhaus,
Bürokomplex oder einer Packstation, welche alle meist mehrere Sendungen
annehmen, nicht mehr als ein Paket zu einem Einfamilienhaus am Ende
einer Sackgasse. Auch wird nicht unterschieden ob ein Pakte im gleichen
Zustellbezirk bleibt oder in ein anderes
[Versandzentrum](https://de.wikipedia.org/wiki/versandzentrum "wikilink") geliefert werden muss, was
aufwändiger ist. Auch wenn der Verkehr zwischen Umschlagpunkten
grundsätzlich anfällt und als fixe Kosten in die Gesamtrechnung mit
einfließen.

Alle potentiell beteiligten Dienstleister und Transporteure erbringen
eine Leistung nur wenn diese auch für sie wirtschaftlich lukrativ ist.
Aber kann man die Paketpreisen von 4 bis 10 Euro je Paket überhaupt noch
aufteilen? Durch Netzlogistik teilt man einen Zustellweg eventuell zwar
in noch kleinere Teile, aber sie ermöglicht es einem einzelnen
Transporteur auf einer Strecke mehr zu sammeln und zu zustellen.

Mussten bisher DPL Mann, Hermes Mann und UPS Mann die Treppe einzeln
hintereinander hoch laufen um dabei jeweils lediglich [0,25€ bis
0,80€](http://www.ciao.de/Hermes_Versand__Test_2709012) verdienen,
könnte das jetzt ein Fahrer machen und dabei entweder mehr verdienen,
oder sich sein Zustellgebiete mit dem anderen Teilen und bei gleichen
Verdienst wesentlich weniger arbeiten.

Einzelne Leistungsfaktoren können sein:

-   die **Formfaktoren** Größe, Gewicht, Handlichkeit (Normstapelkiste
    oder Tüte), Klima (warm oder kalt), Gefährlichkeit etc
-   jedes genutzte Transportmittel kostet je nach geleisteter
    **Transportstrecke**
-   alle **Übergaben** von der Sendungsaufnahme, alle Zwischenschritte,
    jeder Zustellversuch und eine qualifizierte Zustellung. Wobei
    automatisierte Zwischenübergaben günstiger sein werden als
    persönliche Zustellungen. Gerade in der Zustellung ist es ein
    Unterschied im Aufwand ob ebenerdig mit Hilfsmitteln
    (z.B.[Laderampe](https://de.wikipedia.org/wiki/laderampe "wikilink")) oder im 5. Stock
    zugestellt wird.
-   maximaler und auch minimaler **Lieferzeitraum**

### Vergabe

Ein Versender **fragt** eine gesamte Lieferung in einem
[Server](/netzlogistik_kommunikation "wikilink") an und nennt dabei
Formfaktoren, Zeitfenster, Start und Ziel. Entweder Lieferanten können
nun Angebote für die erste Teilstrecken unterbreiten, welche der Sender
annimmt und dann am Zwischenziel weitere Teilstrecken anfragt.
Spätestens am Schluss der Kette übernimmt der Empfänger die Steuerung
der Sendung und lässt bringt die Sendung zum Ziel.

Oder die Transportanbieter **bieten** feste Preise für einzelne
Teilleistungen (Streckenkilometer, Zeitfenster, Übergabe, Zustellung
etc) an und Versender buchen eine Kette von passenden Angeboten.

Ein kosten-neutrales [Peering](https://de.wikipedia.org/wiki/peering "wikilink") zwischen
gleichrangigen Anbietern ist bilateral immer möglich.

### Prozess und Problem

Kollision

:   es gibt Versandaufträge die unter keinen Umständen vermischt werden
    dürfen (z.B. Lebensmittel und Abfall). Sendungen müssen daher in
    entsprechenden Kategorien versandt werden.

Organisation

:   Ein solches chaotisches System funktioniert nur wenn es in
    festgelegten Rahmenbedingungen funktioniert. Im Prinzip ist
    Netzlogistik die Anwendung von freien Marktprinzipien auf die
    SupplyChain. Ähnlich zu anderen Märkten müssen diese in einem
    stabilen Umfeld stattfinden.
    -   Es muss für jeden Logistigwilligen **möglich** sein seine
        Transportaufträge in das System einzuführen, aber auch
        Transportaufträge auszuführen.
    -   Eine gemeinsame **Qualitätsdefinition** ist in einem so fragilen
        System unabdingbar. Jeder Teilnehmer muss wissen was er für eine
        Leistung bekommt. Wer z.B. Lebensmittel transportieren lassen
        will muss wissen wie der Transporteur diese behandeln wird.
    -   Eine **Mindestmenge** an Transporten muss abgewickelt werden,
        sonst können die potentiellen Synergien einer offenen
        Logistikkette nicht genutzt werden. Wenn trotzdem jede Strecke
        einzeln gefahren wird, bringt der kommunikative Aufwand nichts.

### Einführung

So ein System weist eine Henne Ei Problematik auf. Ein Einzelner kann
kein Paket darüber verschicken, ohne eine etabliertes Netz nicht
versenden, ein Lieferant kann mit nur einem einzigen Kunden nichts
anfangen.

Aber Netzlogistik kann sich aus einem monokontraktonalen Angebot heraus
entwickeln. Bietet man ganz einfache Lieferungen an und beginnt, noch
mit einem geschlossenen Lieferprozess und liefert die ganze Kette mit
unterschiedlichen Lieferanten einer organisatorischen Einheit (Firma).
Nun beginnt man aber einzelne Strecken auszuschreiben und versucht immer
mehr freie Lieferanten einzubinden.

Sofern man Server und Software (mobile apps) zur Verfügung stelle dürfte
man leicht freie Lieferfahrer finden.

Wichtig sind regional sinnvoll verteilte
[Tauschlager](/#touren_und_hubs "wikilink") um Versendern, Lieferanten
und Empfänger einen Handlungsrahmen zu bieten. Denn streng genommen
braucht man um Netzlogistik zu starten nicht einmal IT Unterstützung.

-   Allein die Konvention das ein Transporteur Pakete auch auf eine
    Teilstrecke mit nimmt genügt
-   sofern sichergestellt ist dass Absender und Empfänger, oder mindest
    eine mailbox, auch mit Netzlogistiktransporteuren erreichbar sind

Wenn Absender, Empfänger und Transporteure untereinander vertrauen und
die Routingentscheidung von den jeweiligen Teilstrecken getroffen wird.
Es genügt ein Adressaufkleber, das Postleitzahlensystem als
Routingsystem und eine Kennzeichnung (z.B. per Signet) dass
Transporteure wissen dass sie das Paket auch nur teilweise
Transportieren müssen. Beispielsweise könnten große Firmen mit
Waren-Ein- und Ausgang und internen Hausboten, könnten sich nach außen
öffnen.

Eine transportleistungsgerechte Bezahlung wäre so natürlich schwierig,
kann aber durch Peering abgedeckt werden. Erst um Übergaben zu
dokumentieren, wäre dann IT Unterstützung wichtig.

Aber was ist [Netzlogistik nicht](/netzlogistik_abgrenzung "wikilink")?
Aber Netzlogistik soll nicht nur existierende logistischen
Herausforderungen verbessern, sondern hat auch [erweiterte
Möglichkeiten](/netzlogistik_erweiterte_möglichkeiten "wikilink").

Umsetzungen
-----------

Viele Projekte probieren [ähnliches](/netzlogistik_abgrenzung "wikilink") 

Ich habe mit [kohr.supply](https://kohr.supply) einen Alphaprototyp gebaut.


<Kategorie:Logistik> <Kategorie:Netz>
