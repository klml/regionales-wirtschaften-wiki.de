# Literatur

Bücher:
-------

-   Margit Kennedy/Bernhard Lietaer: Regionalwährungen
-   verschiedene Ausgaben der Zeitschrift für Sozialökonomie:
    <http://www.sozialoekonomie.de/>

Geld/Regionalgeld als Nebenthema:
---------------------------------

-   Thomas Koudela: Entwicklungsprojekt Ökonomie
-   Brunnhuber/Klimenta: Wie wir wirtschaften werden

Regionalwirtschaft/Regionalisierung/Lokalisierung:
--------------------------------------------------

-   Eigner/Krotschek u.a.: Zukunft: Regionalwirtschaft!
-   Douthwaite/Diefenbacher: Jenseits der Globalisierung. Handbuch für
    lokales Wirtschaften.
-   Rob Hopkins: Energiewende - Das Handbuch
-   [Leopold Kohr](/leopold_kohr "wikilink"): Das Ende der Großen
-   E.F. Schumacher: Die Rückkehr zum menschlichen Maß (small is
    beautiful)

Online-Veröffentlichungen:
--------------------------

-   Siglinde Bode: Potentiale regionaler Komplementärwährungen zur
    Förderung einer endogenen Regionalentwicklung (2005),
    <http://www.regionales-wirtschaften.de/regionalentwicklung.de/Siglinde_Bode_2005_Komplementaerwaehrungen_zur_endogenen_Regionalentwicklung.pdf>
-   Annette Bickelmann: KLEINgeld - Monetäre Regionalisierung durch
    Regiogeld als Werkzeug im Regionalmanagement (2009),
    <http://www.regionalentwicklung.de/download/bickelmann.pdf>
-   Gerhard Rösl: Regionalwährungen in Deutschland - Lokale Konkurrenz
    für den Euro?,
    <http://www.bundesbank.de/download/volkswirtschaft/dkp/2006/200643dkp.pdf>
-   Tobias Plettenbacher: Neues Geld - Neue Welt,
    <http://www.neuesgeld.com/getfile.php?id=192>
-   <http://www.regionales-wirtschaften.de/4.25.0.0.1.0.phtml>

Nebenthema:
-----------

-   Norbert Rost: Homo Oeconomicus - Eine Fiktion der Standardökonomie,
    <http://www.dreigliederung.de/download/2008-12-001.pdf>
-   Norbert Rost: Eine experimentelle Überprüfung der Aussagen der
    Freiwirtschaftstheorie,
    <http://userpage.fu-berlin.de/~roehrigw/diplomarbeiten/rost/ueaf.pdf>

Archive:
--------

-   Werner Onken betreut das Wissenschaftliche Archiv zur Geld- und
    Bodenreform in Oldenburg: onken@sozialoekonomie.info
-   <http://www.geldreform.de>
-   Archiv des Regiogeld-Verbandes, offen für Mitgliedsinitiativen,
    Fördermitglieder und Wissenschaftler,
    <http://www.regiogeld.de/intern.html>
    -   Krister Volkmann: Regional und trotzdem global
    -   Sybille Ditzen: Ansätze zur Verbesserung der Zusammenarbeit von
        Regiogeldinitiativen mit den Kommunen
    -   Anja Malz: Entwurf und Prototypenrealisierung eines
        elektronischen Zahlungssystems für eine Regionalwährung
    -   Franziska Ziegler: Konzept, Umsetzung und Akzeptanz einer
        Regionalwährung am Beispel des Chiemgauers
    -   Gunnar Kliewe: Gemeinschaft in Gesellschaft. zum möglichen
        Einfluss von Regionalwährungen auf die regionale Sozialstruktur
    -   Jörg Großschmidt: Der “Chiemgauer” - die Teilnahme an einer
        Regiogeldinitiative als rationale Entscheidung?
    -   Detlef Denich: Probleme bei der Einführung einer komplementären
        Währung auf regionaler Ebene – eine Fallstudie anhand des
        DreyEcker
    -   Katharina Arnold: Untersuchung der ökonomischen Wirkungen von
        Regionalgeld an ausgewählten Beispielen
    -   u.a.

