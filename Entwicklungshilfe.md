# Entwicklungshilfe

**Entwicklungshilfe** oder wie es modern heißt
[Entwicklungszusammenarbeit](https://de.wikipedia.org/wiki/entwicklungszusammenarbeit "wikilink"),
soll Entwicklungsländern helfen sich zu entwickeln. Dabei spiegelt der
schon anpasste Begriff 'Entwicklungszusammenarbeit' einen großen
Kritikpunkt wieder, dass eben nicht der 'schlaue', 'bessere',
'industrielle' Westen dem Süden sagt wie man es richtig macht; sondern
das in der Enticklungd er Länder gleichberechtigt zusammengearbeitet
wird. Aber auch eine Gleichberechtigung garantiert nicht den “richtigen”
weg für die Entwicklungsländer. [Leopold Kohr](/leopold_kohr "wikilink")
fordert in seinem Buch sogar eine *Entwicklung ohne Hilfe*, aber mit der
Möglichkeit jedes Landes sich selbst zu entwicklen.

Soll sich nun jedes arme Land am Rande des Zusammenbruchs einfach selbst
überlassen werden und wir ziehen uns auf eine passive “wir können doch
eh nichts machen” Stellung zurück?

[Mittlere Technologien](/mittlere_technologien "wikilink")

[p:Angepasste Technologie](https://de.wikipedia.org/wiki/angepasste_technologie "wikilink")

-   [fahrraeder-fuer-afrika.de](http://www.fahrraeder-fuer-afrika.de)
-   [germantoilet.org](http://www.germantoilet.org)

geek choirs
