# Peak Oil

Als **Peak Oil** wird jener Punkt bezeichnet, in der die
Ölfördergeschwindigkeit nicht mehr mit der Ölverbrauchsgeschwindigkeit
Schritt hält. Durch den global steigenden Verbrauch von Erdöl bei
gleichzeitig stagnierender oder gar abnehmender Fördergeschwindigkeit
entsteht eine “Angebotslücke”, die sich in steigenden Preisen und
Ölknappheit ausdrückt. Als besonders problematisch wird es angesehen,
daß die modernen Industriegesellschaften abhängig von der Versorgung
durch Öl sind, nicht nur Energie und Heizwärme wird aus Erdöl
hergestellt, auch der Verkehr, die Landwirtschaft und die chemische
Industrie sind auf diesen Rohstoff angewiesen. Eine Knappheit an Öl hat
Auswirkungen auf all diese Wirtschaftsbereiche, während steigende
Ölpreise Inflation auslösen.

Als Antwort auf **Peak Oil** wird ein Umbau der Vorsorgungssysteme und
hier speziell der Energieversorgung notwendig. Gleichzeitig ist ein
Umbau des Wirtschaftssystems hilfreich, wobei [Regionales
Wirtschaften](/regionales_wirtschaften "wikilink") eine besondere Rolle
spielen kann. Im englischsprachigen Raum formiert sich als Reaktion auf
Peak Oil die [Transition Town](/transition_town "wikilink")-Bewegung, die
versucht auf lokaler Ebene von unten Städte und Gemeinde auf die
bevorstehenden Entwicklungen vorzubereiten.

-   [www.peak-oil-forum.de](http://www.peak-oil-forum.de)
-   [Kybernetische Theorie des
    Ölpreises](http://www.gdr-online.de/peakoilwiki/index.php5?title=Kybernetische_Theorie_des_%C3%96lpreises)
-   [Peak-Oil.com](http://www.peak-oil.com)
-   [Oelschock.de](http://www.oelschock.de)

<Kategorie:Energie>
