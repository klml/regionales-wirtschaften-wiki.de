# Regionalfond

Ein **Regionalfond** sammelt regionales Finanzkapital um es wieder
regional zu investieren. Beispiel dafür ist der Regionalfond der
[RegioStar eG](/regiostar "wikilink"), der vor allem in
Viktualien/Lebensmittel, Immobilien/Grundstücke sowie
Unternehmensbeteiligungen investiert.

Ein Regionalfond ist ein passendes Werkzeug, um regionale Aktivitäten zu
finanzieren und den Anteilseignern/Anlegern Investitionen in
realwirtschaftliche, räumlich nahe - und damit beeinflussbare und
überschaubare - Investitionsobjekte zu ermöglichen.

Mehr Informationen
------------------

-   \[<http://www.regiostar.com/72.98.html>?&tx\_ttnews%5Btt\_news%5D=64&tx\_ttnews%5BbackPid%5D=45&cHash=e259acf3c5
    Regionalfond als Geldanlage bei der RegioStar eG\]

<Kategorie:Regional>
