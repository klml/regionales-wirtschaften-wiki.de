# Verfassung

*“Ihr seid immer nur dagegen, macht doch mal bessere Vorschläge...”*

Wie sähe eine neue Verfassung für dieses (oder ein anderes) Land aus?
Links oben gibts den Knopf Bearbeiten...

Vorlage könnte sein: [Grundgesetz der Bundesrepublik
Deutschland](http://www.bundestag.de/parlament/funktion/gesetze/grundgesetz/)

[Wikitution](http://wikitution.org) macht das schon für die europäische
Verfassung.
