# Transition Town

Die **Transition Town**-Bewegung treibt die Umgestaltung der Städte und
Dörfer in Hinblick auf das postfossile Zeitalter voran. Sie hat ihre
Wurzeln in Großbritannien, Irland und den USA. Konzepte wie (regionale)
[Selbstversorgung](/selbstversorgung "wikilink"),
[Permakultur](/permakultur "wikilink"),
[Energieeffizienz](/energieeffizienz "wikilink"), Modelle des
[Teilens](/teilen "wikilink"), [erneuerbare
Energien](/erneuerbare_energie "wikilink"), [nachwachsende
Rohstoffe](/nachwachsende_rohstoffe "wikilink") werden integriert.

Angetrieben wird die Bewegung vom Wissen um [Peak
Oil](/peak_oil "wikilink") und dem Wunsch nach einer [nachhaltigen
Lebensweise](/nachhaltigkeit "wikilink").

-   [Energiewende Blog](http://energiewende.wordpress.com/)
-   -   [Transition Town Wiki](http://transitiontowns.org/) (english)

