# Netzlogistik Gesellschaft

Das Problem das viele Lieferanten parallel ausliefern und Synergien
durch eine gemeinsame Tourennutzung nicht ausnutzen können, könnte man
auch durch monopolistische Transportdienstleistungen regeln. Wenn nur
einer fahren darf, fährt auch nur einer und es fährt überhaupt einer.
Das Postwesen war nicht ganz ohne Grund in staatlicher Hand z.B. die
staatliche [Deutsche Bundespost](https://de.wikipedia.org/wiki/deutsche_bundespost "wikilink").
Früher kam “die Post” genau einmal am Tag, zu einer definierten Zeit und
hat alles mitgebracht: Briefe, Pakete, sogar Geld. Eine Liberalisierung
und damit die Hoffnung auf Innovation durch Wettbewerb ist
wünschenswert, aber wenn dann richtig. Nur den klassischen Postdienst in
einen Markt umbennen und allen Anbietern Zugang geben umd dann zu hoffen
das alles besser wird ist weder innovativ, noch werden die echten
Chancen einer Liberalisierung überhaupt genutzt.

Das ein groß-staatlicher landesweiter Zustelldienst die Möglichkeiten
einer feinen Logistik abdeckt ist bei der bekannten Flexibilität von
staatlichen aber auch privaten Großunternehmen nicht zu erwarten.

Warum sollte [Netzlogistik](/netzlogistik "wikilink") nun besser
gegenüber den bisherigen, staatlicher Post und monokontraktonal KPEs,
sein?

Wirtschaftlich
--------------

Die durch die anhaltenden Spezialisierung, Arbeitsteilung und
Verfeinerung der Bedürfnissen immer größer werdenden Nachfrage nach
Logistik könnten so effektiver die Gesamtmasse an Waren um- und
verteilen. Händler und Kunden hätten eine wirklich innovativer
Möglichkeit Logistik zu nutzen, dadurch das weniger Arbeitsleitung und
Ressourcen nötig sind; ohne dabei Pseudo-Innovation wie Lohndumping
(Fahrerlöhne), Ökologieverbrauch (mehr Lieferwagenkilometer) oder
Verbrauch von Verkehrsflächen (mehr Fahrzeuge und vor allem Kurzparker)
zu nutzen.

Die dezentrale direkte Versorgung der Konsumenten per Versand oder auch
im nahe gelegenen vertrauten Tante Emma Laden, würde so wirtschaftlicher
und einfacher als WalMart und Rahmenhandelsvertrag.

Volkswirtschaftlich
-------------------

Eine verstärkte Feingliederung der Wirtschaft in eine echte
Marktwirtschaft ist zu erwarten. Da Transport und Warenaustausch weniger
hemmen, können sich Produzenten mehr spezialisieren und Konsumenten
besser nachfragen.

Außerdem werden vorhandene **Verkehrsressourcen** besser genutzt. Neben
der Tatsache das vermutlich pro gelieferten Pakte wesentlich weniger
Kilometer anfallen, kann eine Kommune ihren Verkehr dadurch entlasten
das diese bestimmten konzessiven Logistik-Diensten explizit erlaubt in
dritter Reihe zu parken. Mit [Netzlogistikist](/netzlogistik "wikilink")
es möglich das das bei knapper Verkehrsmöglichkeit (viel Verkehr, wenig
Straße) wie in Innenstädten bestimmte Anbieter konzessiv bevorzugt
werden. In den engen Innenstädten sind fahrende und vor allem parkende
Lieferwagen ein Problem. Würde ein Anbieter für eine kleines Gebiet
allein eine regelmäßig neu ausgeschriebene “Drittereiheparkerkonzession”
bekommen, dafür aber ein Gebiet un-diskriminierend und preislich fixiert
anfahren hätte man die Vorteile der “alten Post”, ohne gleich die ganze
Branche verstaatlichen zu müssen.

Andererseits können auch kommunale Gebiete mit **schlechter logistischer
Abdeckung** eine Grundversorgung fördern und durch Transportbündelung
eine effizientere Versorgung gewährleisten. Für drei KPEs und einen
Pizzadienst lohnt sich die Anfahrt zu einem Aussiedlerhof nicht, ein
Milchsammelfahrer der sowieso täglich den Hof anfahren muss, könnte
Päckchen und Nahrungsmittel mitbringen.

Reglementierung
---------------

Netzlogistik als politische Aufgabe erstreckt sich nicht in einem
staatlichen Betrieb von Transport, aber kann Rahmenbedingungen
festsetzen. [Taxis](https://de.wikipedia.org/wiki/taxis "wikilink") werden auch privatwirtschaftlich
betrieben, aber durch das
[Personenbeförderungsgesetz](https://de.wikipedia.org/wiki/personenbeförderungsgesetz "wikilink")
und [Pflichtfahrgebieten](https://de.wikipedia.org/wiki/pflichtfahrgebiet "wikilink") reglementiert,
aber durch Konzepte wie [Uber](https://de.wikipedia.org/wiki/uber_(unternehmen) "wikilink") in Frage
gestellt.

Transportanbieter die diskriminierungsfreie Netzlogistik anbieten
können, da sie durch Bündelung weniger Verkehrsressourcen verbrauchen,
privilegierte Rechte bekommen. Von der Möglichkeit der Nutzung von
[Busfahrstreifen](https://de.wikipedia.org/wiki/busfahrstreifen "wikilink"), Erlaubnis auf der
Straße (dritte Reihe) zu parken, bis zum Zugang zu bestimmten Gebäuden
gibt es viele Möglichkeiten Transport auch von der infrastrukturellen
Seite noch mehr zu vereinfachen.

An diese Vorteile könnte man politisch gewollte Forderungen, wie
Mindestlöhne oder Umweltstandards, hängen, um ein weiteres Sozialdumping
im Logistikbereich zu verhindern.

So hätte man eine staatlichen Ordnungsrahmen um eine freie
Transportwirtschaft herum.

