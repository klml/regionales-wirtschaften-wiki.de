# Dezentrale Finanzsysteme

Heutige Währungssysteme sind vorwiegend **monopol** und **zentral**
aufgestellt: Eine [Zentralbank](/zentralbank "wikilink") hat die
alleinige Hoheit über die Herausgabe von [Geld](/geld "wikilink"). Sie
legt die Rahmenbedingungen (Geldmenge, Zinssätze) und die Ziele ihrer
Steuerungen fest.

Diese Zentralisierung verursacht sehr große Währungsräume welche von den
Teilnehmern aber auch von Experten nicht mehr beobachtet und
kontrolliert werden können und so zwangsläufig Misswirtschaft und
Missbrauch fördern.

würden dem Warentauschsystem kleine, überschaubare, kontrollierbare
Massstäbe verleihen. [Regionalgelder](/regionalgeld "wikilink") als eine
wirklich praktikable Form dieser Idee konnte sich bislang nur bedingt
durchsetzen.

Vielleicht liegt der Grund dafür das diese untereinander nicht
praktikabel austauschbar sind. Eine praktische Nutzung von Regiogeld
wird mangels Reichweite dann schwierig wenn man außerhalb dieser Region
handeln will und muss.

Einem Devisenmarkt für Regionalgelder war bisher immer ein großer
Hinderungsgrund. Aber gerade das reichweitenstarke und kreative
Multimedium Internet könnte in diese Lücke dringen.

Zur weiteren Diskussion gibt es die [Mailingliste
Finanzsystem](/mailingliste#finanzsystem "wikilink").

Alternativen
------------

Alternativ zu diesem zentralistischen System wäre ein dezentrales System
denkbar. Einerseits würde ein dezentraler Ansatz mehr als einer Stelle
erlauben, Zahlungsmittel zu schöpfen, andererseits könnten die
geschöpften Zahlungsmittel anders konzipiert sein, als dies in der
Historie geschehen ist. Der Mechanismus der
[Geldschöpfung](/geldschöpfung "wikilink"), eine geldsystemingetrierte
[Geldhaltegebühr](/geldhaltegebühr "wikilink") (“nullfristiger
Zinssatz”), der Benutzerkreis bzw. die geografische Ausdehnung
([Regionalgeld](/regionalgeld "wikilink")) oder auch die
Gesamtzielsetzung wären Parameter, die in neuen Geldsystemen änderbar
wären.

Das Internet eignet sich insbesondere für Konzepte und Experimente, denn
es ähnelt der [Geld](/geld "wikilink")-Wirtschaft an zwei entscheidenden
Stellen:

Netzstrukturen
:   existieren sowohl zwischen Computern im Internet als auch zwischen
    [Wirtschaftsakteuren](/akteur "wikilink") im Wirtschaftssystem (siehe
    [Wirtschaftsnetzwerk](/wirtschaftsnetzwerk "wikilink"))

Informationscharakter
:   [Geld](/geld "wikilink") als System bietet Informationen darüber,
    -   welcher Akteur wie viel Leistung erbracht hat und deshalb
        Anspruch auf Leistung des Netzwerkes besitzt
    -   wo Resourcen dringend benötigt werden (und daher die Preise
        steigen).

Austauschprozesse in der Wirtschaft lassen sich in
informationsverarbeitenden Systemen sehr gut abbilden.

Aus der Zielsetzung der Dezentralisierung ergibt sich die Fragestellung:

Wie müsste ein elektronisches System aussehen, welches es seinen Nutzern
erlaubt, in beliebiger Menge Zahlungsmittel zu “machen” und mit diesen
Zahlungsmitteln untereinander zu kaufen und zu verkaufen? Diese Frage
steht hinter dem Stichwort **Dezentrale Finanzsysteme**.

Der erste Gedanke dazu lautet: Jeder elektronische Austausch von
Informationen (Zahlungsmittel=Information) braucht ein Protokoll. Wie
sähe solch ein Protokoll aus? Dazu zwei Ideen von Sommer 2007, die eine
Grundlage bilden könnten, auf deren Basis Lösungen gefunden werden
dürfen:

potentielle Anwendungsfälle
---------------------------

Micropayment
:   Für was auch immer.

Mikrokredite

:   im allgemeinen und [Mikrokredite in
    Deutschland](http://www.handelsblatt.com/unternehmen/banken-versicherungen/kredit-wuerdig;2074691)

(Spiel)geld für Computerspiele
:   Vor allem in Strategiespielen (WoW) oder Roleplays (Sims) finden
    sich in der virtuellen Realität auch virtuelle Währungen die oft,
    vor allem bei größeren Spielen an reale Finanszysteme (€)
    angeschlossen sind. Teilweise nur in eine Richtung. So bestehen die
    meisten Firmenkonzepte darin, das Spieler mit echtem Geld Spielgeld
    kaufen können. Um nicht verdächtigt zu werden, illegales Glückspiel
    zu betreiben, meistens aber nicht Rückwärts.

Soziale Netzwerke
:   [Geld per Facebook
    überweisen](http://www.excitingcommerce.de/2008/09/schnelles-geld.html)
    Es ist sicherlich nicht der Weisheit letzter Schluss, eine neues
    Monopol entstehen zu lassen, aber man sieht, es denken sehr viele
    darüber nach. Aber vor allem liegt hier schon eine, wie auch immer
    qualitative, Vertrauensgrundlage über die sozialen Verknüpfungen
    vor.

Affiliatenetzwerk

:   (sind erst mal umstritten;) aber sie sind eine effektiverer Form der
    Werbung als Fernsehwerbung. Werbung wird anteilmäßig am erbrachten
    Umsatz (meist sogar nach Retourenfrist) bezahlt. Dieses System ist
    sehr datenlastig, da jeder Verkaufsvorgang gespeichert und
    zugeordnet werden muss.

Kundenbindungsprogramm

:   also decent open source PayBack

Alternative Gemeinschaften
:   In der “gibt eine eigene Währung namens LØN (Lohn), die Münzen haben
    einen Wert von 50 Dänischen Kronen.”

Crypto Geld
:   Die verschluesselten Waehrungssysteme bilden eine moderne
    alternative zum zentralan Geldsystem. Das Peer to Peer System
    garantiert Anonymitaet und gleichzeitig eine gerechtere Verteilung.

Praktische Ansätze
------------------

Geld kann als Information gesehen werden, machen [Dezentrale
Finanzsysteme](/dezentrale_finanzsysteme "wikilink") besonders keine
Ausnahme. Neben aller [Theorie](/geld "wikilink") braucht jedes
Finanzsystem eine Methode oder Mittel um diese Information tauschen,
Kommunizieren zu können. Das können Banknoten aus Papier sein so wie sie
eigentlich jedes zentralbänkischen Währungssystem ausgibt oder eben all
die Flussblüten, TalTaler der vielen
[Regionalgelder](/regionalgeld "wikilink").

Reguläre Banken sind die frühesten Nutzer moderen und vor allem
vernetzter Computersysteme. Schon zu mainframe Zeiten war der
Bankenverkehr 'physikalisch' vor allem ein Informationsverkehr über
Computernetze. Inzwischen sind praktikable Computer und Netze, vor allem
mit dem Internet, exorbitant erschwinglich für jeden geworden. Wer auch
immer eine Währung emittieren will braucht wahrscheinlich 'echte'
haptischen Schein aus Papier oder Edelmetall und vor allem für einen
flüssigen Geldverkehr eine Zentralbanksoftware.

### Anforderungen

Aber welche Forderungen stellt man an eine solche Software

Funktionsfähigkeit ⚙
:   Nur Konzepte wie schön ein Softwareansatz sind bringt noch keine
    Bezahlung zustande

Inter Austausch-Protokoll (p2p) ♺
:   Vor allem für den ja gewollten dezentralen Ansatz ist ein
    interbanken- und sogar ein interinterbankenverkehr elementar.

Free Software ⚑
:   Wäre jeder in der Lage die software selbst zu hosten, oder ist man
    an einen kommerziellen Dienst und dessen Instanz gebunden

                                                                                                      Funktionsfähigkeit   Inter Austausch-Protokoll (p2p)   Free Software
  --------------------------------------------------------------------------------------------------- -------------------- --------------------------------- ---------------
  [Ripple](/ripple "wikilink")                                                                         ✔                    geplant                           ✔
  [Cyclos](/cyclos "wikilink")                                                                         ✔                    ✗                                 ✔
  [Webank](http://blogs.nesta.org.uk/connect/2008/12/webank-are-people-replacing-institutions.html)   ?                    ?                                 ?
  [kuberamoney.com](http://www.kuberamoney.com)                                                       ?                    ?                                 ?
  [Geek Credit](http://home.gna.org/geekcredit)                                                       ?                    ?                                 ?
  [PLUTOS](http://plutos.metux.de)                                                                    ?                    ?                                 ?
  [esp.nongnu.org](http://esp.nongnu.org)                                                             ?                    ?                                 ?
  [epointsystem.org](http://www.epointsystem.org)                                                     ?                    ?                                 ?
  [opencoin.org](http://opencoin.org)                                                                 sehr bald            geplant                           ✔
  [bitcoin.org](http://www.bitcoin.org)                                                               ✔                    geplant                           ✔
  eTerra                                                                                              ?                    ?                                 ?
  OpenMoney                                                                                           ?                    ?                                 ?
  Twollars                                                                                            ?                    ?                                 ?
  [bankofhappiness.org](http://www.bankofhappiness.org)                                               ?                    ?                                 ?

Theoretische Ansätze
--------------------

-   [LUNA](/luna "wikilink") und ein
-   [elia](/elia "wikilink")

Weblinks
--------

-   [complementarycurrency.org](http://www.complementarycurrency.org/ccDatabase/les_public.html)
    große Datenbank über dezentrale Finanzsysteme (leider etwas
    veraltet)
-   [Spiegel-Bericht über ein P2P-System für Online-Communitys der TU
    Darmstadt](http://www.spiegel.de/netzwelt/web/0,1518,615927,00.html)
-   [betacoop.de -
    Beta-Währung](http://www.betacoop.de/betacoop/die-idee/beta-waehrung.html)

