# Skalierbarkeit der einzelnen Regiogelder

Skalierbarkeit der einzelnen Regiogelder Die wesentlichen Antworten
dazu, sehe ich wiederum in der Skalierbarkeit der einzelnen Regiogelder.
Dabei ist die wesentliche Frage, ob die aktuellen Strukturen eine
Verdoppelung oder Verdreifachung der Mitglieder/Nutzerzahl innerhalb von
wenigen Wochen bewältigen können. Ich sehe in den konsensorientierten
basisdemokratischen Strukturen ein wesentliches Instrument, dass
Regiogeld den Menschen nützt. Um jedoch diese Entscheidungsinstrumente
zu schützen wird es wahrscheinlich notwendig sein, den einzelnen Geldern
“[Verfassungen](/verfassung "wikilink")” zu geben die nur mit
unrealistischen Mehrheiten verändert werden können. Die Ausarbeitung
dieser “Verfassungen” verlangt jedoch viel Weitsicht (und Phantasie) für
kommende Entwicklungen. Add: “unrealistische Mehrheiten”: Das könnte
z.B. ein Absatz sein, dass die “Verfassung” nur geändert werden kann,
wenn 90% der Währungsnutzer bei der Generalversammlung anwesend sind.
(Und auch das Ändern dieses Absatzes die selbe Mehrheit benötigt).

Im übrigen glaube ich nicht, dass es zielführend ist, “den Einfluß von
Politik, Wirtschaft und Institutionen” zu verhindern. Es wird kaum
möglich sein, gleichzeitig wesentlicher Akteur (Zentralbank 2.0 ;-)) in
Wirtschaft und Politik zu sein und sich gegen diese Marktteilnehmer
abzuschotten. Ich halte es für wirksamer, zu überlegen, in welchen
Bereichen Einflüsse oder Interessen zu Problemen führen.

Skalierbarkeit der Bonitätsprüfung
----------------------------------

Tauschkreise sind nicht beliebig skalierbar. Tauschkreise (und
Zeitwährungen auf Gegenseitigkeit) benötigen informelle Kontakte als
Kern der Gruppe. In einem Tauschkreis wird wechselseitiger Kredit
gewährt - dieses System kann aber durch unehrliche Menschen extrem
leicht betrogen werden. In den derzeit aktiven Tauschkreisen (und
Zeitwährungen) taucht dieses Problem kaum auf, da die beteiligten
Personen sich meistens kennen (und die Gruppengrösse unterhalb von
Dunbars Number liegt). Die Bonitätsprüfung erfolgt also auf der
informellen Ebene. Wenn ein Tauschkreis eine Grösse von 4000 Mitgliedern
(oder mehr) erreicht, ist diese Art der Bonitätsprüfung nicht mehr
möglich. Es ist wahrscheinlich, dass Betrügereien dann zu einem ernsten
Problem werden.

Die Skalierbarkeit von Regiogeldern liegt daher im wesentlichen in der
Starthilfe für neue Initiativen und in der Teilbarkeit der bestehenden
Tauschkreise. Ein Tauschkreis der unüberschaubar gross wird, sollte sich
unbedingt(!) teilen, um die Probleme zu vermeiden, die die Grösse mit
sich bringt. Wo die ideale Grösse liegt, ist schwer vorhersehbar -
vermutlich wird es sinnvoll sein, dass eine Gruppe die 200 aktive
Teilnehmer hat, sich aufteilt. Ein paar Überlegungen gibt es dazu hier:
<http://www.regiostar.com/72.98.html>?&tx\_ttnews\[tt\_news\]=64&tx\_ttnews\[backPid\]=45&cHash=e259acf3c5

<Kategorie:Regionalgeld>
