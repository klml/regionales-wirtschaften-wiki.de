# RegionalesWirtschaften

Das **RegionalesWirtschaften-Wiki** ist ein zur Dokumentation von Ideen
um die Themen [Regionalgeld](/regionalgeld "wikilink"), [Mittlere
Technologien](/mittlere_technologien "wikilink"), [Peak
Oil](/peak_oil "wikilink") und [Dezentrale
Finanzsysteme](/dezentrale_finanzsysteme "wikilink").

Warum ein Wiki
--------------

Ein Wiki hat gegenüber Blogs, Foren oder Mailinglisten den Vorteil, dass
man direkt an einem Ergebnis arbeitet. Normalerweise formuliert jemand
einen Vorschlag, dieser wird dann von anderen kommentiert, aber dann hat
man noch kein fertiges Ergebnis. Ein Wiki geht hier einen Schritt weiter
und erlaubt das Zusammenarbeiten an dem selben Dokument.

Trotzdem sollten größere Änderungswuensche vorher auf der passenden
Diskussionsseite angesprochen werden. Viele andere Projekte verwenden
auch ein Wiki (z.B.
\[microformats.org/wiki\](http://microformats.org/wiki/)).

1.  1.  Was für ein Wiki

Ein \[mediawiki\](https://www.mediawiki.org) mit diesen
[Extensions](/spezial:version "wikilink").

Für User ist wichtig:

-   man kann [Markdown](https://de.wikipedia.org/wiki/markdown "wikilink") schreiben.

