# Konzept Region Elbtal

Grobentwurf eines Entwicklungskonzepts für die Region Dresden, soll
Verwendung werden in einem “Verein für Erneuerbare Energien und
Regionalentwicklung Elbtal” (EERE)

Leitbild
========

Motivation und Hintergründe unseres Engagements
-----------------------------------------------

Die Initiative zu unserer Zusammenarbeit basiert auf den
gesellschaftlichen, ökonomischen und ökologischen Entwicklungen seit
Beginn des 21. Jahrhunderts. Vorausblickend wollen wir uns den großen
Aufgaben der Zeit stellen:

-   der Wirtschafts- und Finanzkrise
-   der Energiewende ausgelöst durch „[Peak Oil](/peak_oil "wikilink")“
-   der Rohstoffknappheit ausgelöst durch überhöhten Verbrauch und
    Wachstumsorientierung
-   dem Klimawandel und der Umweltzerstörung
-   der ungebremsten “kalten” Globalisierung, die auf Kosten anderer
    Völker, ihrer Lebensräume und Ressourcen beruht

Wir fühlen uns dabei den Ideen von Leopold Kohr und E.F. Schumacher nah,
die mit einem „Zurück zum menschlichen Maß“ Kleinräumigkeit und
Dezentralisierung befürworteten sowie die Stellung des Menschen in den
Mittelpunkt des gesellschaftlichen Handelns forderten.

Wir fühlen uns außerdem den Ideen der „Lokalen Agenda“ nah, die davon
ausgeht, dass auch große Entwicklungen nur durch kleine Schritte vor Ort
in Gang gesetzt werden können. „Global denken, lokal handeln“ ist unser
Leitwort.

Wir fühlen uns den Ideen von R. Buckminster Fuller und anderen nah, die
die Welt als vernetztes Gebilde beschreiben und deshalb Bewusstsein
dafür fordern, dass unser Handeln komplexe Wirkungen zeigt, auf uns
zurückwirkt und wir deshalb langfristig orientiert handeln sollten.

Und wir fühlen uns neueren Bewegungen nah, wie der Eine-Welt-Bewegung
für globalen fairen Handel, der
[Regiogeld](/regiogeld "wikilink")-Bewegung für zukunftsfähige
Zahlungssysteme, der Demokratie-Bewegung für mehr Einfluss des Einzelnen
auf Politik und Gesellschaft, der Energiewende-Bewegung für eine
zukunftsfähige Energie- und Güterversorgung und der OpenSource-Szene für
eine Welt offenen Wissens und freier Verbreitung von Ideen.

Wir fühlen uns dem Gedanken der Entwicklung von uns allen zum „mündigen
und aufgeklärten Menschen“ nahe.

Selbstverständnis des Vereins
-----------------------------

### Vision

Die Elbtalregion wollen wir langfristig zu einer Region entwickeln, in
der sich alle Bereiche der Gesellschaft der bestehenden und wachsenden
ökonomischen, ökologischen und sozialen Herausforderungen unserer Zeit
bewusst sind und danach handeln. Wir müssen uns in diesem Prozess Fragen
zu unseren allgemeinen Werteverständnissen neu beantworten, daraus neue
ethische und moralische Grundsätze für unser Handeln ableiten und es
danach ausrichten.

Und wir müssen uns die Frage stellen, **wie wir in Zukunft in unserer
Region leben und wie wir sie gestalten wollen**.

Unsere regionale Verankerung muss ebenso wie regionales Denken und
Handeln wieder in unser Bewusstsein zurückgeholt werden. Jeder Ort und
jeder Mensch birgt Ressourcen und Fähigkeiten, die zusammengenommen die
Lebensqualität einer Region bestimmen können. Dazu müssen wir uns aber
wieder unserer lokalen Geschichte und Traditionen erinnern, das
vorhandene Wissen neu beleben und in die Zukunft weiterdenken. Wir
müssen bestrebt sein, das Schöne und Lebenswerte unserer Kultur- und
Landschaftsräume einerseits zu erhalten, andererseits aber auch
weiterzuentwickeln, um eine stabile und ausreichende Lebensgrundlage für
alle Menschen in der Region zu schaffen.

Dazu bedarf es einer **aktiven Regionalpolitik**, die wirtschaftliche
und soziale Entwicklungen in der Region nicht nur nationalen und
globalen Ordnungsbestrebungen überlässt, sondern von den Menschen selbst
in eigener Verantwortung vorangetrieben wird. Die Geschicke des eigenen
unmittelbaren Lebensumfeldes müssen wieder soweit wie möglich in die
eigenen Hände genommen und damit beeinflussbar und kalkulierbar gehalten
werden. Eine solche **„Regionalpolitik von unten“**, soll in Zukunft die
Ordnungsprozesse des Staates, des Landkreises und der Kommunen ergänzen
und darüber eine stärkere Hinwendung dieser Ebenen zu Dezentralität und
Subsidiarität bewirken. Damit verbunden ist die Vorstellung, dass Staat,
Landkreise und Kommunen ihre Entscheidungs- und Findungsprozesse
tatsächlich offener gestalten und die Menschen einbeziehen. Gelebte
Demokratie und aktive Politik ließen darüber wenig Raum für Politik-
(Parteien-)verdrossenheit und dem Rückzug ins Private. Ein
**Regionalforum** soll diese regionalpolitischen Aktivitäten so
zusammenführen und vernetzen, dass alle Akteure aneinander partizipieren
und sich in ihrer Arbeit weiter voranbringen. Die Vielfalt der Träger
regionaler Entwicklung bleibt weiterhin bestehen und soll auch befördert
werden; eine Verstärkung der einzelnen Initiativen unter einem
gemeinsamen Schirm, und damit ein gemeinsames Eintreten für
Regionalentwicklung, ist der Impetus dieses Forums.

Auch wenn viele Bereiche, wie z.B. die Wirtschaft, als für den Einzelnen
unbeeinflussbar erscheinen, kann das buchstäbliche Über-den
Tellerrand-schauen Entwicklungschancen auch auf diesen Gebieten zeigen.
So sehen wir für das Elbtal eine **rege Regionalwirtschaft**, die von
ansässigen Unternehmen, Händlern, Handwerkern und den Bürgern getragen
und forciert wird. Deren Wirtschafts- und Handelsbeziehungen orientieren
sich – soweit sie nicht überwiegend international tätig sind – zu einem
großen Teil im Einkauf und Absatz in der Region und werden von der
**Regionalwährung Elbtaler** getragen, die als Komplementärwährung die
regionalen Tauschprozesse stützt und fördert. Regionale Unternehmen
beschäftigen vorwiegend ortsansässige Arbeitskräfte, befördern
kontinuierlich ihre Weiterbildung und zahlen ihren Lohn anteilsmäßig in
der Regionalwährung aus. Regionale Wirtschaftsbeziehungen werden darüber
weiter etabliert, regionale Bindungen in allen Bereichen gestärkt. Über
die Schaffung eines **Regionalfonds** lässt sich der Aufbau und die
Stabilität dieses regionalen Wirtschaftskreislaufes weiter
gewährleisten. Die regionalen Akteure werden darüber vernetzt, in ihrem
Austausch z.B. durch die Bereitstellung von Dienstleistungen,
Infrastruktur, Mikrokrediten und Beteiligungen unterstützt und damit
insgesamt gefördert. Geldanlagen in diese Entwicklung wären für
regionale Anleger in dieser Hinsicht eine sichere Investition, zu deren
Erfolg sie nicht nur selbst beitragen, sondern von ihr auch
uneingeschränkt profitieren können. Mögliche Investitionsbereiche eines
solchen Regionalfonds wären z.B. Land- und Forstwirtschaft, Immobilien
und Kreditwesen.

Neben der Wiederbelebung traditioneller Wirtschaftsbereiche – soweit sie
durch tragfähige Wirtschaftkonzepte unterstützt werden -, sehen wir die
Nutzung der Erneuerbaren Energien als richtungsweisendes
Betätigungsfeld, das die zukünftige Entwicklung der Elbtalregion
entscheidend beeinflussen wird. Die Elbtalregion soll eine **sich selbst
versorgende Region** sein, deren Energiebedarf sich zu 100% aus der
Bewirtschaftung der Erneuerbaren Energien deckt und darüber ein erstes
tragfähiges Netz zur Etablierung regionaler Wirtschaftskreisläufe
ausbildet. Der Klimawandel fordert ein nachhaltiges Umdenken in diese
Richtung schon längst - die momentane Wirtschafts- und Finanzkrise
unterstützt nunmehr diese Entwicklung, indem sie zeigt, dass ein
Weiter-so-Wirtschaften auf Grundlage sich zu Ende neigender Ressourcen
keine Zukunft hat. Diesen Stimmungswandel gilt es auszunutzen und
kurzfristig eine weitestgehende Selbstversorgung in der Region
herzustellen, die durch Energieeffizienz- und Energieeinsparmaßnahmen
flankiert wird. Gerade die ländlich geprägten und strukturschwachen
Räume unserer Region werden darin ein Entwicklungspotential finden, das
Strukturen formen und ausgedünnte Räume wieder beleben kann.

Unsere Vorstellungen zu den zukünftigen Lebens- und Arbeitsweisen in der
Elbtalregion sehen wir als realistisch und umsetzbar an, wenngleich
globale Arbeitsteilungen sowie politische und administrative Strukturen
auch weiterhin unser Leben stark beeinflussen werden. Ersterem wollen
wir gerade mit einer verstärkten regionalen Sichtweise begegnen und über
das Verstehen von Einflüssen und Auswirkungen unserer Lebens- und
Arbeitsstile, Globalisierung menschlicher gestalten. Letzterem können
wir nur durch die Breite der beteiligten Akteure sowie der durch sie
gestalteten Projekte von unserer Arbeit überzeugen. Eine **breite**
**Regionalbewegung** kann Entwicklungsansätze politischer Akteure
beeinflussen und lenken, es kann politische Strukturen wieder für die
Menschen nutzbar machen, sie öffnen, und es kann politische Akteure zum
mitmachen einladen, weil es selbst offen und demokratisch gestaltet ist.
Es soll die Gesellschaft widerspiegeln und in die Gesellschaft
hineinwirken.

### Hauptziel

Hauptziel ist es, einen Entwicklungsprozess anzustoßen, um unsere
Lebensqualität zu verbessern. Wir wollen unsere Vision Wirklichkeit
werden lassen und dafür in den verschiedensten Lebensbereichen Projekte
und Zusammenarbeit im Sinne einer nachhaltigen Regionalentwicklung
voranbringen.

*„Nachhaltige Regionalentwicklung ist ein Konzept, mit dem langfristig
die Lebensqualität in einer Region gesichert werden soll – unter
Beachtung der Wirkungen, die die Lebens- und Wirtschaftsweise der Region
auch für andere Regionen hat.\[...\] Nachhaltige Regionalentwicklung ist
ein Prozess, an dem möglichst viele Akteure aus der Region beteiligt
sind: die Produzenten und Verarbeitungsbetriebe von Nahrungsmitteln, der
Handel; die Handwerker und kleinen Betriebe; die öffentliche Verwaltung
und die Politik; die Verkehrsbetriebe; die Bürgerinnen und Bürger (jeden
Alters) als Konsumenten und als Beteiligte an der Gestaltung der Region;
die Bildungseinrichtungen: Schulen, Kindergärten und Einrichtungen für
Erwachsene, die Universität; Verbände und Vereine.“* Prof. Dr. Ute
Stoltenberg, Universität Lüneburg, Institut für Integrative Studien
[1](http://www.wielkopolska-projekt.org/resources/vortrag_stoltenberg_workshop.pdf)

Regionalentwicklung umfasst also alle Bereiche unserer Gesellschaft.
Strebt man sie in einem Teilbereich an, erfordert eine nachhaltige
Entwicklung Weichenstellungen an daran angrenzende Bereiche und
befördert wiederum auch deren Entwicklung. Dies zeigt: Alle
gesellschaftlichen Akteure sind in ihren Bemühungen letztlich
aufeinander angewiesen und können sich und die Region nur im Austausch
miteinander voranbringen.

*„Regionalentwicklung ist eingebettet in ein Spannungsfeld von Politik
(Arbeitsmarkt-, Raumplanungs-, Regional-, Agrar-, Wirtschafts- und
Sozialpolitik), Verwaltung (Europäische Union (Wettbewerbsrecht –
Beihilfen, Strukturfonds – Zielgebiete und Agrarpolitik), Staats- und
Länderebene, Gemeinde) und regionalen Akteuren (Vereine, Kooperativen,
Beratungsinstitutionen, Regionalmanagements etc.).“* Mag. Dr. Martin
Heintel, Institut für Geographie und Regionalforschung der Universität
Wien[2](http://homepage.univie.ac.at/martin.heintel/publikationen-Dateien/zkl193.pdf)

Diesen Austausch wollen wir befördern, indem wir mit unserem Verein ein
dafür geeignetes Forum anbieten. Wir wollen insbesondere darauf
hinwirken, dass sich natürliche Spannungsfelder auf regionaler Ebene
durch Kontakte und gemeinsame Zielorientierungen abbauen und eine
gemeinsame Interessensverfolgung, ausgedrückt auch durch konkrete
Projektarbeit, daraus hervorgeht.

Wir rufen alle regionalen Akteure auf, diesen
Regionalentwicklungsprozess aktiv mitzugestalten: Unternehmer und
Kommunen, Vereine und Politik, Bildungseinrichtungen und Verwaltung,
Bürger und Bürgerinnen jeden Alters. Wenn wir alle unsere Wünsche,
Fähigkeiten und Fachkenntnisse einbringen entsteht nicht nur eine
Region, wie wir sie uns vorstellen, sondern wir finden auch die Kräfte,
unsere Vorstellung Realität werden zu lassen.

### Arbeitsansätze und Arbeitsbereiche

Dieses Hauptziel wollen wir mit

-   Vernetzung
-   Projektarbeit

und

-   Öffentlichkeitsarbeit erreichen.

**Vernetzung**

Unser Verein soll als regionales Forum fungieren, auf dem sich regionale
Akteure austauschen, miteinander kommunizieren und auf Wegen der
Kooperation für gemeinsame Projekte stark machen und sie auch umsetzen.

Unser Forum soll die Plattform sein,

-   auf der sich Kooperationspartner für Projekte finden lassen,
-   wo Debatten über die Zukunft unserer Region und passende
    Herangehensweisen geführt werden,
-   das als Katalysatorbecken für neue Ideen fungiert,
-   das ein Sprungbrett für die Realisierung konkreter Projekte ist.

**Projektarbeit**

Wir wollen aber auch selbst kreativ sein und eigene Ideen in diesen
Prozess einbringen. Unser besonderes Augenmerk soll dabei den Bereichen

-   regionale Selbstversorgung mit Erneuerbaren Energien
-   regionale Versorgungscluster
-   regionale Integration

und

-   regionale Raum- und Verkehrsplanung gewidmet werden.

Diese Bereiche sollen unter einer differenzierten Leitbildentwicklung
und Zielableitung mit Projektarbeit gefüllt werden, was sowohl
Forschungsarbeit mittels Analysen und Studien als auch konkrete Anlagen
und Institutioneneinrichtung umfassen kann. In diesem Sinne wollen wir
auch auf Auftragsbasis tätig sein.

**Öffentlichkeitsarbeit**

Das Anliegen dieses Vereins, seine Regionalentwicklungsorientierung und
konkrete Arbeit, soll parallel dazu in der Öffentlichkeit bemerkbar
gemacht und gehalten werden. Der Verein kann nur dann zur Entfaltung
kommen, wenn sich soviel Menschen wie möglich darin engagieren und seine
Entwicklungsidee in der Gesellschaft Rückhalt findet.

Neben einer Internetpräsenz und Broschüren, die den Verein, seine Arbeit
und seine Entwicklung allgemein zeigen, sollen regelmäßige
themenbezogene Vorträge die Verankerung der Vereinsarbeit voranbringen.
Mit dieser Ausrichtung soll auch die Offenheit unserer Initiative
signalisiert und Bürger zur Mitarbeit eingeladen werden.

### Arbeitsregion

Unsere Aktivitäten beziehen sich auf die Region um die Stadt Dresden
(“Region Dresden”), zu der wir Orte wie Meißen, Riesa, Radebeul und
Großenhain zählen, als auch Orte wie Pirna, Dippoldiswalde und Bad
Schandau sowie Orte wie Nossen, Radeberg, Radeburg und Freital. Ebenso
arbeiten wir für Menschen in Orten wie Neustadt, Döbeln, Freiberg,
Kamenz, Bischofswerda sowie für all jene Menschen, die im Umfeld dieser
Städte in anderen Orten und an anderen Plätzen leben und sich der
skizzierten Region zugehörig fühlen. Wir ziehen keine starren Grenzen,
Regionen leben, wie ihre Bewohner leben und die Entwicklung der Region
kann ihre Größe verändern. Wir grenzen keinen aus und heißen neue
Bewohner unabhängig ihrer Herkunft willkommen und lassen gehen, wer
anderswo sein Leben leben möchte.

Wir konzentrieren uns auf diese Region, da sie aus unserer Sicht ein
kulturelles Ganzes bilden kann. Es scheint uns realistisch, daß sich
diese Region mit Lebensnotwendigem selbst versorgen kann, auch wenn
derzeit über 1,2 Millionen Einwohner in ihr leben. Wir wünschen eine
Region, die uns schützt vor Ereignissen, die weit entfernt von uns
passieren, die aber stark und vernetzt genug ist, damit wir uns an der
globalen Gesellschaft beteiligen können. Gleichzeitig wünschen wir uns
eine Region, die so klein ist, daß die Stimme des Einzelnen hörbar wird
und ihn betreffende Prozesse durch ihn beeinflusst und gestaltet werden
können.

Die Region Elbtal
=================

Geographisch-landschaftliche Charakterisierung der Region
---------------------------------------------------------

Die Region Elbtal wie wir sie verstehen, orientiert sich zwar an dem
bisher (von staatlicher Seite mit-) entwickelten regionalen Aktionsraum
Region Dresden, umfasst aber neben der Stadt Dresden, sowie der
Landkreise Meißen und Sächsische Schweiz / Osterzgebirge, Teile der noch
jeweils angrenzenden Landkreise und Naturräume.

Diese bewusste Erweiterung der bisher definierten Aktionsräume zu einer
Region Elbtal entspricht unserem Gestaltungsanspruch innerhalb eines
Aktionsradius, der sich an dem sächsischen Flusslauf der Elbe und der an
ihr angrenzenden Landschaften orientiert. Die Elbe schuf nicht nur die
Landschaften dieser Region und prägt darüber die miteinander verbundenen
Kultur- und Wirtschaftsräume, sie stellt darüber hinaus ein verbindendes
Element dar, mit dessen Hilfe sich regionale Identität und regionale
Entwicklung wieder zusammenführen lässt.

Naturräumlich betrachtet umfasst das Elbtal nach unserem Verständnis

-   die Sächsische Schweiz

und

-   das Osterzgebirge **im Süden**,

<!-- -->

-   die westlichen Ausläufer des Westlausitzer Hügel- und Berglandes

sowie

-   des Oberlausitzer Berglandes **im Osten**,

<!-- -->

-   das Mittelsächsische Hügelland südwärts entlang der Freiberger Mulde
    **im Westen**

und

-   die Großenhainer Pflege **im Norden**.

Landschaftlich zählen wir analog zu dieser Aufzählung

-   das Elbsandsteingebirge,
-   den Rabenauer Grund,
-   den Tharandter Wald,
-   das Schönefelder Hochland,
-   die Dresdner Heide,
-   die linkselbischen Täler,
-   die Triebischtäler,
-   die Lößnitz,
-   die Moritzburger Teichgebiete,
-   die Lommatzscher Pflege,
-   den Klosterbezirk Altzella,
-   die Elbweindörfer,
-   die Großenhainer Pflege,
-   die Königsbrücker Heide

und weitere angrenzende Landschaften zu unserer Arbeitsregion.

Strukturelle Ausgangslage in Wirtschaft, Kultur und Gesellschaft
----------------------------------------------------------------

-   prägende Strukturen in Wirtschaft, Kultur und Gesellschaft

Potentiale in der Regionalentwicklung
-------------------------------------

-   Ausgangslage Regionalentwicklung / Stärken & Schwächen

Ziele, Leitbilder und Projekte
==============================

Regionale energetische Selbstversorgung
---------------------------------------

Leitbild: Energieregion Elbtal

Ziele:

-   Energieeffizienz
-   Energieeinsparung
-   Erneuerbare Energien

Projekte:

### Analyse der Strukturen, Akteure und Potentiale in der Energieregion Elbtal

Ausgangsbasis für konkrete Projekte: WEN gibt es in der Region, welche
Personen, welche Vereinigungen (Akteursstrukturen). Aus welchen
Energieträgern wird wieviel Strom verbraucht (Verbrauchsstrukturen), wem
gehören die Netze (Netzstrukturen)

1.  Umsetzung und Instrumentenwahl: informationsbündelnde Webseite
2.  Zeithorizont und Gewichtung: Priorität: Hoch, Zeithorizont: Bis
    Sommer 2010

### Initiierung energieautarker Kommunen

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Konzeptionierung wirtschaftlicher Modellvorhaben

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Einrichtung kommunaler Energieleitstellen

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### “Energie macht Schule”

Schulgebäude oder KiTas haben Dächer, auf denen Photovoltaikanlagen,
Wärmekollektoren oder andere Energieproduktionsanlagen installiert
werden können. Unter dem Stichwort “Energie macht Schule” können
Schuldächer mit Anlagen belegt werden und zugleich die Schüler
informiert sowie die Eltern in die Finanzierung einbezogen werden.

1.  Umsetzung und Instrumentenwahl: Info- und Ausstellungs-Material,
    Info-Veranstaltungen und Integration in den Unterricht (z.B. Physik,
    Gesellschaftskunde) sorgen für Informationsverbreitung, mit Hilfe
    eines Regionalfonds können Finanzierungs-Anteile der Eltern
    gebündelt werden, mit Hilfe regionaler Firmen können die Anlagen
    aufgebaut werden. Notwendig ist Konzeptionierung und Koordination
    durch den Verein, Ansprache von Schulen oder KiTas, die
    Bereitstellung eines Fonds zur Beteiligung der Eltern sowie die
    Vermittlung zwischen Anlagen-Anbietern und Schule/KiTa. Außerdem
    müssen Infomaterialien gestaltet und den Schulen/KiTas zur Verfügung
    gestellt werden, mit denen das Projekt begleitet und die Kinder an
    das Thema Energie herangeführt werden können.
2.  Zeithorizont und Gewichtung: Priorität: niedrig, Zeithorizont: Bis
    Ende 2010
3.  Projektleitung: Norbert

Regionale Versorgungscluster
----------------------------

Leitbild: Erhöhung der regionalen Wertschöpfung / Region soll selbst
erbringen, was sie erbringen kann

Ziele:

-   Förderung regionaler Produkte und Leistungen
-   Nutzung nachwachsender Rohstoffe
-   Aufbau, Förderung und Vernetzung von kleinen und mittelständischen
-   Unternehmen im Handwerk, der Industrie, der Landwirtschaft und der
-   Dienstleistungsbranche
-   Stärkung und regionale Ausrichtung der kommunalen
    [Stadtwerke](/stadtwerke "wikilink")
-   Unterstützung eines regionalen Währungs- und Bartersystems

Projekte:

### Analyse vorhandener Strukturen und Potentiale unterteilt nach Wirtschaftsbranchen und regionaler Verankerung

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Aufbau eines Regionalfonds

1.  Umsetzung und Instrumentenwahl: Ein genossenschaftsorientierter
    Regionalfond kann dazu genutzt werden, um Geld für Energie- und
    andere Projekte zu sammeln. Die Ausschüttung erfolgt nicht zwingend
    wieder in Geld, sondern im Idealfall in Scheinen, die Anspruch auf
    die Früchte der realisierten Projekte darstellen. Ein vergleichbarer
    Ansatz wird von der RegioStar-Genossenschaft derzeit umgesetzt, eine
    Partnerschaft oder Unterstützung aus deren Richtung ist möglich und
    würde zugleich die überregionale Vernetzung intensivieren. Notwendig
    ist die Gründung einer Genossenschaft sowie Entwurf von Verträgen
    und rechtlichen Rahmenbedingungen zum Einsatz der Fond-Mittel.
    Möglich wäre auf Basis dieses Regionalfond-Ansatzes eine
    Regionalwährung Elbtaler abzuleiten (in Form der Anspruchs-Scheine).
2.  Zeithorizont und Gewichtung: Priorität: mittel, Zeithorizont: bis
    Sommer 2011
3.  Projektleitung: Norbert
4.  Partner: RegioStar eG, Volksbank Pirna, Sparkasse Meißen

### Einführung der Regionalwährung Elbtaler

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Aufbau und Vernetzung regionaler Wirtschaftsakteure und von Erzeuger- und Verbrauchergemeinschaften

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Entwicklung von Direktvermarktungsmodellen und deren Stärkung

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Mehrgenerationenhäuser

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Entwicklung kombinierter Versorgungsmodelle für den ländlichen Raum

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

Regionale Integration
---------------------

Leitbild: Schaffung und Festigung einer regionalen Identität
(Kooperationen fördern und stärken) Ziele:

-   Stärkung des [regional
    governance](/regional_governance "wikilink")-Ansatzes in den Kommunen
    und den Landkreisen (interkommunale Zusammenarbeit)
-   Institutionalisierung alternativer gesellschaftspolitischer
    Partizipationsformen
-   Vernetzung und Bündelung regionaler Akteure
-   Stärkung des Kulturraumes

Projekte:

### Entwicklung und Stärkung neuer offener Partizipations- und Kommunikationsformen

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Förderung und Verbreitung regionalen Kulturgutes

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Informationen zur regionalen Entwicklung: Regionalbroschüre

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Förderung von OpenSource und OpenHardware

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

Regionale Raum- und Verkehrsplanung
-----------------------------------

Leitbild: Förderung der innerregionalen Mobilität (Menschen sollen ihre
Zeit mit Leben verbringen und nicht mit fahren)

Ziele:

-   regionale Mobilitätsansätze
-   Entwicklung integrierter Vor-Ort-Verkehrskonzepte

Projekte:

### BürgerBusse und BürgerBahnen

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Sharing-Modelle: CarSharing / RoomSharing

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### Optimierung von kommunalen Raumentwicklungsprogrammen hinsichtlich erneuerbarer Energien

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:

### „Bürger gestalten ihre Kommune“

1.  Umsetzung und Instrumentenwahl:
2.  Zeithorizont und Gewichtung:
3.  Beispiele: alte Dorfzentren wiederbeleben

Finanzierung und Durchführung der Vereinsarbeit
===============================================

Links & Weiterführendes
=======================

-   Regionales Entwicklungskonzept der Region Dresden:
    <http://region.dresden.de/media/pdf/region/Abschlussbericht.pdf>
-   Regionalwährungsprojekt Elbtaler: <http://www.elbtaler.de>

<Kategorie:Regionalgeld> <Kategorie:Regional>
