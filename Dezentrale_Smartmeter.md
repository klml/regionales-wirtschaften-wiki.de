# Dezentrale Smartmeter

Moderne Stromnetze sollen nicht nur einfache galvanische
[Energierträger](/energie "wikilink") sein, sondern mit [Demand Side
Management](https://de.wikipedia.org/wiki/demand_side_management "wikilink") und [intelligenten
Zählers](https://de.wikipedia.org/wiki/intelligenter_zähler "wikilink") den Verbraucher und Erzeuger
dazu befähigen, auf einfache Art und Weise den Energieverbrauch
permanent [entlasten](/netzentlastung "wikilink") zu können.

Am wichtigsten ist es, in kostspieligen Verbrauchsspitzen und eventuell
auftretenden Unterversorgungen Verbraucher, die keinen stetigen Bedarf
haben, zu [verschieben](/netzentlastung "wikilink"). Derartige
Verbraucher sind vor allem Heizungen, Kühlaggregate, Waschmaschinen und
sogar [Aluminiumhütten](http://www.manager-magazin.de/unternehmen/industrie/0,2828,766712,00.html),
eigentlich alles außer Licht, Kaffeemaschine, TV und Rechner;)


## Datenschutz

*Wenn der Windpark mit der Waschmaschine redet* lautet die Vorstellung
in Hochglanzbroschüren. Aber vielleicht will ich gar nicht, dass
Windpark mit meiner Waschmaschine redet. Die Vision des
[Smartgrids](https://de.wikipedia.org/wiki/intelligentes_stromnetz "wikilink") oder des [virtuellen
Kraftwerks](https://de.wikipedia.org/wiki/virtuelles_kraftwerk "wikilink") hat auch Schattenseiten.
Der
[Datenschutz](http://www.heise.de/newsticker/meldung/Datenschuetzer-fordern-klare-Regeln-bei-Smart-Meter-Nutzung-1131632.html)
findet quasi nicht statt. Durch die übermittelten fein granulierten
Stromprofile kann der Energieversorger oder jeder der Zugriff auf so ein
Profil hat. mit recht genau analysieren welchen Lebensrhythmus der
menschliche Verbraucher pflegt oder welches
[Fernsehprogramm](http://www.heise.de/security/meldung/Smart-Meter-verraten-Fernsehprogramm-1346166.html)
er konsumiert. Da hilft auch keine [Künstliche Intelligenz](https://www.sueddeutsche.de/wissen/energiewende-stromnetz-kuenstliche-intelligenz-ki-1.4261047).

Auch wenn das Thema Datenschutz als Feigenblatt angefasst wird, sei dazu
[aber eine kontinuierliche Kommunikation zwischen Kraftwerken, dezentralen Stromerzeugern und Verbrauchern nötig, so die Telekom.](http://www.golem.de/news/smart-metering-intelligente-stromzaehler-ohne-vorratsdatenspeicherung-bauen-1207-93163.html) oder [die neue Haustechnik aber ist auf Datenerhebung geradezu
angewiesen.](http://www.sueddeutsche.de/digital/sicherheit-von-smart-home-offen-wie-ein-scheunentor-1.2227389)


Dabei stellt sich die Frage, **warum** ein Energieversorger meinen
aktuellen Verbrauch wissen muss, um Sekundärgeräte abzuschalten? Kann
das der Verbraucher nicht einfach selbst? Der Verbraucher muss nur
wissen wann Strom knapp oder reichlich vorhanden ist. Gibt es viel Strom
kann er Strom verbrauchen, gibt es weniger, soll er das eben nicht. Wird
der Verbraucher über [variable Tarife](https://de.wikipedia.org/wiki/variable_tarife "wikilink")
motiviert Stromabnahme zu verschieben, gibt keinen Grund warum der
Energieversorger das Lastprofil jedes einzelnen auslesen muss.

Vergleichbar zur Umsetzung beim klassischen
[Nachtstrom](https://de.wikipedia.org/wiki/nachtstrom "wikilink"), könnte doch einfach ein Signal
gesendet werden, dass dem Verbraucher sagt wann sekundärer Strom genutzt
werden kann und wann nicht: Der Hausanschluss hat zwei Stromzähler und
durch eine [Rundsteuertechnik](https://de.wikipedia.org/wiki/rundsteuertechnik "wikilink") schaltet
der Zähler automatisch um. Es werden so zwei Stromsummen gebildet, die
zu unterschiedlichen Preisen abgerechnet werden. Das geht mit digitalen
Zählern deutlich granularer, wodurch das Konzept erweitert werden kann.

Stellt sich die Frage, warum es keine Smartmeter gibt, die einfach genau
das tun. Nicht den Verbrauch an eine Zentrale melden, sondern den
Stromverbrauch je nach Zeitfenster mit einem Preis multiplizieren und
dann eine Summe an Geld regelmäßig abrechnen. Er kann zusätzlich die
Stromsumme mitliefern. Wenn der Stromlieferant zeitnah den Strompreis
[digital signierten](https://de.wikipedia.org/wiki/digitale_signatur "wikilink") und maschinenlesbar
veröffentlicht, kann Strom nicht nur nach vorgegeben Zeitfenstern
berechnet werden, sondern nach aktueller Angebotslage. Je nach
Verbraucherwunsch könnte z.B. am Ende des Monats die Geldsumme nach
Hause telefoniert werden, oder man nutzt das bisherige Ablesesystem. Auf
diesen Weise hätte man ein
[selbstregulierendes](https://de.wikipedia.org/wiki/selbstregulation "wikilink") Smartgrid und die
informationelle Selbstbestimmung des Verbrauchers bliebe geschützt.

Verbraucher wie Waschmaschinen oder die Gefriertruhe können den Preis
autonom abfragen und “sich überlegen”, ob sie morgens um 8 bei 0,43
€/kWh unbedingt kühlen muss oder ob die Gefriertruhe nachts um vier Uhr
ein bisschen vor kühlen soll, zu z.B. 0,13 €/kWh und so [Netze
entlasten](/netzentlastung "wikilink").

Ein erster Schritt Richtung Datenschutz wäre es die Abfragefrequenz
variabel zu gestalten[^researchgateSelf]. Wenn auch hier sich die
Frage stellt, warum muss mein Versorger wissen was ich verbrauche, es
genügt wenn er den Preis verändert.

[^researchgateSelf]: [A Self-Organising Approach for Smart Meter Communication Systems](http://www.researchgate.net/publication/264789009_A_Self-Organising_Approach_for_Smart_Meter_Communication_Systems): “Hence, an ideal interval cannot be predicted a priori, but needs to be adapted dynamically.”

## Single Point of Failure und Marktautonomie

Ein weiterer Kritikpunkt an derzeitigen Smartgrid Ansätzen ist die
zentrale Steuereinheit. Tritt hier ein technischer Fehler oder ein
Computervirus auf, so werden im gesamten Versorgungsgebiet die
elektrischen Verbraucher fehl gestellt. Außerdem, wer entscheidet
überhaupt über die Schaltrhythmen? Wer entscheidet, dass es das Beste
ist, nur die morgendlichen, mittaglichen und abendlichen Stromspitzen
abzufangen? Vielleicht findet ein Erzeuger oder ein Verbraucher heraus,
dass für seine Konstellation aus elektrischen Maschinen und
Lebensrhythmus ein ganz spezielle Taktung funktioniert. Das sollte doch
berücksichtigt werden können, oder? Dieses weite Innovationsfeld darf
man nicht einer Zentralinstanz überlassen, die einseitige Interessen hat
und auch nicht kontrolliert wird.

Die derzeitigen Entwürfe von Smartgrids sind in zentralistischen
Systemen umgesetzt, in welchen der Energieversorger die Macht über die
Schalter in meiner Wohnung inne hat. Der mündige Verbraucher aber soll
selbst entscheiden, wann er Strom bezieht und wann nicht. Wenn ich
morgens um 8 Uhr kühlen will, dann soll ich das tun dürfen, wobei ich
halt einen höheren Preis bezahlen muss. Jeder Verbraucher steht so in
der Verantwortung, in seiner persönlichen wirtschaftlichen Verantwortung
Strom sinnvoll zu nutzen.

Der Markt, auch wenn er durch Missbrauch einen schlechten Ruf hast, ist
immer noch eine der besten Methoden zur Güterallokation. Den
Einfallsreichtum der Maschinenhersteller und die Schläue der Verbraucher
zu provozieren und wirtschaftlich zu belohnen zeigt sich stets als die
bessere Innovationsquelle als planwirtschaftliches Kontrollversuche,
egal ob vom ZK oder von E.on.

### Marktgefahren

Auch wenn der Markt eigentlich ein gute Allokationsmethode ist, er hat
auch inhärente Problemfälle, den es kann sich [eine Nachfrage-Blase
bilden](http://www.haustechnikdialog.de/News/17770/Chaos-am-Strommarkt-vorprogrammiert-Blackout-durch-Smart-Metering-).
Ist nur die Frage ob man diese Gefahren durch ein generelle
Zentralisierung und Preisvereinheitlchung lösen muss, oder ob man
trotzdem institutionell Spitzenentlaster vorhalten muss?

## Ähnliche Projekte


-   [mysmartgrid.de](https://www.mysmartgrid.de): “Regelung des
    Energieverbrauchs von wärmespeichernden Verbrauchern wie
    Tiefkühltruhen oder Wärmepumpen. Dabei sollen die internen
    Regelungen der Geräte durch externe Signale übersteuert werden. Ziel
    ist es, im Falle eines Überangebotes an Strom (z.B. viel
    Windenergie) möglichst viel Energie aufzunehmen, um später
    entsprechend weniger Strom zu benötigen.”
-   [buzzn ist ein Netzwerk, das Menschen über Strom miteinander
    verbindet.](https://www.buzzn.net/)
-   [utilicount.com - Der Preis als aktuelles
    Knappheitssignal](http://www.utilicount.com/energie-intelligenz/projekt-smart-watts.html)
-   [Easy Smart Grid](https://www.easysg.de/de/unser-besonderer-ansatz/der-schluessel-sind-netzzustandsvariable/): "Der Schlüssel sind Netzzustandsvariable"
-   [Autonomes selbstlernendes Energiemanagment](http://ufegmbh.de/pool/download/texte/Poster2.pdf)

##  Bedarf

-   [Per Digitaltechnik wäre es dann auch möglich, die im Viertel
    erzeugte Energie in engmaschige, intelligent verknüpfte Netze
    einzuspeisen und nach Bedarf zu
    verteilen.](http://www.sueddeutsche.de/muenchen/neue-energie-die-ganze-stadt-eine-quelle-der-solarenergie-1.3252795)

<Kategorie:Energie>
