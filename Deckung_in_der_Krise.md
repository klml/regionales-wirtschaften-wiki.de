# Deckung in der Krise

Im Bereich der Deckung gibt es zwei wesentliche Problemfelder:

Was statt der [Eurodeckung](/eurodeckung "wikilink")?

Wann gibt man die Eurodeckung auf?

Was statt Euro-Deckung?
-----------------------

Möglichkeiten Regionalwährungen zu decken gibt es in der Praxis viele.
Leistungsdeckung (“Gutscheine” die von allen Teilnehmern als Geldwert
akzeptiert werden) Das Problem dabei ist, das diese Regionalwährung der
Inflation des Euro 1:1 unterliegt. Man könnte dies beheben, indem man
ein eingefrorenes Preisniveau vereinbart. Ich sehe dies jedoch als
problematisch in der Abwicklung - viele Teilnehmer wären (meiner Meinung
nach) davon moralisch überfordert.

Leistungsdeckung mit Leitbetrieben
----------------------------------

Es gab Überlegungen zu “Eisenbahngeld”. Die Währungseinheit sollte in
der Verpflichtung bestehen eine Person einen Kilometer zu
transportieren. - Ich sehe es als interessanten Ansatz, es erfordert
jedoch grosse (oder zumindest homogene) Betriebe. - Eine Abwandlung
könnte in einer “Agrardeckung” liegen. Geldschöpfung in der
Landwirtschaft, mit der Verpflichtung pro “Agrano” ein Ei, 20 dag Weizen
oder 3 dag Rindfleisch zu liefern.

Golddeckung
-----------

Deckung mit Edelmetall - ein wesentlicher Vorteil läge in der gleitenden
Umwandelbarkeit. Man könnte bestehende Eurodeckungen für den Ankauf von
Gold verwenden. Und die Währungsdefinition in die Form “ein Regional ist
0,3 gramm Gold” bringen. Einziges Problem sind die schwankenden Kurse
Euro/Edelmetall. Doppelte Preisauszeichnungen wären erforderlich. Sind
jedoch (siehe eGold usw) möglich.

Davon abgesehen, bietet sich die Deckung mit 5-Euro-Silbermünzen an: Die
Bankeinlagen, die jetzt als Deckung der Regionalwährungen dienen werden
zu einem (Gross-)-Teil in 5-Euro Silbermünzen gewechselt. Die
Euro-Deckung bliebe erhalten und es gäbe einen klaren Zeitpunkt der
Trennung vom Euro: wenn der Metallwert der Silbermünze den Nominalwert
der Münze übersteigt, ist der Zeitpunkt gekommen, den Mitgliedern
mitzuteilen, dass doppelte Preisauszeichnung das Gebot der Stunde ist
und ab sofort die Regionalwährung in Silber UND Euro getauscht wird.

Stundendeckung
--------------

Die Deckung mit “1 Stunde Arbeit” erfordert entweder sehr reife und
idealistische - oder sehr homogene Teilnehmer. Es wird kaum gelingen
einen Stundenkreis zu starten, in dem dauerhaft eine Stunde
Steuerberater gegen eine Stunde Babysitten getauscht wird.

Wann gibt man die Eurodeckung auf?
----------------------------------

Das wesentliche Problem zu Deckungsfragen ist jedoch: Wann gibt man die
Bindung an den Euro auf? Bei einer Inflationsrate von 10%? Wenn die
Geldmenge M3 um 20 % im Jahr wächst? Wenn 30 % der Banken pleite sind?

Damit stellt sich natürlich auch die Frage: Zieht man in der
Regionalwährung eine Währungsreform des Euro einfach vor? - Wenn ein
Laib Brot 20 Euro kostet - ist dass der Zeitpunkt zu sagen: Stop ? oder
wartet man bis zum Brotpreis von 200 Euro? Oder 2000 Euro? Ich befasse
mich deshalb hauptsächlich mit inflationären Szenarien, weil ich diese
für wesentlich wahrscheinlicher (und problematischer) halte. Persönlich
fürchte ich, dass viele Regionalwährungen die Trennung vom Euro nicht
schaffen werden und mit ihm gemeinsam in die Hyperinflation gehen
werden.

Liquidität für die Region/Clearing/neutrale Scheine Ich sehe (bei
unterschiedlichen Deckungen) nur eine einzige Möglichkeit
Clearingstellen zu schaffen: freie Wechselkurse mit transparenter
Abwicklung der Transaktionen an diesen Clearingstellen sollten auch
sämtliche Tobin-Tax-ähnlichen Abgaben eingehoben werden. Die Einrichtung
von Clearingstellen ist jedoch nebensächlich, da es für den Anwender in
der Krise durchaus praktisch ist, mehrere Komplementärwährungen zu
verwenden.

<Kategorie:Geld>
