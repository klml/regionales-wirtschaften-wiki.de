# Mittlere Technologien

Definition
----------

Mögliche Definitionen für mittlere Technologien:

> „Mittlere Technologien sind technologische Konzepte die es erlauben,
> mit kleinen Stückzahlen auch im Rahmen regionaler
> Wirtschaftskreisläufe ökonomisch sinnvoll zu handeln.“ - Dr. Reinhard
> Stransfeld

> Mittlere Technologien sind die Antwort auf folgende Frage: Wie sollen
> *bezahlbare* Maschinen, Herstellungsverfahren und Produkte aussehen,
> die es den Menschen ermöglichen, die Ressourcen ihrer Region
> nachhaltig zu nutzen um selbst die Dinge herzustellen, die für das
> tägliche Leben in der Region gebraucht werden? Diese Frage muss jeder
> für seinen Berufstand selbst durch Kreativität und Fachwissen
> beantworten und wenn er eine Antwort auf diese Frage gefunden hat,
> dann hat er ein Konzept für eine mittlere Technologie erschaffen. -
> Dipl.-Ing. Michael Klotsche

Mittlere Technologien sollen gleichzeitig so gestaltet sein, dass sie
dem heutigen Stand der Technik entsprechen. Die technische
Herausforderung liegt hierbei darin, das technische Know-How, das in der
Industriellen Massenproduktion Stand der Technik ist, sinnvoll auf die
Herstellung von Produkten in kleiner Stückzahl zu übertragen.

Beispiel
--------

Diese theoretischen Aussagen sollen anhand eines Beispiels erläutert
werden:

Was nun genau eine mittlere Technologie ist, hängt immer vom jeweiligen
Anwendungsfall ab. Für einen Menschen in einem Land in der dritten Welt
wird eine mittlere Technologie anders aussehen als für einen Menschen in
einem industrialisierten Land. Es kommt immer darauf an, ob die
Technologie und ihre Herstellung für die Anwender mit den Mitteln, die
ihnen in der Region zu Verfügung stehen, bezahlbar ist.

Warum soll eine Technologie mit den Mitteln der Anwender in der Region
bezahlbar sein? Mittlere Technologien bedeuten wirtschaftliche- und
somit auch politische Unabhängigkeit. Menschen, die gezielt mittlere
Technologien anwenden, gewinnen an wirtschaftlicher und politischer
Macht. Das soll an einem Beispiel erläutert werden: Man stelle sich eine
kleine Gemeinde vor, die von Arbeitslosigkeit geprägt ist. Viele
Menschen in dieser Gemeinde sind von der Sozialhilfe abhängig. Es gibt
einfach zu wenig Unternehmen in der Gemeinde und in deren Umgebung. Die
Menschen, die Häuser besitzen können ihre Häuser nicht renovieren und
hoffen, dass die Bausubstanz noch so lange hält, bis bessere Zeiten
kommen. Der Verein für Sonnenenergie in dieser Gemeinde macht den
Vorschlag, alle Häuser mit Solarkollektoren auszustatten, damit die
Gemeinde nicht mehr so viel Brennstoffe kaufen muss und das Geld in der
Gemeinde bleibt. Es fehlt aber an Geld, um so viele Gebäude mit
Solarkollektoren auszustatten.

Eine Lösung für dieses Problem sind Solarkollektoren, die durch die
Handwerksbetriebe in der Gemeinde und und durch Eigenarbeit der Nutzer
hergestellt werden können. Dazu wird zuerst einmal überlegt, welche
Rohstoffe in der Gemeinde vorhanden sind. In der Gemeinde befindet sich
ein Sägewerk, aus dem Holz bezogen werden kann. Glasscheiben und
Steuergeräte müssen von Firmen außerhalb der Gemeinde gekauft werden.
Das Isolationsmaterial der Solarkollektoren kann aus Hanf hergestellt
werden, der in der Gemeinde angebaut werden kann. Aus diesem Materialien
wird dann ein Solar-Luftkollektor entwickelt, der als
Heizungsunterstützung für Wohnräume eingesetzt werden kann. Der
Solarkollektor kann von den Handwerksbetrieben in der Gemeinde und von
den Nutzern direkt in Eigenarbeit gefertigt werden. Dadurch müssen keine
Löhne an Leute gezahlt werden, die außerhalb der Gemeinde arbeiten. Das
Geld bleibt im Ort. Die Menschen in der Gemeinde können die
Solarkollektoren auf diese Weise selbst bezahlen.

Durch die Heizungsunterstützung der Solar-Luftkollektoren werden
Brennstoffe eingespart, die nicht mehr durch die Einwohner der Gemeinde
zugekauft werden müssen. Das eingesparte Geld bleibt übrig, um andere
notwendige Dinge kaufen zu können. Hätten sich die Menschen in der
Gemeinde herkömmliche Warmwasser-Kollektoren mit Absorbern aus Kupfer
gekauft, hätten sie diese Kollektoren über Kredite finanzieren müssen.
Die Kreditgeber hätten dann entschieden, ob die Menschen diese
Solarkollektoren bekommen. Die Gemeinde hätte auch in diesem Fall die
Brennstoffkosten gespart, aber dafür hätten sie die Hersteller der
Solarkollektoren bezahlen müssen. Zum Schluss wären die Geldbeutel in
der Gemeinde genau so leer wie ohne Solarkollektoren. Das wird durch den
Einsatz mittlerer Technologien verhindert. Mittlere Technologien geben
ihren Nutzern ein Stück wirtschaftliche Unabhängigkeit zurück und somit
auch ein Stück Selbstbestimmung.

Projekte
--------

-   [Global Village Construction
    Set](http://opensourceecology.org/wiki/Global_Village_Construction_Set/de)

siehe auch:

-   [Methode der
    Selbstfinanzierung](/methode_der_selbstfinanzierung "wikilink")

Weblinks
--------

-   [nuevalandia.net Mittlere
    Technologien](http://wiki.nuevalandia.net/Mittlere_Technologien)

