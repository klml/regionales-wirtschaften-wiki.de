# Eurodeckung

Warum ist die Euro-Deckung problematisch?

Nachdem das Vertrauen in Aktien und Banken schrumpft, wird in einem der
nächsten Schritte das Vertrauen in den Staat und staatliche
Institutionen untergehen. Da viele Komplementärwährungen auf dem Euro
basieren, werden sie von dem Vertrauensverlust, einer eventuellen
Inflation oder auch Währungsreform 1:1 getroffen werden.

Das wesentliche Problem zu Deckungsfragen ist daher:

Wann gibt man die Bindung an den Euro auf?
------------------------------------------

Bei einer Inflationsrate von 10%? Wenn die Geldmenge M3 um 20 % im Jahr
wächst? Wenn 30 % der Banken pleite sind?

Damit stellt sich natürlich auch die Frage: Zieht man in der
Regionalwährung eine Währungsreform des Euro einfach vor? - Wenn ein
Laib Brot 20 Euro kostet - ist dass der Zeitpunkt zu sagen: Stop ? oder
wartet man bis zum Brotpreis von 200 Euro? Oder 2000 Euro? Ich befasse
mich deshalb hauptsächlich mit inflationären Szenarien, weil ich diese
für wesentlich wahrscheinlicher (und problematischer) halte. Persönlich
fürchte ich, dass viele Regionalwährungen die Trennung vom Euro nicht
schaffen werden und mit ihm gemeinsam in die Hyperinflation gehen
werden.

Eine mögliche Lösung wäre, die Bankeinlagen, die jetzt als Deckung der
Regionalwährungen dienen zu einem (Gross-)-Teil in 5-Euro Silbermünzen
umzuwechseln. Die Euro-Deckung bliebe erhalten und es gäbe einen klaren
Zeitpunkt der Trennung vom Euro: wenn der Metallwert der
[Silbermünze](/silbermünze "wikilink") den Nominalwert der Münze
übersteigt, ist der Zeitpunkt gekommen, den Mitgliedern mitzuteilen,
dass doppelte Preisauszeichnung das Gebot der Stunde ist und ab sofort
die Regionalwährung in Silber UND Euro getauscht wird.

<Kategorie:Geld>
