# Geld

ist ein Zwischentauschmittel, welches sich von anderen Tauschmitteln
dadurch unterscheidet, dass es nicht unmittelbar den Bedarf eines
Tauschpartners befriedigt, sondern aufgrund allgemeiner Anerkennung zum
weiteren [Tausch](/tausch "wikilink") eingesetzt werden kann.

Theorie
-------

Ein wesentliches Problem der Geldtheorie ist, dass die verschiedenen
Positionen “Was ist Geld?” teilweise widersprüchlich sind. Ein
wesentlicher Faktor in der Diskussion liegt auch darin, dass die
historische Entwicklung (Geld ist eine Ware) sich von der erlebten
Gegenwart (Geld ist ein vom Staat gesetztes Symbol) stark unterscheidet.
Die Verfechter der [Golddeckung](/golddeckung "wikilink") erklären diesen
Widerspruch damit, dass alle Einführungen von ungedeckte
[Papiergeld](/papiergeld "wikilink") gescheitert sind und auch das
aktuelle [Fiatmoney](/fiatmoney "wikilink") scheitern wird.

### Geld ist eine Ware

In einer Naturalwirtschaft bildet sich im Lauf der Zeit ein Gut heraus,
das die Funktion des Geldes als allgemein akzeptiertes Tauschmittel am
besten ausfüllen kann. Geld ist somit immer und überall ein Gut, das
zunächst nur von einigen, später von immer mehr Marktteilnehmern
nachgefragt wird und sich wie bereits erwähnt durch die vergleichsweise
höchste Absatzfähigkeit von allen anderen Gütern essentiell
unterscheidet. Je nach historischem Kontext erwiesen sich verschiedenste
Güter als besonders absatzfähig und nahmen folglich die Rolle des Geldes
ein: Salz (sog. ), Bronze, Kabeljau, Gold und Silber – um nur einige zu
nennen. Jedem Marktprozeß ist die Tendenz inhärent, daß Güter besserer
Qualität Güter schlechterer Qualität verdrängen. Der Markt für das Geld
stellt keine Ausnahme dar und so setzten sich mit der zunehmenden
Vernetzung der vormals isolierten Wirtschaftsräume die beiden
letztgenannten Edelmetalle als Geld durch. Geld ist jene Ware, die von
den Bürgern als das geeignetste Tauschmittel empfunden wird. Es ist ein
Ergebnis der Markttransaktionen aller Menschen und kann nicht per Gesetz
verordnet werden.

### Geld ist Schuld

ist eine Theorie, dass Geld statt als Tauschmittelgut als übertragbares
Schuldverhältnis (Kredit) und damit als Verpflichtung definiert ist.
Wenn auch nicht allgemein in der Wissenschaft anerkannt, so stimmt diese
These mit dem praktischen Erleben des bargeldlosen Zahlungsverkehrs
wesentlich besser als die klassische Sicht: “Geld ist eine Ware”
überein.

Prinzipiell ist dieses Vorgehen *jedermann* möglich! Jeder kann
Schuldscheine erstellen, die er und andere als Zahlungsmittel verwenden.
Wichtig für das Funktionieren solch eines Systems ist das Vertrauen in
den Schuldner. Dabei stellt jeder ausgestellte Schuldschein eine
zweiseitige Beziehung dar:

-   der Besitzer des Scheines hat Anspruch auf Leistungen
    (Leistungsguthaben)
-   der Herausgeber des Scheines hat die Pflicht zur Leistungserbringung
    (Leistungsschuld)

Jeder dritte, der davon ausgeht, dass entweder der Leistungsschuldner
oder ein anderer den Schein anerkennt, kann den Schein ebenfalls
akzeptieren.

Konkret lief solch ein Vorgehen bei uns wie folgt ab: Von einem
befreundeten Tischler liessen wir uns ein Bücherregal bauen. Den
Rechnungsbetrag akzeptierte er zu 20% in einer Art Schuldschein: Dieser
Schuldschein existierte nie real auf Papier, sondern nur als
Verrechnungsposten in einer Excel-Tabelle. Jedoch akzeptiert ein
befreundeter Webdesigner ihn als Zahlungsmittel, so dass unser Tischler
in absehbarer Zeit seine Internetpräsenz von diesem Webdesigner machen
lassen wird. Der Designer wiederum kann mit dem Schuldschein meine
Fähigkeiten für dieses oder andere Webprojekte einkaufen.

#### Aspekte und Formen von Geld als Schuld:

##### Geld ist Information

Diese Tatsache ist bereits daran erkennbar, dass das heutige Geld zum
Grossteil in Computern verwaltet wird. Jeder Besitzer eines Kontos bei
einer Bank wird dies bestätigen. Wenn man “Geld” im Internet abbilden
will ergibt sich daraus die Frage: Wie muss diese Information “Geld”
strukturiert sein, um im Internet verwendet zu werden? Schließlich kann
in Computern prinzipiell jeder beliebig Bits und Bytes manipulieren...

##### Geld ist Kommunikation

Information ist mit *Kommunikation* eng verbunden. Informationen, welche
nicht kommuniziert werden, sind nutzlos. Oder anders: Nur durch
Kommunikation wird Information nutzbar. Kommunikation andererseits setzt
ein *Kommunikationsprotokoll* voraus. Im Internet ist beispielsweise das
hypertext-transfer-protocol (http) die Grundlage für das WWW, in der
“realen Welt” ist es der Sprachschatz, der Kommunikation möglich macht.
Mit diesen überlegungen soll deshalb eine Grundlage für eine
Kommunikation über wirtschaftliche Vorgänge erschaffen werden: Ein
[Protokoll](/protokoll "wikilink") für den Austausch wirtschaftlicher
Informationen.

### Gelddiskussion

All diese Modelle sind eben e. Keines ist unumstößlich wahr und erklärt
alles, keines ist absolut falsch. So wie man das verbrennen (oxidieren)
von Schwefel mit dem Periodensystem erklären kann, so kann man mit
diesem Modell (Moleküle sind eben keine bunten Kugeln mit stecken
zwischen drin) den molekularen nicht erklären.

So kann man eben auch Geld uns alles was damit zusammenhängt, wie Glück,
Macht, Sucht, Arbeitsteilung, Religion, Ablehnung nicht mit
Güteraustausch, Information oder deren Austausch (Kommunikation),
Vertrauen oder Schuld erklären. Der Mix machts.

Das Taschengeld kann ein Kind nicht am Leben erhalten, aber es gibt ein
Stück vertrauen. Der Apfel essende Stahlbetonbauer der eine
Autobahnbrücke baut, über die eine Klavierlehrerin fährt, die dem Sohn
des Obsthändlers Unterricht gibt lässt sich mit dem Austausch am besten
erklären.

Unsere durchglobalisiert Wirtschaft wird meistens mit dem
Austauschmodell erklärt, derzeitige Vorgänge könnte man aber auch mit
dem Schuldmodell wenigstens mal betrachten, das aber genauso wenig wahr
ist wie das Moleküle Schaschlikspiese mit Knetgummibobbel sind.

Anwendung
---------

überträgt man dieses Prinzip auf grössere Strukturen sowie ins Internet,
so wird deutlich:

-   jeder kann Geld machen
-   jeder ist frei, eigenes oder fremdes Geld als Zahlungsmittel
    anzuerkennen
-   jeder ist frei, mehr als ein Zahlungsmittel zu nutzen
-   jeder, der ein Geld akzeptiert, wird Teil des jeweiligen
    Geld-Netzwerkes; also des Netzwerkes jener
    [Akteure](/akteur "wikilink"), die ein bestimmtes Zahlungsmittel
    nutzen

Deutlich sollte werden, dass unser Geldverständnis um eine Ebene
erweitert werden muss:

-   man besitzt Geld nicht mehr nur in einer bestimmten Menge
    (Quantität)
-   sondern auch in einer bestimmten “Ausprägung” (Qualität)

Denn wenn “jedermann” Geld machen kann, so wird es wichtig zu wissen,
*welches Geld* man in *welcher Menge* besitzt und *welche Akzeptanten*
für dieses Geld leisten.

Bisher *dürfen* nur aber [Zentralbanken](/zentralbank "wikilink") Geld
machen, oder besser gesagt machen nur die Geld.

Weblinks
--------

-   [Wie funktioniert Geld?](http://www.youtube.com/watch?v=9BrLrwbkQWQ)
    [Teil 2](http://youtube.com/watch?v=aK2yZlHk4cA) & [Teil
    3](http://youtube.com/watch?v=0VOtdQrCoyk)
-   [unausweichliche Folge: Jeder sollte sein eigenes finanzielles
    Schicksal selbst in die Hand
    nehmen.](http://www.heise.de/tp/r4/artikel/29/29372/1.html%7CDie)

<Kategorie:Geld> <Kategorie:Theorie>
