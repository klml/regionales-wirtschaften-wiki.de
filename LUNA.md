# LUNA

**Luna** soll ein [dezentrales elektronisches
Marktsystem](/dezentrale_finanzsysteme "wikilink") werden. (nach [v0.1,
Norbert
Rost](http://wiki.regionales-wirtschaften.de/index.php?title=LUNA&oldid=1522)).
Dies wird hier [projektiert](/projekt_luna "wikilink")

Die Idee eines dezentralen elektronischen Marktsystems existiert schon
länger. Sie wird gespeist durch eine Kritik am heutigen Geldsystem sowie
durch technologische Entwicklungen, die es möglich erscheinen lassen,
solch ein dezentrales System erstehen zu lassen. Ein Beispiel für diese
Entwicklungen sind die Tauschbörsen im Internet, mit deren Hilfe es
möglich ist, Informationen (Musik, Filme, Dokumente, Software)
auszutauschen und die viele dezentrale Elemente beinhalten. Die Frage
ist, wie ein System aufgebaut sein soll, um wirtschaftliche
Transaktionen abzubilden.

Kriterien für ein Transaktionssystem für wirtschaftliche Vorgänge:

-   jedem Menschen und jeder Gruppe von Menschen ist es möglich, eigene
    Zahlungsmittel in ausreichender Menge zu generieren
-   Transaktionen von Leistungen sollen diese Akteure untereinander mit
    Hilfe dieses Zahlungsmittels abwickeln können
-   das System sollte so gestaltet sein, dass es nicht möglich ist, es
    durch den Ausfall einzelner Subsystme (z.B. Server) komplett
    abzuschalten

Philosophische & strukturelle Grundsätze
----------------------------------------

Logische und philosophische Grundsätze auf welchen diese System aufbaut:

1. [Geld](/geld "wikilink") ist Information
:   Diese Tatsache ist bereits daran erkennbar, dass das heutige Geld
    zum Grossteil in Computern verwaltet wird. Jeder Besitzer eines
    Kontos bei einer Bank wird dies bestätigen. Wenn man “Geld” im
    Internet abbilden will ergibt sich daraus die Frage: Wie muss diese
    Information “Geld” strukturiert sein, um im Internet verwendet zu
    werden? Schliesslich kann in Computern prinzipiell jeder beliebig
    Bits und Bytes manipulieren...

2. Die Verschmelzung von “Markt” und “Geld” im Internet steht bevor.
:   Ausgehend von der Idee, dass Geld “nur” Information ist und das
    Internet **das** Informationsmedium der Zukunft darstellt sowie in
    Kombination mit dem Wissen, dass auch Angebot und Nachfrage aus
    einem informationsverarbeitenden Blickwinkel erstmal “nur”
    Informationen *über* Angebote und Nachfragen sind, ist es
    vorstellbar, dass beide (Markt & Geld) in einem
    informationsverarbeitenden System vereinbar sind.

### Information & Kommunikation

Information ist mit *Kommunikation* eng verbunden. Informationen, welche
nicht kommuniziert werden, sind nutzlos. Oder anders: Nur durch
Kommunikation wird Information nutzbar. Kommunikation andererseits setzt
ein *Kommunikationsprotokoll* voraus. Im Internet ist beispielsweise das
hypertext-transfer-protocol (http) die Grundlage für das WWW, in der
“realen Welt” ist es der Sprachschatz, der Kommunikation möglich macht.
Mit diesen Überlegungen soll deshalb eine Grundlage für eine
Kommunikation über wirtschaftliche Vorgänge erschaffen werden: Ein
Protokoll für den Austausch wirtschaftlicher Informationen.

Als erstes steht die Frage nach der Strukturierung der Information
“Geld”. Gleichzeitig wäre es hilfreich, wenn diese Struktur so gebaut
wird, dass sie als Kommunikationsprotokoll für wirtschaftliche Vorgänge
dienen kann. Erst im nächsten Schritt sollte dann an eine
programmiertechnische Umsetzung gedacht werden, zumal gilt: Wenn die
Datenstruktur offen genug gestaltet ist, so kann *jedermann* in *jeder*
beliebigen Programmiersprache eine Bedienungsoberfläche bauen. Die
Datenstruktur soll deshalb in **XML** realisiert werden.

### dezentrale Netzwerke

Man kann die [Wirtschaft als Netzwerk](/wirtschaftsnetzwerk "wikilink")
betrachten. Jedes Netzwerk besteht aus *Knoten* und *Verbindungen*, in
der Wirtschaft sind die Knoten die
[Wirtschaftsakteure](/akteur "wikilink") und die Verbindungen zwischen
ihnen sind ihre [Geschäftsbeziehungen](/beziehungen "wikilink"). Entlang
dieser Geschäftsbeziehungen fliessen die *Leistungen* der
Wirtschaftsakteure und entgegengesetzt dieser Leistungen fliesst *Geld*.
Wirtschaft ist also ein Netzwerk, dessen Struktur durch den *Fluss von
Leistungen und Geld* ausgeformt wird. Dabei trennt die Währung, die die
Akteure benutzen, die jeweiligen Netzwerke voneinander: Wer Euro
akzeptiert nimmt am Wirtschaftsleben im Euro-Netzwerk teil, wer Dollar
akzeptiert am Dollar-Netzwerk und wer die Regionalwährung
[Chiemgauer](http://www.chiemgauer.info) akzeptiert nimmt am
Chiemgauer-Netzwerk teil. Gehen wir davon aus, dass jeder Akteur
beliebig viele Währungen akzeptieren kann, so kann er Teil mehrerer
Netze werden. Gehen wir weiterhin davon aus, dass es jedem Akteur
möglich wäre, seine eigene (elektronische) Währung zu *schaffen*, so
wären beliebig viele Netze möglich. In Kombination mit der Dezentralität
würde solch ein System jeder Gruppe von Individuen auf diesem Planeten
(und gern darüber hinaus ;-) ) erlauben, ein eigenes Wirtschaftsnetzwerk
zu etablieren, in dem miteinander getauscht und gehandelt werden kann.

Interessanterweise ist die “reale Wirtschaftswelt” also ebenso als
Netzwerk aufgebaut wie die Welt, in der ein Abbild dieser
Wirtschaftswelt als Werkzeug erzeugt werden soll: Das Internet. Was
liegt also näher, als die Struktur des Internets gleich als Struktur des
(noch aufzubauenden) Wirtschaftsnetzes zu nutzen?

### Information: Angebot, Nachfrage, Verrechnungseinheit

Jede Software, die ein Wirtschaftssystem abbilden will, würde es den
einzelnen Wirtschaftsakteuren erlauben, ein Profil einzustellen, welches
das Angebot des jeweiligen Unternehmens beinhaltet (vereinfacht wird
hier davon ausgegangen, dass jeder in der Wirtschaft agierende Mensch
ein Unternehmer ist; ein Unternehmer der Leistungen *pro*duziert und
Leistungen kon*sum*iert - für dieses Phänomen etabliert sich der Begriff
“Prosument”). Solche Profile kennen wir bereits aus entstehenden
Web2.0-Angeboten: MySpace bietet Profile von Musikern, Facebook und
StudiVZ von Studenten und Schülern, XING von Geschäftsleuten - und jedes
Profil stellt einen Einblick in die Person dar, von der es erbracht
wird. Unternehmen bilden heute ihr Profil meist auf ihrer eigenen
Webseite ab - die Webseite selbst ist das Profil. Jedoch: Diese
Informationen sind unstrukturiert und lassen sich selten automatisiert
auswerten. Dabei ist es doch höchst interessant sowohl für das
Unternehmen als auch für seine Kunden oder Geschäftspartner zu erfahren:

-   was bietet das Unternehmen? (Angebot)
-   was sucht es? (Nachfrage)

“Markt” funktioniert nur, wenn auf ihm Angebot und Nachfrage
zusammentreffen. Dieses Zusammentreffen setzt allerdings eben die
*Information über* Angebot und Nachfrage voraus. Transaktionen auf einem
Markt werden darüber hinaus nur getätigt, wenn Anbieter und Nachfrager
sich über Leistung und Gegenleistung einigen können - in einer
arbeitsteilig organisierten Ökonomie tritt ein Zahlungsmittel/eine
Verrechnungseinheit als Hilfsmittel in den Austauschprozess.

Das Internet ist der ideale “Raum”, um Angebot, Nachfrage und
Zahlungsmittel in Form von Informationen zu einem SYSTEM verschmelzen zu
lassen.

Dabei entstehen Fragen:

-   Wie wird das Zahlungsmittel erschaffen?
-   Wie wird garantiert, dass niemand “Geld fälscht” und sich damit
    Leistung auf Kosten anderer “erschleicht”?
-   Wie finden die Transaktionen statt?

XML
---

Der Gedanke hinter dem hier skizzierten und mit dem Namen LUNA
bezeichneten System ist nun: Jeder Nutzer dieses Systems legt **eine
XML-Datei** auf seinen Webserver, in welcher alle Informationen
gespeichert werden. XML ist ein offener Standard, der es erlaubt,
Informationen maschinenlesbar zu strukturieren - und zwar
systemübergreifend. Es sollte also egal sein, ob ein LUNA-Nutzer mit
Windows, Linux oder dem MacOS arbeitet; zugleich sollte das System offen
für Erweiterungen sein (wie beispielsweise die Ausbreitung auf
Plattformen wie Mobiltelefone).

Die Idee, eine offen lesbare XML-Datei auf dem eigenen Webserver liegen
zu haben, macht es einfach, Informationen des Nutzers abzurufen: Jeder
Nutzer dieses Systems kommuniziert mit ihrer Hilfe

-   was er bietet
-   was er sucht
-   und welche Zahlungsmittel er akzeptiert (=in welchen Geldnetzwerken
    er aktiv ist).

siehe [LUNA-XML](/luna-xml "wikilink")

### Transaktionssicherheit

Da solch eine XML-Datei von seinem Besitzer beliebig manipulierbar ist,
kommt ein System zum Einsatz, welches der doppelten Buchführung ähnelt:
**Jede Transaktion** wird immer **in beiden XML-Dateien** verzeichnet.
Da eine Transaktion (wie beispielsweise ein Kauf/Verkauf) immer zwei
Akteure betrifft, wird sie auch in beiden XML-Dateien abgelegt, um
gültig zu sein. Einseitige Manipulationen sind somit nicht möglich: Ein
Dritter kann deshalb immer die Gültigkeit der Transaktionen
nachvollziehen, indem er die XML-Dateien beider beteiligten Akteure auf
Konsistenz überprüft. Wird bei einer solchen Prüfung deutlich, dass
Akteur A angibt Geld von Akteur B erhalten zu haben, Akteur B aber diese
Gegen-Buchung nicht verzeichnet hat, so ist die Buchung als
unvollständig anzusehen und der Kontostand von Akteur A ist korrupt.

Dieses Vorgehen setzt voraus, dass ein unrechtmässig handelnder A nicht
Zugriff auf **beide XML-Dateien** hat - also auch auf die LUNA-Datei des
B. Normalerweise ist dies gegeben, wenn wir davon ausgehen, dass
Webserver den schreibenden Zugriff nur für seinen Besitzer erlaubt. Für
Situationen, in denen beide XML-Dateien gehackt werden, müssen eventuell
weitergehende Szenarien und Sicherheitsmechanismen erdacht werden. Als
Möglichkeit der Überprüfung bleibt jedoch immer folgendes Vorgehen:
Jeder Akteur innerhalb des Systems hat Geld durch Transaktionen
bekommen. Wenn jede Transaktion verzeichnet ist, so ergibt sich eine
*Kette von Transaktionen*. Folgt man (bzw. die überprüfende Software)
dieser Kette von einem Akteur zum nächsten, so muss *jede* Transaktion
konsistent sein. Ist dies nicht der Fall, so ist ein Betrug aufgedeckt
und der prüfende Akteur sollte seinerseits prüfen, ob er mit diesen
Leuten Geschäfte machen will.

### Geldschöpfung

Ein noch fraglicher Knackpunkt bei diesen Überlegungen ist, wie der
Vorgang der [Geldschöpfung](/geld#geldschöpfung "wikilink") vonstatten
gehen soll. Letztlich lässt sich jede Transaktionskette ja genau bis zu
dem Punkt “nachgehen”, wo das Geld in die Welt kam. Kommt Geld per
Leistungserbringung in die Welt (wie im Beispiel mit dem Tischler, siehe
[v0.1, Norbert
Rost](http://wiki.regionales-wirtschaften.de/index.php?title=LUNA&oldid=1522)),
so sollte dieser Vorgang ebenfalls in zwei XML-Dateien auftauchen:

-   in der Datei des Leistungsempfangenen = Schuldner
-   in der Datei des Leistungsgebenden = Gläubiger

In einem solchen System gäbe es neben der *Quantität* von Geld auch eine
*Qualität*, also eine Unterscheidung unterschiedlicher Gelder. So wie
wir heute Euro, Dollar und tschechische Kronen nutzen (bzw. so wie in
der Regiogeld-Szene bereits [Chiemgauer](/chiemgauer "wikilink"),
[Berliner](/berliner "wikilink") und
[Urstromtaler](/urstromtaler "wikilink") unterwegs sind), so werden auch
in solch einem System eine Vielzahl von Geldern und damit eine Vielzahl
von Geld-Netzwerken existieren.

Bis zu den bisherigen Überlegungen dieses elektronischen Marktsystems
ist vor allem der Gedanke wichtig, dass die Qualität eines Geldes sich
aus den miteinander handelnden Partnern bei seiner Entstehung ergibt. Um
beim Beispiel des Tischlers zu bleiben: Die Eltern dieses durch
Leistungserbringung geschaffenen Geldes sind sozusagen der
Leistungsempfänger und der Tischler als Leistungserbringer. Das durch
diesen Vorgang in die Welt gebrachte Geld unterscheidet sich also
dadurch von allen anderen Geldern dieser Welt, dass es eben **durch
diese beiden** in die Welt gebracht wurde. Jedes andere Geld, welches
durch ähnliche Vorgänge **anderer Menschen** geboren wird, ist also auf
diese anderen Menschen zurückführbar und hat damit “deren Qualität”. (Es
ist leichter diesen Denkprozess nachzuvollziehen, wenn man sich
tatsächlich vorstellt, jeder Mensch könne Geld machen wenn dies zum
Wirtschaften nötig ist und die Wirtschaftswelt würde demzufolge als
vielen Geldern und entsprechend vielen Geld-Netzwerken bestehen.) Die
Qualität des Zahlungsmittels muss also bei seinem Geburtsprozess
vermerkt werden. Und ebenso muss bei jeder Transaktion vermerkt werden,
*mit welchem Geld* die Wirtschaftsakteure den Leistungsaustausch
vorgenommen haben.

Das System soll es also jedermann erlauben, Geld zu machen. Es muss aber
verhindern, dass jemand solches Geld machen kann, zu dem er nicht befugt
ist. Oder anders: Wird Geld durch einen in diesem System “ganz normalen”
Geldschöpfungsprozess gemacht, so muss dieses Geld die Qualität des
Schöpfenden tragen. Es darf aber kein *spezifisches* Geld von Akteuren
gemacht werden, die nicht für dieses spezifische Geld zuständig sind.
Konkret: Jenes Geld, welches durch die Vorgänge zwischen dem
Leistungserbringer A und Leistungsakzeptanten B geschöpft wurde, dürfen
andere nicht nachmachen dürfen. Nur A und B dürfen Geld von der Qualität
“A-B” machen. Hierbei ist noch unklar, wie genau dies technisch
realisiert werden kann, zumal ein weiteres Problem hinzukommt: Das
Problem der Ausweitung der Geldmenge. Denn: Wirtschaftssysteme jeder Art
benötigen eine gewisse Grösse. Um beispielsweise ganze Regionen zu
versorgen, wie dies die Regionalgelder durchaus anstreben, müssen
teilweise hunderttausende Akteure daran teilnehmen können. Wenn nun aber
jedes Geld nur durch solche Paar-Beziehungen wie sie zwischen A und B
herrschen, gemacht werden kann, so ist die nutzbare Geldmenge viel zu
klein! Auf diesem Wege würden sich keine solch grossen Geldsysteme
entwickeln, dass sie für ganze Regionen einsetzbar sind, vielmehr würde
eine absolute Zersplitterung verhindern, dass grössere
Wertschöpfungsketten entstehen. Ein gutes elektronisches Marktsystem
würde jedoch sowohl winzige Marktsysteme (Nachbarschaftshilfe,
Tauschringe) also auch mittelgrosse (Regionalgelder) und sogar globale
Währungen erlauben und ermöglichen.

Software
--------

Die LUNA.xml-Dateien könnte man natürlich händisch schreiben, das lässt
sich aber auch per [Software](/luna_software "wikilink") tun.

Grund
-----

Welche Möglichkeiten stellt LUNA in Aussicht? Transparenz. Jeder Akteur
ist mit all seinen Transaktionen der Vergangenheit zu sehen. Und nicht
nur das: Seine Preise als auch sein Kontostand sind genauso einsehbar,
wie Informationen darüber, mit wem er welche Geschäfte macht und in
welchen Geld-Netzwerken er handelt. Diese Informationen sind jedoch für
faire Wirtschaftsakteure kein Hindernis, sondern, im Gegenteil, ein
Marketingwerkzeug. Denn alle Transaktionen innerhalb dieses (!) Systems
(womit gesagt sein soll: Die bisher gewohnten Markt- und Währungssysteme
stehen natürlich weiterhin zur Verfügung) lassen natürlich auf die
Vertrauenswürdigkeit des Akteurs schliessen. Solche Wirtschaftsakteure,
die nur zu ihrem eigenen Vorteil handeln oder gar unredlich agieren,
sind erkennbar und dürften sich keine gute Reputation erarbeiten.
Entsprechend ist es sinnvoll, das System um eine Bewertungsfunktion zu
erweitern: Wenn jede Transaktion von den Transaktionspartnern bewertet
wird, wird (man kennt das von eBay) Vertrauen aufgebaut.

Diese absolute Offenheit muss jedoch nicht bestehen bleiben. Es geht bei
diesem System um die Daten-Struktur. Mit welchen Daten diese Struktur
gefüllt wird, liegt im Ermessen der Software-Programmierer. Das heisst:
Die konkreten Inhalte des Systems könnten natürlich verschlüsselt
werden. Nach aussen wäre dann nicht mehr sichtbar, was genau die Akteure
untereinander gehandelt haben. Es könnten einzelne Daten oder alle Daten
verschlüsselt werden, ja es könnte auch eine Option geben, mit der
bestimmte Daten bestimmten Akteuren zugänglich sind und anderen nicht.
Erfahrungen in diesem Sektor hat sicherlich die OpenSource-Szene rund um
die Verschlüsselungssoftware PGP, deren asymmetrische Funktionalität
auch in solch einem Marktssystem gut aufgehoben wäre. Ebenfalls von PGP
kennt man “Signaturen”, die Möglichkeit einzelner Akteure, Nachrichten
zu unterschreiben. Solch ein Authentifizierungssystem wäre hilfreich in
einem Marktsystem, da Transaktionen oder Geldschöpfungsprozesse von den
beteiligten Akteuren unterzeichnet werden könnten und die Sicherheit
erhöhen würden.

Allgemein bietet der Aufbau auf Basis von XML eine gute Erweiterbarkeit.
Was bislang als Funktion nicht absehbar ist, könnte nachgerüstet und
erweitert werden. So lange unterschiedliche Versionen von der
entstehenden Software bearbeitet werden können bzw. so lange es für jede
Version eine nutzbare Software gibt, sind hier keine Grenzen gesetzt.
Was die Software betrifft ist gut vorstellbar, dass Plugins für
Wordpress ebenso existieren könnten wie separate Software. Wichtig - und
in diesem Dokument bislang noch nicht beschrieben - ist, wie die
Software miteinander kommuniziert. Denn ein Transaktionsvorgang muss wie
gesagt *in beiden XML-Dateien* der beiden Beteiligten auftauchen. Da
nicht beide [Akteure](/akteur "wikilink") zum selben Zeitpunkt an ihren
Rechnern sitzen wollen, muss die Software einen asynchrone Eintrag in
das XML-File erlauben sowie in der Lage sein, von extern übergebene
Daten in das eigene XML-File einzuarbeiten.

Weblinks
--------

-   [Geldschöpfung bei sogenannten “leistungsgedeckten”
    Regionalwährungen](http://www.regionales-wirtschaften.de/14.36.0.0.1.0.phtml)
-   [Moderne Verrechnungssysteme als Sicherheitseinrichtung für die
    globale Kredit- und
    Finanzkrise](http://www.regionales-wirtschaften.de/14.50.0.0.1.0.phtml)

<Kategorie:LUNA-XML>
