# Internet

Zugang zu Information ist nicht nur philosophisch aufklärerisch wichtig,
nein auch ganz technisch trivial, konkret **breitbandiges Internet am
Land**. Dazu kann man auf die Telekom warten der
[fordern](http://www.heise.de/newsticker/Niedersaechsische-Gemeinden-fordern-Breitband-Internet-im-ganzen-Land--/meldung/121046)
oder das ganze eben
[selber](http://www.heise.de/netze/Niedersaechsische-Gemeinde-baut-eigenes-Glasfaser-Netz--/news/meldung/119285)
[baggern](http://www.spiegel.de/spiegel/0,1518,616387,00.html).

[Schnelle Internetanschlüsse können helfen, entscheidend ist jedoch
etwas
anderes.](http://www.zeit.de/2015/50/demografie-landleben-infrastruktur-internet-anschluss-doerfer/komplettansicht)

In Mitteleuropa sind die Abstände zu einem breitbandigen Anschlusspunkt
relativ kurz und auf längeren Strecken liegen dann oft potentielle
Mitnutzer auf dem Weg.

Und man kommt auch einfach an den Punkt ran: “Leefmann und sein Team
fanden aber heraus, dass eine vergleichsweise nahe gelegene
Hochspannungsleitung auch ein Glasfaserkabel trägt, \[sie\] erlaubte es
Leefmann, sich sozusagen in die Leitung einzuklinken.” [Internet, wie es
sein
sollte](http://www.zeit.de/digital/internet/2015-08/cccamp-internet-gigabit)

Bauformen
---------

Gegen eine Anschluss sprechen ja die unsagbar hohen Baukosten mit [500
bis 2000
Euro](http://www.airnet-dsl.de/2011/01/31/kosten-und-wirtschaftlichkeit-dsl-ausbau-in-l%C3%A4ndlichen-regionen/)
je Anschluss. Das sind aber Preise klassicher Anbieter. gemdinen und
Initativen können mit innovativen Ansätzen Breitband sicherlich
einfacher bekommen.

Neben den bekannten Alternativen über Funk (Long-Term-Evolution (LTE),
Satellit) gibt es auch andere interessante Bauformen:

-   [Verlegepflug](https://de.wikipedia.org/wiki/verlegepflug "wikilink")
-   z.B. über das [Abwasser, Trinkwassernetz, kleine Gräben
    (Trenching)](http://www.teltarif.de/alternative-breitband-verlegetechniken/news/57443.html?page=1)
-   [Abwasserkanäle](http://sasbachwalden.blogspot.de/2010/01/stimmt-es-dass-das-glasfaserkabel-uber.html)
    (*Das insgesamt rund 42 km lange Netz führt Glasfaserleitungen über
    konventionelle Gräben, Abwasserkanäle und Freiluftleitungen.*)

Frei
----

[Freifunk](https://de.wikipedia.org/wiki/freifunk "wikilink"), den [Wlan hilft gegen
Vandalismus](http://www.sueddeutsche.de/wirtschaft/busunternehmen-wlan-hilft-gegen-vandalismus-1.2795941)

Weblinks
--------

-   [Bürgernetzvereine wollen flächendeckend 100 Mbit/s mit
    Glasfaser](http://www.heise.de/newsticker/Buergernetzvereine-wollen-flaechendeckend-100-Mbit-s-mit-Glasfaser--/meldung/122324)
-   [geteilt.de](http://www.geteilt.de) Initiative gegen digitale
    Spaltung -geteilt.de- e.V

<Kategorie:Regional> <Kategorie:Netz>
