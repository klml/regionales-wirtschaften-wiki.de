# Eigentum

ist die Verfügungsgewalt über eine Sache auf rechtlicher Grundlage.
Dabei kann es sich wie beim geistigen Eigentum auch um immaterielle
vermögenswerte Sachen handeln. Eine Kuh sollte dem gehören der sie
aufgezogen hat.

geistiges Eigentum
------------------

ist ein nicht im Naturrecht wurzelnder Begriff, der Ausschluss-Rechte an
immateriellen Kulturgütern beschreibt. Immaterialgüter sind z. B. Ideen,
Erfindungen, Konzepte, geistige Werke, Informationen.

Geistiges bedarf zwar auch einer menschlichen Anstrengung, allerdings
ist diese wesentlich mehr abhängig vom Input anderer (Bildung, andere
Ideen etc). Ein alleiniger Zuspruch an eine (juristische) Person ist
daher realistisch nicht möglich, sondern nur durch eine einseitige
Zuwendung.

Von der kollektivistischen Seite hat aber Information im Gegensatz zu
Materie den Vorteil, dass sie kopiert werden kann, also mehrmals
existiert. Eine Verknappung an kann nur künstlich erfolgen und ist für
den Sozialerfolg äusserst hinderlich. Die geringe Motivation einzelner
die durch ein Aussprechen solcher geistiger Monopole erfolgt bringt
unter dem Strich wesentlich weniger.

Wen man eine Kuh halbiert hat man 2 halbe Kühe. Teilt man Information
hat man die ursprüngliche immer noch, aber die Chance auf schnelle
Innovation.

<Kategorie:Theorie>
