# Wirtschaftsnetzwerk

Die [Beziehungen](/beziehungen "wikilink") zwischen
[Wirtschaftsakteuren](/akteur "wikilink") geben dem Wirtschaftssystem
eine netzwerkartige Struktur. Jeder Akteur ist Teil dieses Netzwerkes,
sobald er auf irgendeine Art mit einem anderen Akteur, der Teil des
Netzwerkes ist, auf wirtschaftlicher Ebene in Beziehung steht. Geld- und
Leistungsflüsse formen das Netzwerk.

<Kategorie:Theorie>
