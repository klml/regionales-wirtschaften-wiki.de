# LUNA-XML-Struktur-Beispiel

    <?xml version="1.0" encoding="ISO-8859-1"?>
    <marktsystem>
     <system>
      <id>http://www.regionales-wirtschaften.de/luna.xml</id>
       <version>
        <typ>luna</typ>
        <nummer>0.1</nummer>
       </version>
      </system>
      <akteur>
       <name>Norbert Rost</name>
       <anschrift>Dresden</anschrift>
       <email>norbert.rost@regionales-wirtschaften.de</email>
       <homepage>http://www.regionales-wirtschaften.de</homepage>
      </akteur>
      <suche>
       <item>Programmierer fuer Umsetzung eines dezentralen elektronischen Marktsystems auf OpenSource-Basis</item>
      </suche>
      <biete>
       <item>Regionalentwicklung</item>
       <item>2 PCs (486er) ohne Monitor</item>
       <item>Kirschen</item>
      </biete>
      <netze>
       <netz>
       <menge>500</menge>
       <qualitaet>http://www.regionales-wirtschaften.de/luna.xml|http://www.irgendwo.de/luna.xml</qualitaet>
       <transaktionen>
        <trans>
         <id>E9EABDE8D29494A56A1B3A57208F9CCC2</id>
         <datum>2007-06-01</datum>
         <akteur>http://www.irgendwo.de/markt.xml</akteur>
         <menge>
          <vorher>600</vorher>
          <trans>-100</trans>
          <nachher>500</nachher>
         </menge>
         <leistung>Beschreibung der Leistung, die getauscht wurde</leistung>
         <bewertung>
          <note>1</note>
          <beschreibung>hohe Qualitaet der gelieferten Leistung, super freundliche Transaktion!</beschreibung>
         </bewertung>
         <status>schwebend</status>
         <signatur>eventl. eine Signatur, die nur der Transaktionspartner generieren kann</signatur>
        </trans>
        <trans>
         <id>E90AC180C03BF45A49B4E0535CDF1FF65</id>
         <datum>2007-06-01</datum>
         <akteur>http://www.irgendwoanders.de/luna.xml</akteur>
         <menge>
          <vorher>700</vorher>
          <trans>-100</trans>
          <nachher>600</nachher>
         </menge>
         <leistung>Ein Buecherregal</leistung>
         <bewertung>
          <note>1</note>
          <beschreibung>hohe Qualitaet der gelieferten Leistung, super freundliche Transaktion!</beschreibung>
         </bewertung>
         <status>vollendet</status>
         <signatur>eventl. eine Signatur, die nur der Transaktionspartner generieren kann</signatur>
        </trans>
       </transaktionen>
       <schoepfung>
       ---- siehe Beschreibung: Noch genau zu klaeren ----
       </schoepfung>
      </netz>
     </netze>
    </marktsystem>
