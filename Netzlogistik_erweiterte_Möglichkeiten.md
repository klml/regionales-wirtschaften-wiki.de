# Netzlogistik erweiterte Möglichkeiten

Aber [Netzlogistik](/netzlogistik "wikilink") soll nicht nur existierende
logitischen Herausforderungen verbessern sondern hat auch erweiterte
Möglichkeiten.

Neue Transportarten
-------------------

Die Transportart soll dem Transporteur überlassen bleiben, das bestimmt
jeweils praktikabelste anwendbare Transportportmittel. Oder bietet seine
bestimmtes Transportmittel so günstig an dass jeweils das Idealste
eingesetzt wird. Ob ein [Fahrradkurier](https://de.wikipedia.org/wiki/fahrradkurier "wikilink") oder
bei schlechtem Wetter doch ein Taxi ein Päckchen in die Fußgängerzone
bringt, kann so den Gegeneheiten angepasst werden, weil der
Fahrradkurier vermutlich schneller durch überfüllte Straßen und das Taxi
an Regentagen wahrscheinlich trockener ankommt.

-   ![Bus mit Briefkasten: aussen
    ...](/postbus-grafenau1.jpg "fig:bus mit briefkasten: aussen ...")![...
    oder
    innen](/bremen_gt8n-1_tram._post_box_and_ticket_machine_-_flickr_-_sludgegulper.jpg "fig:... oder innen")
    Ein ÖPNV Bus, mit festen Touren, hat ein Fach für Päckchen. Ein
    'Viertelbote' bringt und holt dabei die Päckchen zu und von einem
    Bus oder Wohnungen, Büros oder Geschäften. Natürlich müssen sich
    dabei Busfahrer und 'Viertelbote' gut und persönlich kennen um nicht
    bei jeder Haltestelle eine komplette Personenverifizierung machen zu
    müssen. Aber wenn der 'Viertelbote' immer als erster in den Bus
    einsteigen darf, er könnte ja als solcher gekennzeichnet sein, hat
    er Zeit, bis die regulären Fahrgäste eingestiegen sind, seine Pakete
    auszuwechseln.

Anknüpfungspunkte
-----------------

Packstation

:   ![Packstation](/packstation_winter.jpg "fig:packstation")Eigentlich
    sind die [Packstationen](https://de.wikipedia.org/wiki/packstation "wikilink") eine tolle
    Erfindung, denn im Prinzip sind es große paketfähige Hauspostkästen
    die variable unter mehreren Nutzen zugewiesen werden, so kann jeder
    ein Paket zeitunabhängig annehmen, ohne einen eigene
    “Paketbriefkasten” zu haben. Leider sind die Packstationen der
    Deutschen Post AG ein monopolistisches Angebot, denn es können dort
    nur Pakete der DHL abgeholt werden, obwohl die Kästen im
    öffentlichen Raum stehen. Sinnvoll wäre es, der öffentliche
    Infrastruktur-Träger, würde die Paketstationen betreiben und jeden
    Lieferanten die Möglichkeit geben eine Slot für sich und seinen
    Endkunden zu buchen.

Autos

:   [Online bestellte Ware soll schnell und zuverlässig in den privaten
    PKW geliefert werden
    können.](http://www.sueddeutsche.de/auto/auto-als-paketstation-wenn-der-postmann-zweimal-hupt-1.1894146)

Inhalt
------

Aber nicht nur Socken und CD-Player, der klassische Inhalt des UPS
Pakete, lassen sich netzlogistisch versenden. Fast jede Branche ist
irgendwo in der gesellschaftlichen Logistikkette intergriert oder darauf
angewiesen.

Briefe und Pakete

:   sind klassischen Inhalte

Lebensmittel

:   nicht erste seit Amazon darüber nachdenkt Lebensmittel zu versenden
    wird Nahrung geliefert. Es gibt schon lange klassische
    Tiefkühllieferdienste, Getränkelieferanten und die
    [Ökokisten](https://de.wikipedia.org/wiki/ökokiste "wikilink") liefern schon immer direkt.

Schweres, Großes und Unhandliches

:   nicht alle Pakete sind mittellleicht und bequem zu tragen. Wenn
    schwere, große oder unhandliche Sendungen geliefert werden, kann
    sich ein spezielisierter Lieferant um einen solchen Auftrag bemühen.
:   Soll beispielsweise ein Schrankwand von einem Möbelversender
    gelifert werden, so ist ein klassische KEP Lieferfahrer mit Kombi
    und Klappsackkarren das falsche Mittel. Ein Getränkelaster mit
    Hubbühne, Handhubwagen und 2. Fahrer kann, auf einer regulären Tour,
    die Schrankwand einfacher liefern.

Umzugsware

:   Bei Wohnungsumzügen werden die Güter 'first out - last in'
    verfrachtet. Erst müssen “Möbelinhalte” wie Kleidung,
    Haushaltswaren, Bücher verpackt werden um dann die eigentlichen
    Möbel abzubauen. Beides wird, meist zusammen, transportiert und dann
    umgekehrt wieder aufgebaut und eingeräumt. Dabei werden kleinteilge
    Inhalte paketiert, um diese überhaupt transportieren zu können.
:   Würde man diese per [Lagern per Post](https://de.wikipedia.org/wiki/lagern_per_post "wikilink")
    vorab nicht mit dem Möbelwagen fahren sondern per KEP, und auf der
    Supply-Chain auch kurzfristig lagern, könnte man Inhalte nach dem
    Möbelaufbau nach Bedarf abrufen und hätte diese nicht während der
    Möbelmontage im Weg stehen.

Zeitlich besondere Lieferungen

:   die meisten Transporte haben eine Zielzeitrahmen, denn irgendwann
    will der Kunde seine Ware. Aber dennoch gibt es an Dauer und
    Qualität unterschiedliche Anforderungen. So sieht sich eine geplante
    Nachbestellung Zement für eine Baufirma mit einer anderen
    Zeitvorgaben konfrontiert als eine eilige Blutkonserve.

    Besonders eilig
    :   Eilige Arzneimittel oder eben der klassische Expressdienst

    Bestimmten Uhrzeit
    :   z.B. zubereitete Lebensmittel, morgens der
        [Milchlieferservice](https://de.wikipedia.org/wiki/milchlieferservice "wikilink") und Abends
        [Pizzaservice](https://de.wikipedia.org/wiki/pizzaservice "wikilink")

Reisegepäck

:   Reisegepäck wird meist mit dem Reisenden im gleichen Transportmittel
    verfrachtet um zeitgleich am Zielort zu sein. Muss daher auch im
    Reiseverkehrsmittel verladen und umgeladen werden. Kofferwuchten im
    Zügen und Warten an der Gepäckausgabe der Flughäfen sind die Folge.
    Würde man Reisegepäck aber per KEP an eine dem Zielort nahe Mailbox
    versenden, kann man diese dort nach eintreffen bequem anfordern.
    Schickt man das Hauptgepäck noch früher los (am Abend vor dem
    Abflug) kann das Gepäck kostengünstigere Transportmittel nutzen.
    z.B. bei inner-kontinentalen Flügen kann das Gepäck ohne Nachteile
    12 oder mehr Stunden reisen und muss nicht als schnell und teure
    Flugfracht, wie Personen eben schon, fliegen.

