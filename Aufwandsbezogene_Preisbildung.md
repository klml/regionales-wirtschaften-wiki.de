# Aufwandsbezogene Preisbildung

**Grundgedanke der aufwandsbezogenen Preisbildung**

Die Preisbildung aus Angebot und Nachfrage kann sich nachteilig
auswirken, da sie zu [Konkurrenzkampf](/konkurrenz "wikilink") auf dem
Markt führt, nicht aber zwangsläufig zur Verbesserung der Produkte und
Herstellungsverfahren. Die Folge davon sind Leistungsdruck und die
Neigung zu Korruption und Kriminalität, sobald der Leistungsdruck nicht
mehr ertragen werden kann. Wer überleben will der muss sich dass, was er
zum überleben braucht notfalls auch illegal erwerben, auch wenn das
nicht erstrebenswert ist. Eine Lösung für diese Probleme bietet die
Preisbildung anhand des gesamten Aufwands, der für die Herstellung eines
Produktes nötig ist. Die Voraussetzung hierfür ist, dass die
Herstellungsverfahren der Produkte für jedermann offengelegt werden und
auch für Händler und Kunden nachvollziehbar sind.
([Transparenz](/transparenz "wikilink"))

**Nähere Beschreibung**

Um ein Produkt herzustellen ist immer ein bestimmter Aufwand nötig.
Dieser Aufwand setzt sich unter anderem zusammen aus Arbeitszeit,
Material und Rücklagen für unvorhergesehene Risiken und Investitionen.
Der Aufwand ist abhängig von Herstellungsverfahren und unter anderem
davon, wie leicht die Rohstoffe zu beschaffen sind oder ob eine
umfangreichere Produktentwicklung im Vorfeld nötig war. In jedem Fall
ist der Aufwand nachvollziehbar, für den Produzenten wie auch für den
Kunden. Der neue Gedanke ist hier, dass der Kunde sich auch über den
Aufwand im klaren sein sollte, der hinter einem Produkt steht. Der Kunde
soll einen Einblick darin erhalten, wie die angebotenen Produkte
hergestellt werden. Ist dem Kunden sowie auch dem Händler der Aufwand
hinter einem Produkt bekannt, dann ergibt sich der Preis in seiner
Größenordnung automatisch, er wird nachvollziehbar. Preisverhandlungen
sind hier nur noch durch ein Entgegenkommen des Kunden an den
Produzenten sinnvoll, wie zum Beispiel durch eine Zeitlich befristete
garantierte Abnahme. Durch die Transparenz des Produktionsaufwandes
sinkt das [Preisintervall](/preisintervall "wikilink"). Ähnliche
Verfahren werden z.B. bei Automobilzulieferern angewandt - durch die
Offenlegung der Kalkulation soll die langfristige wirtschaftliche
Stabilität des Lieferanten gewährleistet werden.

**Wahrscheinliche Auswirkungen der aufwandsbezogenen Preisbildung**

Bei der Preisbildung unter Betrachtung des Aufwands werden sich die
Kunden nicht mehr vollkommen blind und ohne nachzudenken für das
billigste Angebot entscheiden. Die Kunden werden beginnen, ihre Produkte
mit offenen Augen und mit einer Neugier für deren Herstellungsverfahren
zu kaufen. Sie werden sich verstärkt für ihre Ware interessieren. Es ist
nicht schlecht, wenn Kunden sich für ein günstiges Angebot entscheiden,
wenn der günstige Preis darauf beruht, dass das Produkt mit einem sehr
effizienten Herstellungsverfahren erzeugt wurde. Kunden werden dann
durch ihre Sparsamkeit effiziente und gut durchdachte
Herstellungsverfahren unterstützen.

Durch eine Preisbildung nach Aufwand ist es auch nicht mehr nötig, dass
Unternehmen sich im Preiskampf gegenseitig in den Ruin treiben. Ruinöse
Preiskämpfe schaden der Wirtschaft. Die Mitarbeiter der Unternehmen sind
in ihrem Privatleben Kunden. Die Anbieter der Waren sind Arbeitgeber.

**Kritik**

Die Aufwandsbezogene Preisbildung wird von der [Österreichischen
Schule](/österreichischen_schule "wikilink") der Nationalökonomie
kritisch gesehen. Preise sind ein wesentliches Signal, um Sinn oder
Unsinn einer wirtschaftlichen Handlung zu erkennen. Ohne Preiswettbewerb
ist es nicht möglich, die Effizienz des eigenen Handelns zu
hinterfragen.

Bsp: Wenn jemand eine Betriebsanleitung händisch abschreibt, wird er für
diese Arbeit mehrere Stunden benötigen. Trotzdem ist diese Arbeit nicht
werthaltig, da sie billiger und besser von einem Kopiergerät erledigt
werden könnte. Falls ein Käufer eines Radios bereit ist, zu akzeptieren,
dass das Herstellen der Anleitung ein Aufwand von mehreren Stunden war
(und er daher eine entsprechende Gegenleistung bringen muss) dann wird
die [Fehlallokation](/fehlallokation "wikilink") “Schreiben von
Gebrauchsanweisungen mit der Hand” aufrecht bleiben. - Die
Gesamtökonomie wird ineffizient.

Die Beispiele aus der Automobilindustrie sind nicht allgemein anwendbar
- es besteht dort ein sehr starkes Bedürfnis des Leistungsempfängers,
dass sein Lieferant nicht insolvent wird.

Die praktische Erfahrung zeigt auch, dass der notwendige
Bewusstseinsgrad und die erforderliche Einsicht nur bei wenigen Personen
vorhanden ist. Aufwandsbezogene Preisbildung ist daher hauptsächlich in
informellen Kleingruppen anwendbar, in der die Gruppenmitglieder unter
starker gegenseitiger sozialer Kontrolle stehen.

<Kategorie:Konzepte>
