# Energie

Regionale Versorgung mit **Energie** soll weder zentral verwaltet noch
komplett autark sein, sondern
[dezentral](https://de.wikipedia.org/wiki/dezentrale_stromerzeugung "wikilink") und untereinander
[vernetzt](/netzbewusstsein "wikilink"). Alternative Energiequellen
abseits von fossilen zentralisierten Trägern, sind als Naturenergie
einigermaßen auf dem Vormarsch. Regionale Energie hat vor allem
Potential bei dieser fossilfreien Versorgung mit regenerativen Energien,
den [Peak Oil](/peak_oil "wikilink") kommt sicher.

Verteilung
----------

Ein weiteres Problem der Energieversorgung sind oligopole
Verteilungsorgansiationen, nicht nur Gazprom oder Texaco sondern auch
Eon und Ruhrgas sind viel zu mächtige Akteure. Um regional mit Energie
zu wirtschaften braucht man einen regionalen Energiemarkt. Das
derzeitige Oligopol der vier deutschen Energieversorger (in anderen
Ländern ist die Situation ähnlich) kann aber nicht die Kreativität der
Marktteilnehmer entfalten.

Den Märkte müssen erschaffen und gepflegt werden und müssen für alle
Teilnehmer transparent sein. Regional strukturierte
[Stadtwerke](/stadtwerke "wikilink") können diesen Markt erschaffen und
so zu einem sinnvolleren Energiewirtschaften beitragen.

Warum sollte eine Energie nicht selbst verwalten? [Nie hat es so viele
Gründungen unabhängiger Energieversorger gegeben wie 2008.... Besonders
beliebt ist dabei eine lange Zeit belächelte Organisationsform - die
Genossenschaft](http://www.handelsblatt.com/politik/deutschland/hochwuerden-gibt-gas;2117688)

Energiegenossenschaften, abseits der unfähigen ExStaatsversorger die nur
noch Banken sind, sogar [Kirchen steigen ins Energiegeschäft
ein](http://www.spiegel.de/wirtschaft/0,1518,601042,00.html)

Region
------

![Das Netz kostet ein Viertel des
Strompreises](/strompreiszusammensetzung_2014.png "fig:das netz kostet ein viertel des strompreises"){width="350"}
Auch wenn [Stromsee](https://de.wikipedia.org/wiki/stromsee-modell "wikilink") alles gleich macht,
er besteht aus teuren Trassen und
[Übertragungsverluste](https://de.wikipedia.org/wiki/übertragungsverlust "wikilink") von 1 % auf 100
km. Das wären bei den geplanten Nordsee-Bayern Trassen ca 8%.

So haben Regionen unterschiedliche Herausforderungen in der
Energieversorgung bei der Umstellung auf regenerativen Energien.
Niedersachsen und Mecklenburg Vorpommern hat viel Windstrom vor der
Haustüre und der Küste, ein Versorgung ist einfacher und kostengünstiger
möglich. Das Problem sind südliche Länder wie Bayern, Baden-Würtemberg
die Windstrom über lange Strecken importieren müssen.

Strom der nicht verbraucht wird, muss auch nicht transportiert werden.
Versuchen energieferne Gebiete wie Süddeutschland Peaks abzumildern, hat
das neben der einfacheren Herstellungskurve den zusätzlichen Vorteil das
der Strom auch nicht transportiert werden muss. Energiesparen und
Lastverschieben ist im Süden also doppelt so sinnvoll wie in Stralsund.

Versucht man nun energieintensive Industrie aus dem Süden in den Norden
zu verschieben. Entlastet man den teuren Südstrombedarf und kann
strukturschwachen Regionen eine Chance geben. das aber natürlich nur
wenn die Politik nicht für alle Regionen den Einheitstarif fordert
sondern regionale Probleme (wie das süddeutsche Transportproblem) auch
über den Preis von den Süddeutschen lösen lässt.

Außerdem fördert eine regionale Netzentlastung kommunale Investition.

Neben Energiesparen sollte man:

-   kann man Bedarfsspitzen durch regionale
    [Netzentlastung](/netzentlastung "wikilink") abfangen
-   muss man jeden erzeugten Strom auf 50Hz 230V Wechselstrom hoch
    spannen oder kann man auch eine geringere
    [Stromqualität](/stromqualität "wikilink") nutzen
-   müssen Smartmeter zentrale Datenschutzkatastrophen sein, oder kann
    man [Smartmeter](/smartmeter "wikilink") auch verbraucherfreundlich
    betreiben

Literatur
---------

-   Peter Becker: Aufstieg und Krise der deutschen Stromkonzerne ISBN
    978-3920328577

Weblinks
--------

-   [kommunal-erneuerbar.de](http://www.kommunal-erneuerbar.de) soll
    kommunale Entscheidungsträger sowie engagierte Bürgern unterstützen,
    den Ausbau Erneuerbarer Energien mit breiter Zustimmung vor Ort
    voranzutreiben.
