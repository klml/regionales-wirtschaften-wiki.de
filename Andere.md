# Andere

Es gibt viele Ansätze die sich mit alternative Wirtschaftsordnungen
beschäftigen.

Initiative für Natürliche Wirtschaftsordnung
--------------------------------------------

Die [Initiative für Natürliche
Wirtschaftsordnung](http://www.inwo.de/modules.php?op=modload&name=paged&file=index&page_id=3)"
(INWO e.V.) engagiert sich seit 1983 vor allem in der Öffentlichkeit für
die Ideen nach [Silvio Gesell](/silvio_gesell "wikilink")

