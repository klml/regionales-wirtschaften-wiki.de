# Staffelgenerator

Der **Staffelgenerator** ist eine Idee zu einem abgestuften
Elektrogenerator der ein nicht-konstantes Drehmoment auf unterschiedlich
viele Wicklungen abbildet. Stromgeneratoren wandeln Drehenergie in Strom
um. Dabei müssen Generatoren entweder werden entweder netzsynchron
betreiben, wobei diese erst bei der Nenngeschwindigkeit Leistung
einspeisen können und darüber hinaus gebremst werden müssen, oder
aufwändiger durch

Bei regelbaren Energieträgern wie Wasserkraft oder Dampf kommen
einfache, wartungsarme und vor allem hochwirksame zum Einsatz. Bei nicht
regelbaren Drehenergien wie zB bei einer Windkraftanlage müssen entweder
auf Nennleistung geregelt werden, wobei Pitchregelung aufwändig ist und
Stallregelung einen verhältnismäßig schlechterem Wirkungsgrad aufweist.
Oder aufwändigerer mit nachgeschaltetem Gleichrichter, Filter und wieder
Wechselrichter.

Wäre es nicht möglich eine unsteuerbare Energieform je nach auftreten an
einem physikalisch mehreren gestaffelten Generatoren abzufangen? Also je
nach Drehmoment bei gleich bleibender Drehzahl und damit Frequenz und
selber Spannung aber insgesamt schwankender Leistung.

Baulich vereint könnte man doch mehrere unabhängige Wicklungsstränge
optional zu schalten.

Abstufung
---------

Um nicht für jede Abstufung innerhalb der Einspeisetoleranz gar ein
eigene Wicklung bereit stellen muss, könnte man diese Staffel und das
sogar quadratisch. Also je ein Wicklung für 1 W, 2W, 4W, 8W. Bei 6 W
Nennleistung würden dann die 4W und die 2 W Wicklung zugeschalten, die
jeweils andern auf Leerlauf. so ähnlich wie die

Probleme
--------

Abschirmung

:   stören sich die einzelne baulich eng aneinander liegenden
    Wicklungspakte elektromagnetisch?

Leerlauf

:   Wenn also eine Wicklung nicht geschalten wird baut diese eine
    Spannung auf, aber kein Strom und daher auch kein Feld?

Gibt es sowas schon? Oder habe ich einen immensen Gedankenfehler?
[edit](http://wiki.regionales-wirtschaften.de/index.php?title=Staffelgenerator&action=edit)

