# Versorgungscluster

Ein **Versorgungscluster** ist ein Netzwerk aus regionalen Unternehmen,
deren Hauptaufgabe in der Grundversorgung der Bevölkerung einer Region
besteht:

-   Nahrungs-/Lebensmittel
-   Wohnraum
-   regionale Mobilität / Nahverkehr
-   Kommunikation ([Flächiges
    Breitband](/flächiges_breitband "wikilink"))
-   Bildung
-   Kultur
-   Soziales
-   Gesundheit
-   [Energie](/energie "wikilink")
-   kollektive Risikoübernahme [Versicherungen](/versicherung "wikilink")

Die meisten dieser Aufgaben sollte eine funktionierende Region aus sich
selbst heraus erfüllen können. Der Versorgungscluster jeder Region macht
sie von externen Schocks unabhängiger und ist zugleich die Basis für
[redundant](/redundanz "wikilink") ausgelegte, globale
Versorgungsstrukturen.
