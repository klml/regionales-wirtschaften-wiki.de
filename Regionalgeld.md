# Regionalgeld

**Regiogeld** (Regionalgeld, Regionalwährung) ist ein Zahlungsmittel,
welches zusätzlich zu den bestehenden nationalen und kontinentalen
Zahlungsmitteln eingesetzt wird. Sein Wirkungsbereich ist auf eine
*konkrete Region* begrenzt und es gilt die *Freiwilligkeit der Annahme*.

Theorie
-------

### Zielsetzung

-   regionale [Wirtschaftskreisläufe](/wirtschaftskreisläufe "wikilink")
    stimulieren
-   [Wertschöpfungsketten](/wertschöpfungsketten "wikilink") in die
    Region verlagern und/oder neu aufbauen

### Erwartete Nebenwirkungen

-   verbesserte [Zahlungsmoral](/zahlungsmoral "wikilink")
-   zunehmende [Vernetzung](/vernetzung "wikilink") der regionalen
    Wirtschaftsakteure
-   günstigere [Kreditkosten](/kreditkosten "wikilink")

Praxis
------

### Initiativen und Regiogelder

Über 50 Regiogeld-Initiativen gibt es allein im deutschsprachigen Raum,
die meisten davon haben sich im Regiogeld-Verband organisiert. Den
Aufbau eines Regiogeldes ist nicht trivial, die Entwicklung kann man in
verschiedene [Phasen](/aufbauphasen_eines_regiogeld-projekts "wikilink")
einteilen.

-   [Alto](/alto "wikilink")
-   [Ammerlechtaler](/ammerlechtaler "wikilink")
-   [Augusta](/augusta "wikilink")
-   [Berliner](/berliner "wikilink")
-   [Bremer Roland](/bremer_roland "wikilink")
-   [BrodEinheit](/brodeinheit "wikilink")
-   [BürgerBlüte](/bürgerblüte "wikilink")
-   [Chiemgauer](/chiemgauer "wikilink")
-   [DeMark](/demark "wikilink")
-   [DreyEcker](/dreyecker "wikilink")
-   [Elbtaler](/elbtaler "wikilink")
-   [Freitaler](/freitaler "wikilink")
-   [Hansemark](/hansemark "wikilink")
-   [Havelblüte](/havelblüte "wikilink")
-   [KannWas](/kannwas "wikilink")
-   [Kirschblüte](/kirschblüte "wikilink")
-   [Landmark](/landmark "wikilink")
-   [Lausitzer](/lausitzer "wikilink")
-   [Nahgold](/nahgold "wikilink")
-   [Oderblüte](/oderblüte "wikilink")
-   [Ortenauer](/ortenauer "wikilink")
-   [Pälzer](/pälzer "wikilink")
-   [Regio in München](/regio_in_münchen "wikilink")
-   [Regio im Oberland](/regio_im_oberland "wikilink")
-   [Regio im Ostallgäu](/regio_im_ostallgäu "wikilink")
-   [Rössle](/rössle "wikilink")
-   [Sterntaler](/sterntaler "wikilink")
-   [Styrrion](/styrrion "wikilink")
-   [Tauberfranken](/tauberfranken "wikilink")
-   [Urstromtaler](/urstromtaler "wikilink")
-   [Volmetaler](/volmetaler "wikilink")
-   [Waldviertler](/waldviertler "wikilink")
-   [Zschopautaler](/zschopautaler "wikilink")

### International

-   [ithacahours](/ithacahours "wikilink")
    [ithacahours.com](http://www.ithacahours.com/german.html)

### Historisch

-   Das war 1932 ein wirksame Abhilfe gegen die damalige
    Weltwirtschaftkrise, wurde jedoch jäh verboten. [Wörgl-ein
    historisches
    Beispiel](http://www.futuremoney.de/geld/komplement_text.html#woergl)

