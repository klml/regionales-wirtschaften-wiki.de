# Stromqualität

[Elektrischer Strom](//https://de.wikipedia.org/wiki/elektrischer_strom "wikilink") ist einer der
praktischsten [Energieformen](//energie "wikilink"), leider recht flüchtig
und nicht gut speicherbar.

Aber Strom ist nicht gleich Strom, was bei uns aus der Steckdose kommt
ist Qualitätsstrom mit hoher
[Versorgungsqualität](//https://de.wikipedia.org/wiki/versorgungsqualität "wikilink").
[Dreiphasenwechselstrom](//https://de.wikipedia.org/wiki/dreiphasenwechselstrom "wikilink") einer so
konstanten Netzfrequenz das man damit Uhren betreiben kann und Computer
und elektronische Bauteile sich nicht vertakten. Vor allem ist
Dreiphasenwechselstrom gegenüber von einphasigem Strom und vor allem
Gleichstrom viel besser transportierbar.

Bei der Herstellung aber bekommt man erst mal 'Schmuddelstrom', entweder
schwankenden Gleichstrom, weil es an der geerntete Wind nun mal etwas
böhig ist, eine Windrad nicht mit 50 Hz dreht oder Gleichstrom aus
halbverschatteten Solaranlagen. Konventionelle Erzeuger haben meist
stetigere Primärquellen wie Kohlefeuer oder Reaktorhitze und können
diese, da über Dampf übertragen sehr leicht regeln. Natürlich liefern
alles diese Erzeuger sauberen Strom im Netz ab. Aber sie haben einen
doch beträchtlichen Aufwand den Erntestrom aufzubereiten und eine hohe
[Spannungsqualität](//https://de.wikipedia.org/wiki/spannungsqualität "wikilink") und stabile
[Netzfrequenz](//https://de.wikipedia.org/wiki/netzfrequenz "wikilink") zu erhalten. Aufwand in
Anlagen wie [Wechselrichter](//https://de.wikipedia.org/wiki/wechselrichter "wikilink") bei
Photovoltaik oder Bremsen und Getriebe bei der Windkraft.

![miniatur|300px|[Bedarf an elektrischer
Energie](//https://de.wikipedia.org/wiki/bedarf_an_elektrischer_energie "wikilink"), nicht alles
braucht sauberen
Strom](//haushalt-energieverbrauch.svg "fig:miniatur|300px|bedarf an elektrischer energie, nicht alles braucht sauberen strom")
Allerdings brauchen wir nur sehr wenig Strom für Uhren, Messgeräte und
auch nicht alles für industrielle Motoren, Aufzüge oder Krane. Ein ganz
großer Teil unseres wahnsinnig sauberen Stroms geht in eine galvanische
thermisch Verwertung und dort ist es egal ob er überhaupt als
Wechselstrom oder in welcher Frequenz der Strom ankommt, Hauptsache er
hat Energie.

Kochplatten, Aluwerke und Warmwasser braucht gar keinen sauberen Strom.
Auch Beleuchtung, hier ist Dreh oder Gleichstrom wieder egal, kann man
so konstruieren das diese nicht gleich flackert. Auch wenn Motoren von
gewerbliche Kältemaschinen mit Wechselstrom wirtschaftlicher laufen,
unregelmäßig laufen ist die Kühlleistung dennoch die gleiche. Ist dann
nur die Frage ob ein ungeregelter Wechselrichter wesentlich günstiger
ist.

Netz
----

Das Problem ist das Netz: Ein 230V 50Hz \~ Netz ist schon teuer, ein
zusätzliches 12 V = Netz und ein 48 V \~ Netz ist aufwändiger. Außerdem
ist nieder gespannter Gleichstrom viel zu schwierig zu produzieren, auch
wenn ich selbigen aus meiner PV Anlage raus bekomme.

Eine öffentlich Versorgung mit einem zweiten unedlen Strom wäre
vermutlich volkswirtschaftlich unwirtschaftlich. Lokale Verbraucher
könnten diesen 'Schmuddelstrom' aber
[eigenverbrauchen](//https://de.wikipedia.org/wiki/eigenverbrauch_(solarstrom) "wikilink").

Die meisten Haushalte werden mit Drehstrom beliefert, nutzen alle Phasen
aber einzeln. Die meisten Privatverbraucher brauchen auch keine
konstante Frequenz: Heizen, Warmwasser, Kochen, Licht (Glühlampen und
LEDs), Kühlung. Selbst Elektronik hängt hinter einem
Gelichstromnetzteil. Ladegeräte für Mobiltelefone oder Laptops sind
meist auch nicht lade-geregelt, den der Regler sitzt im Gerät. Kommt
eine Laptop, der sowieso eine internen Puffer hat evtl nicht mit
schwankendem Strom aus?

Ein Niederstromnetz in einem Haus ist aber technisch einfacher möglich.
Wird Beleuchtung, Elektronik gleich mit Niederstrom versorgt und nur in
der Nacht mit einem zentral Netzteil versorgt, so spart man sich auch
weiten Teil des 230V Netzes. Allerdings fehlen bisher noch
haushaltsfähige Kühl- und Kochgeräte die Niederspannung umgehen können.
Mit Campingbedarf gibt es aber probate Versuchsgeräte.

Und dann kann man auch den unstetigen Strom aus
[Bleiakkus](//https://de.wikipedia.org/wiki/bleiakkumulator "wikilink") verbrauchen.

Anbindung
---------

Eine Insellösung mit rohem Gleichstrom ist vermutlich nicht sinnvoll.
Aber man könnte eine PV-Anlage im Verhältnis zu deren Wechselrichter
über-dimensionieren und damit in Sonnen schwachen Stunden die volle
Leistung ins Netz speisen. Und in der Sonneleistunsgspitze den für den
Wechselrichter überflüssigen Strom lokal für Kochen, Vorkühlen etc
verbrauchen.

Den [Wechselrichter haben 10% –
20%](//http://www.photovoltaik.info/billig-oder-teuer-teil-1-verteilung-der-kosten/#anteil-der-einzelnen-materialien-und-dienstleistungen-am-gesamtpreis)
Anteil am Gesamtpreis.

<Kategorie:Energie>
