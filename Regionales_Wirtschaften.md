# Regionales Wirtschaften

**Regionales Wirtschaften** :konzentriert wirtschaftliche Aktivitäten
auf geografisch überschaubare Regionen, in deren Mittelpunkt der Mensch
und seine Umwelt steht. Um einen Fokus auf die Region zu erreichen,
werden neue Werkzeuge und Konzepte eingesetzt und/oder entwickelt:

-   [Regionalgeld](/regionalgeld "wikilink")
-   [Mittlere Technologien](/mittlere_technologien "wikilink")

Neue Ideen sollen dazu führen, daß
[Transparenz](/transparenz "wikilink"),
[Kooperation](/kooperation "wikilink") und
[Fairness](/fairness "wikilink") neben
[Konkurrenz](/konkurrenz "wikilink"), [markt](/markt "wikilink") und
[Gewinnstreben](/gewinnstreben "wikilink") eine bedeutende Rolle spielen:

-   [Aufwandsbezogene
    Preisbildung](/aufwandsbezogene_preisbildung "wikilink")
-   [Methode der
    Selbstfinanzierung](/methode_der_selbstfinanzierung "wikilink")

Güter die man nicht mehr nutzt, aber es zu aufwändig wäre diese zu
verkaufen kann man einfach verschenken, um jemand zu finden der genau
das auch sucht [freecycle.org](http://freecycle.org).
