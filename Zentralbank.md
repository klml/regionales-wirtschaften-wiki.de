# Zentralbank

Eine Zentralbank ist eine für die Geld- und Währungspolitik eines Waehrungsraums
zuständige Institution. In klassischen Wirtschaftsräumen sorgt eine
meist staatliche Zentralbank für die Menge und die Umlaufkriterien ihres
[Geldes](/geld "wikilink"). Die Zentralbank ist also kein göttliches
Wesen, was man manchmal vermuten könnte.

Allerdings gibt es auch alternative *Herausgeber* von Geld z.B. oder
zwar an eine andere Währung angelehntes Geld, aber mit alternativem
Verbreitungsweg.

Vorraussetzung
--------------

-   Jeder Teilnehmer muss dem Geldsystem
    [Vertrauen](/vertrauen "wikilink") schenken. Meist ist das der Fall,
    es gibt aber viele Fälle wo sich die Handelspartner in einem
    Wirtschaftsraum aufgrund von durch Enttäuschung dieses Vertrauen
    verloren haben und eine benutzt haben.
-   Eine kontrollierte Geldmenge steuert das Verhältnis zwischen
    Inflation und Deflation, aber auch ob ein Wirtschaftssystem liquide
    (im eigentlichen Sinne von flüssig) ist um schnellen Handel treiben
    zu koennen.

Unabhängigkeit
--------------

Die angebliche [Unabhängigkeit der
Zentralbanken](http://www.freilich.ch/blog/?p=564).

Luna
----

[Luna](/luna "wikilink") soll eine Möglichkeit darstellen, für wen auch
immer (Einzelperson, Verein, Region, Religion, etc), nach diesen
Grundsätzen eine Währung aufzulegen.

<Kategorie:Theorie>
