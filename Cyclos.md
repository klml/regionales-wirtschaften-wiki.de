# Cyclos

[Cyclos](http://project.cyclos.org) ist eine
[Open-Source](/open-source "wikilink") Software um alternative
[Finanzsysteme](/finanzsysteme "wikilink") wie [lets](/lets "wikilink"),
[Barter](/barter "wikilink"), [time bank](/time_bank "wikilink"),
[C3](/c3 "wikilink") (consumer commerce circuit) und
[Regionalgelder](/regionalgeld "wikilink") zu unterstützen.

Die [Havelblüte](/havelblüte "wikilink") setzt Cyclos
[ein](http://havelblueten.digitekst.com/) (anscheinend auch für
[Oder-](/oderblüte "wikilink") und [spreeblüte](/spreeblüte "wikilink")).

[digitekst.com](http://digitekst.com/lets) unterhält ein
Whitelabelangebot und auch ein [Cyclos 3 demo
system](https://www.digitekst.com/cyclosen/do/)

