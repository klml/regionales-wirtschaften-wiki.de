# Effizienzrevolution

Herausforderungen wie [Peak Oil](/peak_oil "wikilink") und
[Klimawandel](/klimawandel "wikilink") machen eine
**Effizienzrevolution** innerhalb der menschlichen Gesellschaft nötig.

Ansätze & Konzepte:

-   [Energieeffizienz](/energieeffizienz "wikilink")
-   [Mobilitätseffizienz](/mobilitätseffizienz "wikilink")
-   [Ressourceneffizienz](/ressourceneffizienz "wikilink")

