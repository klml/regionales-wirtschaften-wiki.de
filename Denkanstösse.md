# Denkanstösse

Einige weitere Denk- und Diskussionsanstösse:

VW tritt einem Regiogeldnetzwerk bei und zahlt 20% der Löhne in Regionalgeld.
:   Wie ist jetzt vorzugehen? Gibt es auch genügend Abnehmer für das
    Geld? Oder bewirkt diese Schwemme an Regionalgeld
    Annahmeverweigerung?

Eine Interessensgruppe tritt geschlossen bei und stellt 75% der Mitglieder. Es gibt eine Abstimmung über eine geänderte Methode der Geldschöpfung.
:   bleibt man demokratischen Grundsätzen treu, auch wenn sie
    missbraucht werden?

Die Arbeitszeiten freiwilliger Helfer reichen nicht mehr aus - man würde praktisch 6 Vollzeitkräfte brauchen, die Einnahmen aus Umlaufgebühren sind aber viel zu niedrig.
:   Wie kann man die Einnahmen erhöhen, ohne das Vertrauen zu
    beschädigen?

Es wird ein fiktives Szenario beschrieben. Aus diesem Szenario heraus,
sollen möglichst konstruktive Ansätze des
[Neustart](/szenario_neustart "wikilink") entwickelt werden.

Um eigene Ideen einem “Feuertest” zu unterziehen, gibt es den [advocatus
diaboli](/advocatus_diaboli "wikilink"). Der advocatus diaboli ist
gedacht, um die eigenen Ideen zu überprüfen.
