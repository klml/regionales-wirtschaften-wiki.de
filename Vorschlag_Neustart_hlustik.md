# Vorschlag Neustart hlustik

Dieser Text stammt von <Benutzer:Hlustik> und wurde ursprünglich in
einem Forum verwendet.

Im ökonomischen Mainstream wird der “Souverän” ein Geld (irgendwelche
bedruckten Papiere) schaffen und den Papierln dadurch Wert verleihen,
dass er jeden Bürger zwingt, an jedem Monatsende 5 dieser Papierln als
Steuer zu bezahlen. Wenn das Ganze mit einem Papierlverteilungsprogramm
(pardon: Konjunkturprogramm) verbunden ist, besteht sogar die Chance,
dass es ohne übermässige Anwendung des Halsrechtes funktioniert. Die
damit verbundenen Gefahren sind jedoch enorm: Wenn zuviel Geld
ausgegeben wird, ist Inflation vorprogrammiert, wenn es zu wenig ist,
ist die Schuldknechtschaft für einen Teil der Bevölkerung ebenso fix.
Grösstes Problem sind die völlig fehlende Limitierung der
“Staats”-ausgaben und die Konvertierbarkeit. Da es (derzeit) straffrei
ist, empfehle ich dertiges “Geld” nur in der unbedingt notwendigen Menge
anzunehmen.

Den “Neustart” aus der Sicht des Debitismus (Jedes Geld ist schuld.)
sehe ich schwierig. Eventuell könnte es auf der Basis von
wechselseitigen Arbeitsverpflichtungen funktionieren - vergleichbar mit
Fureai-Kippu aus Japan.

Ich glaube, dass aber der Weg der österreichischen Schule (soweit ich
ihn verstanden hätte) am ehesten funktioniert: Wer Waren besitzt, die er
verkaufen möchte, gibt Gutscheine auf eine bestimmte Menge dieser Waren
aus und bezahlt zugekaufte Leistungen mit diesen Gutscheinen. Die
Gutscheine sind tauschbar und können jederzeit gegen Ware eingelöst
werden.

&gt; Der lokale Markt hat seit einer Woche keine Lieferung mehr erhalten
und &gt; ist leergekauft.

“Markt” funktioniert immer. Der pure Tausch in ausgebombten Städten
blieb immer (!) aufrecht. Eine der Aufgaben des Gemeinderats besteht
darin, Sicherheit am Markt zu gewährleisten - im Austausch dafür kann es
eine Abgabe geben. Wenn diese jedoch zu hoch ist, verzichten die
Marktteilnehmer auf den überteuerten Schutz.

&gt; Welchen Masterplan kann man dem Gemeinderat auf den Tisch legen

Der Gemeinderat ist nicht der neue Initiator. Über den Gemeinderat kann
man Interessen bündeln, aber nicht “Wirtschaft” neustarten. Es kann
daher begleitend (!!!) Sinn ergeben, die beiden ältlichen Polizisten als
Markt/Nachtwache einzusetzen und offen über deren Entlohnung/Verpflegung
usw zu sprechen - ich gehe davon aus, dass dies von der Bevölkerung
verstanden würde.

&gt; um nicht in das Zeitalter des puren Warenaustausches zurückzufallen

Wäre nicht so schlimm - nach Mises bildet sich innerhalb von kurzer Zeit
die “marktgängigste” Ware heraus. Es handelt sich dabei um eine Ware,
die nicht zum Verbrauch nachgefragt wird, sondern die gefragt wird, weil
sie im Tausch begehrt ist. (historisch z.B. Zigaretten). Welche Ware das
in dem Dorf wird, kann ich nicht sagen - vielleicht Salz oder
Zündkerzen?

&gt; gleichzeitig einen vorzeitigen Kollaps oder einseitige
Machtansammlung &gt; (Geldansammlung) zu verhindern ??

Wenn das System “Geldansammlung” verhindern soll, wird es nicht
funktionieren. In einer derartigen Situation sind Konzentrationen von
Macht und Geld in den Händen einzelner Akteure sogar notwendig, um so
Impulse (in Form von Produktionsstätten oder Infrastruktur) liefern zu
können. Der willkürliche Missbrauch von Macht ist jedoch ein anderes
Thema.

Das stablisierendste Element gegen vorzeitigen Kollaps sehe ich darin,
dass mehrere Methoden parallel verwendet werden. Wenn etwas kollabiert,
bricht nur ein Teil zusammen und nicht gleich die ganze Gemeinschaft.

&gt; Keiner soll verhungern (also eine gewisse Solidarität und
Finanzausgleich &gt; im System, gleichzeitig soll Fleiß sich auch real
lohnen).

Umverteilung, wie sie heute üblich ist (ca 50% Staatsquote) wird nicht
möglich sein. Ich glaube eher, dass es maximal (!) im Bereich der
mittelalterlichen Abgaben liegen kann: Kirchenzehent für soziale Zwecke
und Staatszehent für öffentliche Ausgaben (Wege, Gericht und
Verteidigung).

Egal, wie das System beschaffen sein soll, wird es unbedingt (!)
Eigentumsrechte respektieren müssen. Ohne Eigentum tritt
Leistungsverweigerung (und damit Stagnation) ein. Eine Idee der Form: “X
besitzt Y, die ,Gemeinschaft' benötigt dringend Y. Also ist es zumutbar,
von X das Gut Y als Solidarbeitrag wegzunehmen” verurteilt das Dorf zur
Stagnation.
