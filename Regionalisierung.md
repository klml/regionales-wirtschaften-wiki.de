# Regionalisierung

**Regionalisierung** kennzeichnet den Prozess der Ausrichtung
menschlicher Siedlungs-, Wirtschafts-, Verkehrs-, Politik-, Netzwerk-
und anderer Strukturen in Form einer [Region](/region "wikilink"). Er
kann als Parallelprozess zur [Globalisierung](/globalisierung "wikilink")
gesehen werden.

Insbesondere bedeutet Regionalisierung

-   eine engere Zusammenarbeit der Kommunen
-   Bildung eines [Versorgungsclusters](/versorgungscluster "wikilink")
-   [Identitätsbildung](/identitätsbildung "wikilink")
-   Schaffung politischer Strukturen (z.B.
    [Regionalparlament](/regionalparlament "wikilink"))
-   [Regionale Selbstverwaltung](/regionale_selbstverwaltung "wikilink")

<Kategorie:Regional>
