# Netzlogistik Abgrenzung

Es gibt zahlreiche Ideen, Konzepte und startups im Bereich Logistik
ähnlich von [Netzlogistik](/netzlogistik "wikilink") die meisten haben
andere Ziele. Wenn diese auch eine offene Lieferkette nicht
ausschließen, sondern sogar damit effizienter wären,

p2p
---

Logistik von Privatperson an Privatperson, allerdings liegt hier der
Fokus nicht auf der Möglichkeit zur Übergabe während der Transportkette

-   ~~[mitpackgelegenheit.de](http://www.mitpackgelegenheit.de)~~
-   ~~[raumobil.de](http://www.raumobil.de) - Plattform zum tauschen von
    Transport Angeboten und Anfragen~~ ist jetzt eine Softwareschmiede
-   ~~[cocarrier.de](https://cocarrier.de)~~ offline
-   ~~[bringbee.ch](https://bringbee.ch) von
    [polyport.ch](http://polyport.ch) bringt Einkäufe (Lebensmittel oder
    IKEA) mit.~~
-   für Auslandseinkäufe
    -   ~~canubring.com~~
    -   [piggybee.com](http://www.piggybee.com)
    -   ~~bringwasmit.de~~
* Die ~~[Physical Internet Initiative](https://web.archive.org/web/20170707150206/http://www.physicalinternetinitiative.org/)~~ hat versucht genau so ein
Logistiksystem, in Anlehnung an das (Daten)-Internet, zu etablieren.
(jetzt gehört die Domain einer data science initative)


Privatpersonen am Ende der Kette
--------------------------------

Ein Unternehmen nutzt die Dienstleistung von Privatpersonen am Ende der
Kette, sie sind aber keine gleichberechtigten Partner.

* Bring.BUDDY: Eine, leider nur auf DHL monokontraktale, Umsetzung ist
  Bring.buddy. Auch weil man weiterhin von A bis B mit einem Anbieter
  verhandelt, andere Dienstleister können Aufträge nur an der letzten
  Meile annehmen
* [pickshare.de](http://pickshare.de) versucht das gleiche, bietet aber
zusätzlich die Möglichkeit Sendungen über Dritte (Community) ausliefern
zu lassen.



Same Day Delivery
-----------------

Wie der Name schon sagt überwindet die **Same Day Delivery** das warten
auf die Post am nächsten oder über-über-nächsten Morgen und liefert
innerhalb von Stunden. Gerade dem Versandhandel würde SDL
Kundenbedürfnisse nach schnellerer Lieferung bedienen. Es gibt
[zahlreiche
Anbieter](http://www.excitingcommerce.de/2013/08/same-day-delivery-wer-mit-wem.html)
welche sich [den
Markt](http://d-lab.com/2013/11/2014-wettrennen-um-den-same-day-delivery-markt/)
sichern wollen. Allerdings ist SDL aber auch nur die bisherige
monokontraktuale Logistikstruktur, nur schneller, weil aufwändiger und
daher auch teurer.

[sppon.de - Google testet Lieferung am ersten
Tag](http://www.spiegel.de/netzwelt/web/google-testet-same-day-delivery-a-887256.html)
... Bestellungen aufgaben und am selben Tage zugestellt bekamen. Schon
damals war es offensichtlich, dass der Konzern keine eigenen
Warenmagazine oder Versanddienste aufbauen wollte.

Auch wenn SDL angeblich
[vor-dem-durchbruch](http://mckinsey.de/warenzustellung-am-selben-tag-vor-dem-durchbruch)
steht, es ist nicht klar [wer den Irrsinn mit Same Day Delivery et
cetera bezahlen soll. Der Kunde will nicht, und der Händler kann
nicht.](http://www.onetoone.de/Einspruch-von-Peter-Hoeschl-Der-grosse-Logistik-Quatsch-24449.html)

Personen
--------

-   ~~hitchbase.com~~ - Datenbank für Tramper
-   [moovel.com](https://www.moovel.com) - Routing für Personen

Alternative Sendungsannahme
---------------------------

Das Problem Sendungen [zu
empfangen](/netzlogistik#letzte_meile "wikilink") lösen derzeit viele
Initiativen und Startups.

### Paketboxen

-   Die raren und oft überfüllten
    [Packstation](https://de.wikipedia.org/wiki/packstation "wikilink") und der voluminöse
    [Paketkasten](https://www.paket.de/paketkasten) sind leider auf DHL
    beschränkt.
-   ~~[lockbox](https://www.lockboxsystem.com/) ist ein fliegender
    Paketkasten der DPD und auf bestimmte onlineshops beschränkt~~ ist
    offline
-   [paketin.de](https://www.b2b.paketin.de/wohnungswirtschaft) hat
    Paketboxen für Ein- und Mehrfamilienhäuser sowie ganze Wohnblöcke
    und ist Anbieteroffen, d.h. alle Dienstleister können die Box.

Bislang hat sich aber noch kein System durchgesetzt und es existieren
einige nebeneinander.

-   Anbieteroffen waren ~~locumilabs~~ die
    (anscheinend) einmal von einem Boten befüllt und verschlossen werden
    kann und dann nur vom Empfänger geöffnet werden kann. Und für die
    nächste Einlage bereitgestellt werden kann. Verkaufen inzwischen
    Gegensprechanalgen, mit App!

### Sammler

Einige Anbieter sammeln Pakete verschiedener Lieferanten, aber will dann
4,90 € für eine Zustellung, egal wie viele Pakete. Allerdings geben die
KEPs ihr Ersparnis, da sie plötzlich ja noch noch einen Hub anfahren
müssen, nicht weiter.

-   Konzepte wie ~~paket.de~~ versuchen den
    Empfänger mehr zu integrieren, aber nicht über Anbietergrenzen
    hinweg.
-   ~~pakx.de~~ sammelt und liefert

Ebenfalls Pakete sammelt [borderlinx.com](http://www.borderlinx.com),
aber nicht auf der letzten Meile, sondern über Zoll- und damit oft auch
Liefergrenzen hinweg.

Die meisten Konzepte haben sich aber nicht durchgesetzt

- deliverix.net wurde zum 29. Februar 2016 eingestellt.

## Blockchain

PassLfix, ein peer2peer Logistik-Netzwerk, das
vor allem auf die Integrität der Transporte durch eine
[Blockchain](https://de.wikipedia.org/wiki/blockchain "wikilink") achtet, aber auch das Übergaben
entlang der ganzen Kette ermöglicht.