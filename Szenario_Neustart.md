# Szenario Neustart

Dieses Szenario wurde von User [offthspc im gelben
Forum](http://www.dasgelbeforum.de.org/user.php?id=157) entwickelt:
Nehmen wir mal an es kommt zum Gau und alle Geldströme versiegen (und
damit auch die Warenströme unserer globalisierten Welt).

Das ganze in meinem kleinem Kuhdorf ohne ethnische Spannungen.

Der Lokalbanker läßt sich beim Dorfwirt volllaufen, der Gemeinderat tagt
seit 2 Tagen, die “Zuagroasten” die hier ihre Siedlungshäuser errichtet
haben sind schon seit mehreren Wochen arbeitslos da die Großkonzerne in
der Stadt nach monatelangen Produktionsstillegungen geschlossen haben,
der Tankstellenpächter gibt keinen Sprit raus selbst wenn sporadisch
noch der Strom eingeschaltet wird (Strombörse mangels Vertrauen auf
Bezahlung durch Abnehmer etc. ebenfalls außer Funktion), ebenso der Rest
der modernen und privatisierten Telekommunikation.

Der lokale Markt hat seit einer Woche keine Lieferung mehr erhalten und
ist leergekauft.

Während bei den einen die Lebensmittelvorräte schwinden sitzen die
Bauern auf Tonnen an lebender (Tiere) und toter (Futtervorräte, für
menschlichen Verzehr notfalls geeignet) Nahrung für die sie keine
Großabnehmer mehr haben bzw. fällt die allwöchentliche Futterlieferung
wohl aus.

Die beiden ältlichen einheimischen Postenleiter der Polizei stehen
schulterzuckend vor dem Posten während der Rest der Belegschaft aus weit
entfernten Orten (lokaler Usus) gar nicht erst zum Dienst erschienen
ist.

Ich gehe natürlich davon aus das sich die Menschen nicht in mordlüsterne
Raubtiere verwandeln (zumindest jetzt noch nicht) und angesichts der
Krisensituation eine erhöhte Kooperationsbereitschaft gegeben ist. Euro
ist wertlos, Gold hat kaum wer und wer es hat rückt es nicht raus.

Lösungsansätze
--------------

Wie bringt man wieder das Schmiermittel “Geld” in den lokalen Umlauf ?

Freigeld ?
:   Eventuell möglich

Freiland ?
:   Sicher nicht möglich, die hiesigen Bauern sitzen teilweise seit
    Jahrhunderten auf ihren Grundstücken und eher tauchen da an
    Waldrändern neue Gedenkkreuze auf die an diejenigen erinnern sollen
    die darunter begraben sind als das da auch nur ein Quadratmeter per
    Zwang den Besitzer tauscht...

Das KnowHow ist da (qualifizierte AN die bislang außerorts tätig waren
und ich habe schon Hobbywerkstätten in schlichten Reihenhauskellern
gesehen die Mittelständler vor Neid erblaßen lassen), Manuelle Arbeit
gibt es aufgrund des Mangels externer Dienstleister, mangels an
Ersatzteilen, etc. mehr als genug (Aufrechterhaltung Sicherheit (um den
Faktor Macht gleich wieder im Spiel zu haben) Ernte, Tierhaltung,
Holzarbeiten um eine rudimentäre Energieversorgung sicherzustellen, ..)

Welchen Masterplan kann man dem Gemeinderat auf den Tisch legen um nicht
in das Zeitalter des puren Warenaustausches zurückzufallen und
gleichzeitig einen vorzeitigen Kollaps oder einseitige Machtansammlung
(Geldansammlung) zu verhindern?

Ich gehe hier nicht von einem “Erhaltung der Staatsmacht” Ansatz aus,
nehmen wir einfach an das Thema externe Machtfaktoren und deren
ausführende (und eventuell korrupten) Organe ist aus diversen Gründen
(kein Sprit, keine Kommunikation etc., Unruhen in den Städten) zumindest
für die nächsten 5 Jahre kein Thema.

Keiner soll verhungern (also eine gewisse Solidarität und
Finanzausgleich im System, gleichzeitig soll Fleiß sich auch real
lohnen). Wie sieht die neue Wirtschaftsordnung anhand dieses kleinen,
regionalen Beispiels in Zukunft aus ?

Lösungsvorschläge
-----------------

-   [Vorschlag Neustart hlustik](/vorschlag_neustart_hlustik "wikilink")
-   [Vorschlag Neustart
    Trägermedium](/vorschlag_neustart_trägermedium "wikilink")

