# OpenSource-Software als Ressource in regionalen_Wirtschaftskreisläufen

Computer sind im heutigen Wirtschaftsleben nicht mehr wegzudenken. Sie
steuern Maschinen, regeln Kommunikationsvorgänge, helfen bei
Konstruktion, Analyse und Gestaltung. In industrialisierten Regionen der
Welt sind Computer sogar in nahezu jedem Haushalt anzutreffen, teilweise
als Personal Computer, teilweise in Geräten wie Mobiltelefonen,
Fernsehern, Spielkonsolen und Haushaltsgeräten. In Unternehmen sind
Computer fester Bestandteil der Unternehmensressourcen.

Wichtige Bausteine eines Computers sind nicht nur die harte, anfaßbare
Materie, aus der die Geräte bestehen, sondern auch die Software, die auf
Computern eingesetzt wird. Erst die Software bestimmt, welchen konkreten
Zweck ein Computer erfüllt. In den letzten Jahren wird ein Konzept der
Softwareentwicklung immer wichtiger: Das Konzept offener Software.

Freie Software
--------------

OpenSource-Software gehört niemanden. Sie ist in ihrer Standardfunktion
kostenfrei nutzbar, die meisten Programme sind übers Internet verfügbar.
Der Quelltext liegt offen vor, kann also von jedermann eingesehen und
geändert werden. Damit wird das Prinzip der Wissenschaft, Wissen
allgemein zu publizieren und es damit einsehbar und vervielfältigbar zu
machen, auf die Softwareerstellung angewendet.

Der offene Quelltext macht OpenSource-Software problemlos auf die
eigenen Bedürfnisse anpassbar. Im Rahmen regionaler
Wirtschaftskreisläufe ist dieses Konzept in mehrfacher Hinsicht
bedeutsam:

1.  Programmierer lernen voneinander, wenn sie Einblick in die Methoden
    anderer bekommen. Das Konzept von OpenSource fördert den allgemeinen
    Wissenszuwachs.
2.  Da keine Lizenzgebühren anfallen und OpenSource-Software im
    Vergleich zu proprietärer Software in der Anschaffung sehr
    kostengünstig ist, findet kein Kaufkraftabfluß aus der Region statt.
3.  Open Source erschwert die Entstehung von Software-Monopolen.
    Monopolgewinne, die naturgemäß direkt von den Software-Nutzern,
    indirekt von Kunden und Gesamtgesellschaft getragen werden müssen,
    entfallen und die entsprechenden Summen verbleiben in den Regionen.
    Die Abhängigkeit von einigen wenigen Softwareherstellern wird
    verringert.
4.  Die freie Verfügbarkeit der Software ermöglicht eine sehr
    differenzierte Entwicklung der Software-Dienstleistungsbranche:
    Installation und Betreuung sowie die Weiter-Entwicklung und
    Anpassung der Software an regionale Bedürfnisse kann von regionalen
    Anbietern erfolgen. Wartung, Schulung und technische Unterstützung
    sind weitere Branchenfelder.
5.  An der Entwicklung von OpenSource-Software können sich viele
    Menschen und Unternehmen beteiligen. Der Entwicklungsaufwand wird
    geteilt und jeder profitiert auch von der Arbeit der anderen. Einmal
    entwickelte Ergänzungen oder Fehlerbehebungen stehen allen Nutzern
    und Regionen auf dem Planeten zur Verfügung. Die Beteiligung an
    dieser intelligenten Art der Arbeitsteilung kann billiger sein als
    Eigenentwicklung oder der Kauf fertiger kommerzieller Software.

OpenSource ist zu großen Teilen eine Leistung von Freiwilligen, die
nicht auf wirtschaftlicher Gewinnmaximierung basiert sondern oft auf
einer pragmatischen “Getting things done”-Haltung. Das Prinzip ist damit
[vergleichbar mit der Freiwilligen
Feuerwehr](http://deisy.org/OpenSource_und_Feuerwehr.html), wo
ehrenamtliche Arbeit in Gemeinschaft dieselben Ergebnisse erzielt wie
die Berufsfeuerwehr - aber eben auf Basis einer anderen Denkweise.
Steuern Unternehmen einen Beitrag zur OpenSource-Softwareentwicklung
bei, so tun sie dies nicht nur um ihr Ansehen zu erhöhen, sondern auch,
um sich im Umfeld der Software-Nutzer als Dienstleister zu präsentieren.
Andererseits können Unternehmen sich die Leistungsfähigkeit der globalen
OpenSource-Community zunutze machen, wenn sie ihre Software als
OpenSource definieren und damit anderen die Weiterentwicklung der
Programme ermöglichen.

Lokalisierung/Regionalisierung nennt man die Anpassung von Software an
lokale Begebenheiten, wie die Übersetzung vorhandener Texte und
Menüpunkt, Berücksichtigung der lokalen Gesetzeslage oder besonderer
Technik. Die meiste global eingesetzte Software durchläuft diesen
Vorgang der Lokalisierung. OpenSource-Programme können durch die freie
Verfügbarkeit ihres Quelltextes von vielen Menschen auf regionale
Anforderungen angepasst werden, es nicht nötig, auf den ursprünglichen
Softwarehersteller und dessen Zeitplan zu warten. Oft finden sich
Gruppen interessierter Programmierer, die gemeinschaftlich Teile der
Programme ändern.

Kommerzielle Software wird einzelnen Akteuren durch entsprechende
Lizenzregeln vorenthalten. Diesem Prinzip der Ausschließlichkeit
unterliegt OpenSource-Software nicht. Sie ist deshalb eher mehr
öffentliches als privates Gut. Diese Stellung macht es wahrscheinlich,
daß der Bereich offener Software anderen Regeln als dem üblichen
Marktgeschehen unterliegt.

OpenSource-Software am Beispiel von Linux
-----------------------------------------

Linux ist ein OpenSource-Betriebssystem, welches seit 1991 von einer
großen Gemeinschaft freiwilliger Programmierer entwickelt wird. Einige
Firmen nahmen sich der professionellen Weiterentwicklung an, so dass es
diverse Distributionen von Linux gibt: Professionell zusammengestellte
und weiterentwickelte Software-Pakete basierend auf Linux. Alle diese
Distributionen sind in ihrem Quelltext frei verfügbar und können weiter
angepaßt werden. Jede Distribution hat ihre eigene Spezialität, manche
sind auf den Betrieb von Servern spezialisiert, manche auf den Einsatz
beim Endbenutzer.

Linux hat gegenüber proprietären Betriebssystemen in erster Linie einen
Kostenvorteil, da selbst professionelle Distributionen kostenfrei
abgegeben werden. Die Firmen finanzieren ihr Engagement durch die
Betreuung von Unternehmen und computernahe Dienstleistungen.

Seit einigen Jahren gewinnt Linux sowohl an öffentlicher Wahrnehmung als
auch an Marktanteilen hinzu. So stieg sein Marktanteil bei
vorinstallierten Betriebssystemen beispielsweise in Großbritannien von
Anfang 2007 bis Mitte 2008 von 0,1% auf 2,8% - eine ver28fachung in
weniger als 2 Jahren
[1](http://ec.europa.eu/enterprise/ict/policy/doc/2006-11-20-flossimpact.pdf).
Zugute kommt dieser freien Software dabei auch, daß es meist sparsamer
mit Ressourcen umgeht, was es auch auf älteren Computern lauffähig macht
und nicht zwingend den Kauf neuester Hardware nötig macht.

Für Linux ist moderne Office-Software (Textverarbeitung,
Tabellenkalkulation, Datenbank) ebenso frei verfügbar wie Grafik- und
eMail-Software, Internet-Browser und viele andere Programme, die von
anderen Betriebssystemen ebenfalls bekannt sind. Im Vergleich zu
kommerzieller Software kann dies Unterschiede in den Anschaffungskosten
von mehreren hundert Euro ausmachen. Mit Hilfe von Emulatoren ist es
möglich, auch die Software anderer Betriebssysteme auf Linux lauffähig
zu machen.

Das Sicherheitskonzept und die bislang geringe Verbreitung haben Linux
bislang weitgehend frei von Virenbefall gehalten und es ist relativ
einfach, eMails und Dokumente zu verschlüsseln, was gerade für
Unternehmen zunehmend wichtiger wird.

Wirtschaftliche Bedeutung von und Förderansätze für OpenSource-Software
-----------------------------------------------------------------------

Eine Studie der Europäischen Kommission beziffert den Gesamtwert des
Marktanteils von OpenSource für das Jahr 2006 auf 12 Milliarden Euro
[2](http://derstandard.at/?id=1216918503031). Für IT-Dienstleistungen,
die rund um OpenSource stattfinden, prognostiziert die Studie für das
Jahr 2010 einen Marktanteil von 32%, was ca. 4% des europäischen
Bruttoinlandsproduktes entsprechen wird. Der dezentrale Ansatz, der mit
der Philosophie des OpenSource-Einsatzes verbunden ist, wird einen
großen Teil der daraus resultierenden Einnahmen auf kleine und
mittelständische Firmen auf regionaler Ebene verteilen. Gefördert werden
kann diese Entwicklung unter anderem durch eine branchenspezifische
Vernetzung von Unternehmen , die die Weiterentwicklung der von ihnen
benötigten Software gemeinsam im Rahmen von Kooperationen unterstützen.

Anstatt **dutzende** Eigenentwicklungen in jeder Branche voranzutreiben
kann im Rahmen von Unternehmensnetzwerken eine OpenSource-Software
vorangetrieben werden, die von allen Martkteilnehmern genutzt und
ihrerseits angepaßt, spezialisiert und weiterentwickelt werden kann.
Eine solche Ressourcenteilung käme allen Beteiligten sowie den
OpenSource-Software-Entwicklern langfristig zugute.

Fazit
-----

Computer und Internet sind inzwischen grundlegende Elemente von
Wirtschaft und Gesellschaft. Die Technik trägt zur effizienten
Gestaltung von Prozessen, Automatisierung und damit zu einer
Wohlstandsverbesserung durch Entlastung des Menschen von monotoner
Arbeit bei. OpenSource-Software liegt im Quelltext vor und ist damit
leicht auf regionale Bedürfnisse anpassbar und erweiterbar. Regionale
Wirtschaftskreisläufe profitieren durch geringeren Kaufkraftabfluß, wenn
OpenSource-Software statt proprietärer Software eingesetzt wird. Für
kleine und mittelständische Computer-Firmen ergibt sich ein Beratungs-
und Betreuungsmarkt, der derzeit mit rasantem Wachstum bei zugleich
kostengünstiger Anschaffung der Software glänzt. Eine Kooperation bei
der Entwicklung von offener Software im Rahmen von
Unternehmensnetzwerken verteilt den notwendigen Aufwand auf viele
Schultern und läßt trotzdem Raum für individuelle
unternehmensspezifische Anpassungen.

<Kategorie:OpenSource>
